#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                     System-wide Package Install Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# @author: John Miller <jmiller@commoninf.com>
# @organization: Commonwealth Informatics - http://www.commoninf.com
# @contact: mailto:esp_support@commoninf.com
# @copyright: (c) 2023 Commonwealth Informatics
# @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This Script is used to install Python3.8 on RHEL servers
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo "installing required redhat-lsb pkg"
echo 
sudo yum install redhat-lsb-core -y
_pkg_manager=''
_pkg_list=''
_os=`lsb_release -sr`

echo
echo Detected OS: $_os
echo

#
# Check Operating System
#
case $_os in
    "8.6" | "8.8" | "8.9" )
        _pkg_manager='yum'
        _pkg_list="python38 python38-setuptools python38-devel python3-virtualenv python38-mod_wsgi"

        ;;
    "7.9" )
	    # add rhscl repo
        echo "adding rhscl repo"
        sudo subscription-manager repos --enable rhel-server-rhscl-7-rpms
        _pkg_manager='yum'
        _pkg_list="rh-python38 rh-python38-mod_wsgi rh-python38-python-setuptools rh-python38-python-devel"
        ;;
esac

#
# Install Packages
#
if [ -n "$_pkg_manager" ]; then
    echo "Automatic system package installation is supported!"
    echo
    echo "Installing packages:"
    echo "    $_pkg_list"
    echo ""
else
    echo ERROR:
    echo ""
    echo "This script currently supports only RedHat Enterprise 7.9 and 8"
    echo ""
    exit 1
fi


case $_pkg_manager in
    "yum" )
        # Update package list
        echo "Refreshing yum package list..."
        echo "" 
        sudo yum update -y
        echo 'Installing yum packages...'
        echo "" 
        # Install Redhat packages
        sudo yum install $_pkg_list -y
        echo "done"
        echo "" 
        ;;
    * )
        echo "Internal error - unknown package manager"
        exit 2
        ;;
esac
