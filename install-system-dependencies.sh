#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                     System-wide Package Install Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# @author: Jason McVetta <jason.mcvetta@gmail.com>
# @organization: Channing Laboratory - http://www.channing.harvard.edu
# @contact: http://esphealth.org
# @copyright: (c) 2009-2011 Channing Laboratory
# @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Run this script to install system-wide dependencies - i.e. those which must
# be installed using the OS's package manager, rather than being installed to
# the local Python virtual environment by Pip.
#
# Currently this script only supports recent versions of Ubuntu.  Eventually it
# may support other OSes.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_pkg_manager=''
_pkg_list=''
_os=`lsb_release -sr`
_arg1=$(echo ${1} | tr [[:upper:]] [[:lower:]])

echo
echo Detected OS: $_os
echo Args: $_arg1
echo

#
# Check Operating System
#
case $_os in
    "22.04" )
        _pkg_manager='apt'
        if [ "$_arg1" == "mssql" ]; then 
            echo "MS SQL"
	    _pkg_list="python3.8 python3.8-venv python3.8-distutils python3-virtualenv apache2 libpq-dev libapache2-mod-wsgi-py3 unixodbc-dev"
        else
            echo "PostgreSQL"
	    _pkg_list="python3.8 python3.8-venv python3.8-distutils python3-virtualenv apache2 libpq-dev postgresql-client-13 postgresql-13 libapache2-mod-wsgi-py3"
        fi
        ;;
    "20.04" )
        _pkg_manager='apt'
        if [ "$_arg1" == "mssql" ]; then 
            echo "MS SQL"
	    _pkg_list="python3 python3-virtualenv python3-pip python3-setuptools python3-dev apache2 libpq-dev libapache2-mod-wsgi-py3 unixodbc-dev"
        else
            echo "PostgreSQL"
	    _pkg_list="python3 python3-virtualenv python3-pip python3-setuptools python3-dev apache2 libpq-dev postgresql-client-13 postgresql-13 libapache2-mod-wsgi-py3"
        fi
        ;;
    "18.04" )
        _pkg_manager='apt'
        if [ "$_arg1" == "mssql" ]; then 
            echo "MS SQL"
	    _pkg_list="python3.8 python3.8-venv python3.8-distutils python3-virtualenv apache2 libpq-dev libapache2-mod-wsgi-py3 unixodbc-dev"
        else
            echo "PostgreSQL"
	    _pkg_list="python3.8 python3.8-venv python3.8-distutils python3-virtualenv apache2 libpq-dev postgresql-client-13 postgresql-13 libapache2-mod-wsgi-py3"
        fi
        ;;
esac
##
## JJM - 8-14-23 - REMOVED support for versions of Ubuntu < 18.04
#
# Install Packages
#
if [ -n "$_pkg_manager" ]; then
    echo "Automatic system package installation is supported!"
    echo
    echo Installing packages:
    echo "    $_pkg_list"
    echo
else
    echo ERROR:
    echo
    echo Automatic installation of system-wide dependencies is currently supported only for Ubuntu Linux.
    echo
    exit 1
fi

case $_os in
   "22.04" )
        echo "UBUNTU 22.04"
        #Ubuntu 22.04
        #Add the postgres key to a new gpg file
        echo 'downloading the POSTGRES apt key'
        curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc| sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg
        #Add postgresql apt file to /etc/apt/sources.list.d/
        echo 'adding POSTGRES to the sources list pgdg.list file'
        sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ jammy-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
        echo 'adding the python deadsnakes repo'
	sudo apt-add-repository ppa:deadsnakes/ppa
    ;;
   "18.04" | "20.04" )
        echo "UBUNTU $_os"
        echo 'adding the python deadsnakes repo'
	sudo apt-add-repository ppa:deadsnakes/ppa
        #Ubuntu 18 or 20
        if [ "$_arg1" == "mssql" ]; then 
            echo 'adding the MS SQL apt sources key'
            # Have to run as super user to execute the following curl command
            sudo su -c "curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -"
            sudo su -c "curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list"
        else
            #Add the postgres key to the apt sources keyring
            echo 'adding the POSTGRES apt sources key'
            sudo wget -q http://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
            #Add postgresql to apt sources
            echo 'adding POSTGRES to the sources list'
            sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
        fi
    ;;
esac
case $_pkg_manager in
    "apt" )
        # Update package list
        echo 'Refreshing apt package list...'
        sudo apt-get update -q
        echo 'Installing apt packages...'
        # Install Ubuntu packages
        sudo apt-get install -q -y $_pkg_list
        if [ "$_arg1" == "mssql" ]; then 
            echo 'Installing additional MS SQL  packages...'
            sudo ACCEPT_EULA=Y apt-get install msodbcsql17 mssql-tools
        fi
        ;;
    * )
        echo "Internal error - unknown package manager"
        exit 2
        ;;
esac




