#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# @author: Rich Schaaf
# @organization: Commonwealth Informatics, Inc
# @copyright: (c) 2013 Commonwealth Informatics, Inc.
# @license: LGPL
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/usr/sbin/logrotate -s /tmp/esp.logrotate.status /srv/esp/backup/esp-sql.logrotate
