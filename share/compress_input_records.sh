#!/bin/bash

# generate name for log file
prefix=$(date +"%Y-%m-")
log_file=$prefix"batch.log"

cd /srv/esp/scripts
python CompressInputRecords.py >>/srv/esp/logs/$log_file 2>&1
