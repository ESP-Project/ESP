#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                    Electronic Support for Public Health
#                              PIP Requirements
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#-------------------------------------------------------------------------------
#
# Packages from PyPI
#
#-------------------------------------------------------------------------------


psycopg2-binary         # Database Adapter (changed to binary for redhat)
python-dateutil         # Date-math utilities
sqlparse                # SQL pretty-printer
hl7                     # Utilities for working with HL7 message
django_tables2==2.7.0   # Utilities for working with HTML tables in Django, last django 3.2 release
configobj               # Advanced configuration file support
#futures                # Python implementation of "futures" threading paradigm
paramiko                # SSH2 protocol library
cryptography==36.0.2    # updated with django - June 2023
django == 3.2           # Updated June 2023 - Web application framework with ORM 
pandas==2.0.3           # date parsing for reading dates from CDAs, last python 3.8 release
djangorestframework==13.5.1	# Django REST Framework, last django 3.2 release
djangorestframework-xml	# REST Framework XML
#pyodbc                  # Only needed for MS SQL implemenations
#django-pyodbc-azure     # Only needed for MS SQL implemenations
#django-mssql-backend    # Only needed for MS SQL implemenations
