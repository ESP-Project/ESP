#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                     System-wide Package Install Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# @author: John Miller <jmiller@commoninf.com>
# @organization: Commonwealth Informatics - http://www.commoninf.com
# @contact: mailto:esp_support@commoninf.com
# @copyright: (c) 2023 Commonwealth Informatics
# @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This Script is used to install Python3.8 on UBuntu servers
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_pkg_manager=''
_pkg_list=''
_os=`lsb_release -sr`
_arg1=$(echo ${1} | tr [[:upper:]] [[:lower:]])

echo
echo Detected OS: $_os
echo Args: $_arg1
echo

#
# Check Operating System
#
case $_os in
    "22.04" )
        _pkg_manager='apt'
        echo "22.04 ubuntu"
	_pkg_list="python3.8 python3.8-venv python3.8-distutils python3-virtualenv libapache2-mod-wsgi-py3"
        ;;
    "20.04" )
        _pkg_manager='apt'
        echo "20.04 ubuntu"
	_pkg_list="python3 python3-virtualenv python3-pip python3-setuptools python3-dev libapache2-mod-wsgi-py3"
        ;;
    "18.04" )
        _pkg_manager='apt'
        echo "18.04 ubuntu"
	_pkg_list="python3.8 python3.8-venv python3.8-distutils python3-virtualenv libapache2-mod-wsgi-py3"
        ;;
esac

if [ -n "$_pkg_manager" ]; then
    echo "This System is supported!"
    echo
    echo Installing packages:
    echo "    $_pkg_list"
    echo
else
    echo ERROR:
    echo
    echo this script only supports Ubuntu Linux.
    echo
    exit 1
fi


case $_pkg_manager in
    "apt" )
        echo 'adding the python deadsnakes repo'
	sudo apt-add-repository ppa:deadsnakes/ppa
        # Update package list
        echo 'Refreshing apt package list...'
        sudo apt-get update -q
        echo 'Installing apt packages...'
        # Install Ubuntu packages
        sudo apt-get install -q -y $_pkg_list
        ;;
    * )
        echo "Internal error - unknown package manager"
        exit 2
        ;;
esac




