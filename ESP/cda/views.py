from rest_framework.views import APIView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes 
from rest_framework.decorators import authentication_classes
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework_xml.parsers import XMLParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from ESP.utils.utils import log
from ESP.settings import DATA_DIR
import datetime

# Create your views here.
@api_view(['POST'])
@parser_classes([XMLParser])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])

def post(request):

    try:
        # verify XML format
        if(request.content_type != 'application/xml'):
            return Response("Only XML accepted, Illegal type: " + request.content_type, 
                status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE, content_type='text/plain')

        # get the patient id from the header
        patientId = ''
        try:
            patientId = request.META['HTTP_X_PATIENT_ID']
        except Exception as e:
            pass

        if(not patientId or len(patientId) < 6 or patientId.find('-') != -1):
            return Response("Missing or bad patient identifier (minimum length 6, dashes not allowed) 'x-patient-id' in header: [" + patientId +"]", 
                status=status.HTTP_417_EXPECTATION_FAILED, content_type='text/plain')
        
        # write the xml to a file under the data/cda structure
        try:
            file = None
            name = ''
            strXml = request.body.decode().strip()

            # check the contents to make sure at least appears to be XML
            length = len(strXml)
            if(length == 0):
                return Response("0 length file", status=status.HTTP_411_LENGTH_REQUIRED, content_type='text/plain')

            if(length < 5 or strXml[0:5].lower() != "<?xml"):
                return Response("Non-XML data", status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE, content_type='text/plain')

            # create a file based on the username, current time and patient id
            dt = datetime.datetime.now()
            prefix = request.user.username[:8] + '-'
            log.debug('name:' + request.user.username)
            log.debug('prefix:' + prefix)
            name = prefix + dt.strftime('%Y%m%d-%H%M%S-%f') + '-' + patientId + '.xml'

            file = open(DATA_DIR + 'cda/incoming/' + name, "w")
            file.write(strXml)

        except Exception as e:
            msgErr = 'Exception creating local cda file: ' + name + '\n' + str(e)
            log.error(msgErr)
            return Response(msgErr, status=status.HTTP_417_EXPECTATION_FAILED, content_type='text/plain')
        
        finally:
            if file != None:
                file.close()

        return Response('received data:' + name, status=status.HTTP_200_OK, content_type='text/plain')

    except Exception as e:
        return Response(e.args[0], status=status.HTTP_400_BAD_REQUEST, content_type='text/plain')
