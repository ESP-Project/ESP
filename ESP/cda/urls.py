'''
                              ESP Health Project
User Interface Module
                               URL Configuration

@authors: Jay Boyer <jboyer@commoninf.com>
'''

from django.urls import include, re_path
from ESP.cda import views

urlpatterns = [
    re_path(r'^$', views.post),   
]
