'''
URLs for entire ESP Health django project
'''
from django.urls import include, re_path
from django.conf.urls import include
from django.contrib import admin
from django.views.generic.base import TemplateView
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.views import LogoutView, LoginView
from ESP.settings import MEDIA_ROOT
from ESP.ui.views import status_page
from ESP.ui.views_survey import generate_survey_report, view_survey_report, survey_import
from django.views import static

class ExtraContextTemplateView(TemplateView):
    extra_context = None

    def get_context_data(self, **kwargs):
        context = super(ExtraContextTemplateView, self).get_context_data(**kwargs)
        if self.extra_context:
            context.update(self.extra_context)
        return context

admin.autodiscover()

urlpatterns = [
    
    # Core Application
    re_path(r'^$', status_page, name='status'),
    
    # Vaers
    re_path(r'^vaers/', include('ESP.vaers.urls')),

    # HIV Risk 
    re_path(r'^hiv_risk/', include('ESP.hiv_risk.urls')),
    
    # Survey 
    re_path(r'^survey_import/', survey_import, name='survey_import'),
    re_path(r'^view_survey_report/', view_survey_report, name='view_survey_report'),
    re_path(r'^generate_survey_report/', generate_survey_report, name='generate_survey_report'),

    # Login and Logout
    #re_path(r'^login/?$', login, {'template_name': 'login.html'}, name='login'),
    #re_path(r'^logout/?$', logout, {'next_page': '/'}, name='logout'),
    re_path(r'^login/?$', LoginView.as_view(), {'template_name': 'login.html'}, name='login'),
    re_path(r'^logout/?$', LogoutView.as_view(), {'next_page': 'logged_out.hml'}, name='logout'),
    
    # About
    re_path(r'^about/', ExtraContextTemplateView.as_view(
        template_name='about.html',
        extra_context={'title': 'About ESP'}
    ), name='about'),
    
    
    # Django Admin
    re_path(r'^admin/', admin.site.urls),
#   (r'^admin/doc/', include('django.contrib.admindocs.urls'),

    re_path(r'^media/(?P<path>.*)$', static.serve, {'document_root': MEDIA_ROOT}),
    
    # Configuration
    re_path(r'^conf/', include('ESP.conf.urls')),
    
    # Nodis
    #re_path(r'^nodis/', include('ESP.nodis.urls')),
    
    # Nodis
    re_path(r'^util/', include('ESP.ui.urls')),

    # cda_input
    re_path(r'^cda/', include('ESP.cda.urls')),

    # Continuity of Care
    re_path(r'^coc/', include('ESP.coc.urls')),

    #
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #
    #re_path(r'^codes', code_maintenance),
    #re_path(r'^json_code_grid', json_code_grid, name='json_code_grid'),
    #re_path(r'^/$', include('ESP.ui.urls')),
]
