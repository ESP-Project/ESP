from django.urls import re_path
from . import views

app_name='vaccine'

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^manufacturers$', views.manufacturers, name='manufacturers'),
    re_path(r'^vaccine/(?P<id>\d*)$', views.vaccine_detail, name='vaccine_detail'),
    re_path(r'^manufacturer/(?P<id>\d*)$', views.manufacturer_detail, name='manufacturer_detail')
    
]
