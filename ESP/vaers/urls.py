# -*- coding: utf-8 -*-

from django.urls import include, re_path

from ESP.vaers import views

app_name = 'vaers'

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^casetable$', views.list_cases),
    re_path(r'^notify/(?P<id>\d+)/$', views.notify),
    re_path(r'^report$', views.report, name='report'),

    # Vaccine and Manufacturer Mapping
    re_path(r'^vaccines/', include('ESP.vaers.vaccine.urls')),
    
    # ptype can be digest or case.  digest takes a digest value for id, case takes a questionnaire id for id.
    re_path(r'^(?P<ptype>case|digest)/(?P<id>\w*)/$', views.case_details, name='present_case'),

    #line listing report
    re_path(r'^download/', views.download_vae_listing, name='download_listing'),    
]

