import datetime


def make_clustering_event_report_file(filename, events, newline_separator='\r\n'):
    f = open(filename, 'w')
    f.write('\t'.join(['id', 'vdate', 'edate', 'gap', 'vaccine', 'comment', 'age', 'gender']))
    f.write(newline_separator)
    for ev in events:
        f.write(ev.render_temporal_report())
        f.write(newline_separator)
    f.close()


def send_event_alert(**kw):
    #TODO work based on case not event
    '''Send newly found adverse events'''
    from ESP.vaers.models import AdverseEvent
    tests_only = kw.pop('test', False)
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    for ev in AdverseEvent.objects.filter(created_on__gt=yesterday):
        if tests_only and ev.is_fake(): 
            ev.mail_notification()


# 'switch' statement to convert ESP values to HL7 race codes
def black():
    return "B"


def asian():
    return "A"


def indian():
    return "I"


def pacific():
    return "P"


def white():
    return "W"


def unk_race():
    return "U"


def other_race():
    return "O"


def race_conversion(race):
    switcher = {
        'BLACK': black,
        'ASIAN': asian,
        'PACIFIC ISLANDER / HAWAIIAN': pacific,
        'NATIVE HAWAI': pacific,
        'ALASKAN': indian,
        'AMERICAN INDIAN / ALASKAN NATIVE': indian,
        'NAT AMERICAN': indian,
        'INDIAN': indian,
        'WHITE': white,
        'CAUCASIAN': white,
        'OTHER': other_race,
    }
    switcher.get(race, unk_race)()


# 'switch' statement to convert ESP ethnicity values to HL7 codes
def hispanic_latin():
    return "H"


def not_hispanic_latin():
    return "N"


def unk_ethnic():
    return "U"


def ethnicity_conversion(race):
    switcher = {
        'HISPANIC': hispanic_latin,
        'Y': hispanic_latin,
        'NON HISPANIC': not_hispanic_latin,
        'N': not_hispanic_latin,
    }
    switcher.get(race, unk_ethnic)()


def state_abbreviation_lookup(state_name):
    from ESP.vaers.models import State

    if len(state_name) < 3:
        return state_name

    s = State.objects.get(name__iexact=state_name)
    if s is None:
        return "FR"    # Not found in the state lookup, FR for foreign
    else:
        return s.abbreviation


if __name__ == '__main__':
    send_event_alert(test=True)
