from datetime import datetime
from ESP.static.models import hl7_vocab, Rxnorm, Ndc
from ESP.utils import log

icd10_code_systems = [
        "http://hl7.org/fhir/sid/icd-10-cm"
        ]

class Fhir:

    @staticmethod
    def get_patient(fields, resource, codes, validate):
        patient = dict.fromkeys(fields)
        # id
        if 'id' in resource:
            try:
                patient['natural_key'] = resource['id']
            except:
                log.warn('Error: patient id is not valid.')
                pass
        elif validate:
            log.warn('Patient id not available.')
        # race, ethnicity, birthsex
        if "extension" in resource:
            race_codes = codes['omb-race']
            ethnicity_codes = codes['omb-eth']
            extensions = resource["extension"]
            for e in extensions:
                if "url" in e:
                    is_race = e["url"] == "http://hl7.org/fhir/us/core/StructureDefinition/us-core-race"
                    is_ethnicity = e["url"] == "http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity"
                    is_birthsex = e["url"] == "http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex"
                    if is_race or is_ethnicity:
                        try:
                            c = e["extension"][0]["valueCoding"]["code"]
                            if is_race:
                                patient['race'] = race_codes[c] if c in race_codes else None
                            elif is_ethnicity:
                                patient['ethnicity'] = ethnicity_codes[c] if c in ethnicity_codes else None
                        except:
                            pass
                    elif is_birthsex:
                        try:
                            patient['birth_sex'] = e['valueCode']
                        except:
                            pass
        elif validate:
            log.warn('Patient id: {}; race, ethnicity, and birth sex not available'.format(patient['natural_key']))
        # MRN, SSN
        if "identifier" in resource:
            identifiers = resource["identifier"]
            for id in identifiers:
                if "type" in id and "coding" in id["type"]:
                    code = id["type"]["coding"][0]["code"]
                    if code == "MR":
                        mrn = id["value"]
                        patient["mrn"] = mrn if len(mrn) < 26 else mrn[0:25]
                    elif code == "SS":
                        patient["ssn"] = id["value"]
                elif "system" in id:
                    if id["system"] == "2.16.840.1.113883.4.1":
                        patient["ssn"] = id["value"]
        elif validate:
            log.warn('Patient id: {}; identifier (MRN or SSN) is not available'.format(patient['natural_key']))
        # name
        if "name" in resource:
            name = resource['name'][0]
            patient['last_name'] = name['family'] if "family" in name else None
            given_names = name['given'] if 'given' in name else None
            patient['first_name'] = name['given'][0] if given_names else None
            patient['middle_name'] = name['given'][1] if (given_names and len(given_names) > 1) else None
        elif validate:
            log.warn('Patient does not have a name.')
        # telecom
        if "telecom" in resource:
            try:
                tel = resource['telecom'][0]
                if tel['system'] == 'phone':
                    tel = tel['value'].split("-")
                    patient['areacode']= tel[0]
                    patient['tel'] = tel[1] + '-' + tel[2]
            except:
                pass
        # gender
        if "gender" in resource:
            patient['gender'] = resource['gender']
        # date of birth
        if "birthDate" in resource:
            patient['date_of_birth'] = resource['birthDate'].replace("-","")
        elif validate:
            log.warn('Patient does not have a birth date')
        # date of death
        if "deceasedDateTime" in resource:
            deceased = resource['deceasedDateTime'].replace("-","")
            patient['date_of_death'] = deceased if len(deceased) < 9 else deceased[0:8]
        # address
        if "address" in resource:
            try:
                address = resource['address'][0]
                patient['state'] = address['state']
                patient['zip'] = address['postalCode']
            except:
                if validate:
                    log.warn('Patient id: {}; state and/or zip code not valid.'.format(patient['natural_key']))
                pass
        elif validate:
            log.warn('Patient id: {}; state and zip code are not available.'.format(patient['natural_key']))

        return patient

    @staticmethod
    def get_encounter(fields, resource, codes, validate):

        encounter = dict.fromkeys(fields)
        # id
        if 'id' in resource:
            try:
                encounter['natural_key'] = resource['id']
            except:
                if validate:
                    log.warn('Encounter id not valid.')
                pass
        elif validate:
            log.warn('Encounter id not available.')
        # patient id
        if 'subject' in resource:
            try:
                patient_id = resource['subject']['reference']
                encounter['patient_id'] = patient_id.replace("Patient/","") if patient_id else None
            except:
                if validate:
                    log.warn('Encounter id: {}; patient id not valid.'.format(encounter['natural_key']))
                pass
        elif validate:
            log.warn('Encounter id: {}; patient id not available.'.format(encounter['natural_key']))
        # dates
        if 'period' in resource:
            try:
                start_date = resource['period']['start'].replace("-","")[0:8]
                end_date = resource['period']['end'].replace("-","")[0:8]
                encounter['encounter_date'] = start_date
                encounter['hosp_admit_dt'] = start_date
                encounter['hosp_dschrg_dt'] = end_date
            except:
                if validate:
                    log.warn('Encounter id: {}; encounter date not valid.'.format(encounter['natural_key']))
                pass
        elif validate:
            log.warn('Encounter id: {}; encounter date not available.'.format(encounter['natural_key']))
        # type
        if 'class' in resource:
            try:
                encounter['event_type'] = resource['class']['code']
            except:
                if validate:
                    log.warn('Encounter id: {}; event type not valid.'.format(encounter['natural_key']))
                pass
        elif validate:
            log.warn('Encounter id: {}; event type not available.'.format(encounter['natural_key']))

        return encounter

    @staticmethod
    def get_encounter_observations(fields, resource, codes, validate):
        encounter = dict.fromkeys(fields)
        vital_loinc_codes = codes['loinc-vit']
        bmi_snomed_codes = codes['snomed-bmi']
        category = 'none'
        if 'category' in resource:
            try:
                category = resource['category'][0]['coding'][0]['code']
            except:
                pass
        # observations
        if ('id' in resource) and (category == 'vital-signs'):
            try:
                # observation id
                try:
                    encounter['observation_id'] = resource['id'] 
                except:
                    if validate:
                        log.warn('Vitals observation id not valid.')
                    pass
                # observation datetime
                try:
                    encounter['observation_datetime'] = resource['effectiveDateTime']
                except:
                    if validate:
                        log.warn('Vitals Observation id: {}; effectiveDateTime not valid.'.format(encounter['observation_id']))
                    pass
                # encounter id
                try:
                    encounter['natural_key'] = resource['encounter']['reference'].replace("Encounter/","")
                except:
                    if validate:
                        log.warn('Observation id: {}; encounter id not valid.'.format(encounter['observation_id']))
                    pass
                # patient id
                try:
                    encounter['patient_id']  = resource['subject']['reference'].replace("Patient/","")
                except:
                    if validate:
                        log.warn('Vitals Observation id: {}; patient id not valid.'.format(encounter['observation_id']))
                    pass
                # vital code
                try:
                    coding = resource['code']['coding']
                    vital = None
                    for c in coding:
                        if c['system'] == 'http://loinc.org':
                            loinc = c['code']
                            vital = vital_loinc_codes[loinc] if loinc in vital_loinc_codes else None
                except:
                    if validate:
                        log.warn('Vitals Observation id: {}; code not valid.'.format(encounter['observation_id']))
                    pass
                # component results
                if 'component' in resource and vital:
                    if vital == 'BP': # blood pressure panel
                        try:
                            component = resource['component']
                            for c in component:
                                coding = c['code']['coding']
                                vital = None
                                for code in coding:
                                    if code['system'] == 'http://loinc.org':
                                        loinc = code['code']
                                        vital = vital_loinc_codes[loinc] if loinc in vital_loinc_codes else None
                                if vital and vital == "SBP":
                                    encounter['bp_systolic'] = str(c['valueQuantity']['value'])
                                elif vital and vital == "DBP":
                                    encounter['bp_diastolic'] = str(c['valueQuantity']['value'])
                        except:
                            if validate:
                                log.warn('Vitals Observation id: {}; error with blood pressure component code and/or valueQuantity.'.format(encounter['observation_id']))
                            pass
                # value quantity results
                if 'valueQuantity' in resource and vital:

                    try:
                        if 'valueQuantity' in resource:
                            vq = resource['valueQuantity']
                            value = vq['value']
                            unit = vq['unit'] if 'unit' in vq else ''
                        elif 'valueString' in resource:
                            # Get BMI snomed code from valueString: only for hdc omop
                            if vital == 'BMI':
                                snomed = resource['valueString']
                                value = bmi_snomed_codes[snomed] if bmi_snomed in bmi_snomed_codes else None
                        if vital == 'BMI':
                            encounter['bmi'] = str(value)
                        elif vital == "SBP":
                            encounter['bp_systolic'] = str(value)
                        elif vital == "DBP":
                            encounter['bp_diastolic'] = str(value)
                        elif vital == 'BT':
                            if unit and unit == 'Cel':
                                value = 1.8*value + 32
                            encounter['temp'] = str(value) # deg F
                        elif vital == "BH":
                            if unit and unit == 'in_i':
                                value = 2.54*value
                            elif unit and unit == 'ft_i':
                                value = 2.54*12*value
                            encounter['height'] = str(value) # cm
                        elif vital == "BW":
                            if unit and unit == 'lb_av':
                                value = 0.4536*value
                            encounter['weight'] = str(value) # kg
                    except:
                        if validate:
                            log.warn('Vitals Observation id: {}; error with vital valueQuantity'.format(encounter['observation_id']))
                        pass
            except:
                if validate:
                    log.warn('Error in Vitals Observation.')
                pass
        elif validate:
            log.warn('Vitals Observation id not available.')
                
        return encounter

    @staticmethod
    def get_encounter_conditions(fields, resource, codes, validate):
        encounter = dict.fromkeys(fields)
        preg_codes = codes['snomed-prg']
        cid = 'none'
        # conditions
        if 'code' in resource:
            try:
                # condition id
                try:
                    cid = resource['id'] 
                except:
                    if validate:
                        log.warn('Condition id not valid.')
                try:
                # encounter id
                    encounter_id = resource['encounter']['reference'].replace("Encounter/","")
                    encounter['natural_key'] = encounter_id
                except:
                    if validate:
                        log.warn('Condition id: {}; encounter id not valid.'.format(cid))
                    pass
                try:
                    code = resource['code']['coding'][0]['code']
                    code_system = resource['code']['coding'][0]['system']
                    code_text = resource['code']['text']
                except:
                    if validate:
                        log.warn('Condition id: {}; code not valid.'.format(cid))
                    pass
                # pregnancy
                if code in preg_codes:
                    if preg_codes[code] == 'TRUE':
                        encounter['pregnant'] = 'TRUE'
                        onset_date = resource['onsetDateTime'].replace("-","")[0:8]
                        encounter['edd'] = onset_date # TODO - need to check if this is edd
                    else:
                        encounter['pregnant'] = 'FALSE'
                else:
                    encounter['pregnant'] = None
                # dx code
                if code_system in icd10_code_systems and code:
                    dx_code = 'icd-10:'+ code + ":"
                    if code_text: dx_code += code_text
                    encounter['dx_code'] = dx_code
                else:
                    encounter['dx_code'] = None
            except:
                if validate:
                    log.warn('Condition id: {}; load error.'.format(cid))
                pass

        return encounter

    @staticmethod
    def get_encounter_claim(fields, resource, codes, validate):
        encounter = dict.fromkeys(fields)
        # claim
        cid = 'none'
        if 'insurance' in resource:
            try:
                # claim id
                try:
                    cid = resource['id'] 
                except:
                    if validate:
                        log.warn('Claim id not valid.')
                try:
                    # encounter id
                    encounter_id = resource['item'][0]['encounter'][0]['reference'].replace("Encounter/","")
                    encounter['natural_key'] = encounter_id
                except:
                    if validate:
                        log.warn('Claim id: {}; encounter id not valid.'.format(cid))
                    pass
                try:
                    # primary payer (insurance)
                    encounter['primary_payer'] = resource['insurance'][0]['coverage']['display']
                except:
                    pass
            except:
                if validate:
                    log.warn('Claim id: {}; load error.'.format(cid))
                pass

        return encounter

    @staticmethod
    def get_medication(fields, resource, codes, validate):
        medication = dict.fromkeys(fields)
        med_period_units = codes['ucum-time']
        rxnorm_sys = codes['rxnorm-sys']
        ndc_sys = codes['ndc-sys']
        # id
        if 'id' in resource:
            try:
                medication['order_natural_key'] = resource['id']
            except:
                if validate:
                    log.warn('Medication id not valid.')
                pass
        # patient id
        if 'subject' in resource:
            try:
                patient_id = resource['subject']['reference']
                medication['patient_id'] = patient_id.replace("Patient/","")
            except:
                if validate:
                    log.warn('Medication id: {}; patient id not valid.'.format(medication['order_natural_key']))
                pass
        # order date
        if 'authoredOn' in resource:
            try:
                medication['order_date'] = resource['authoredOn'].replace("-","")[0:8]
            except:
                log.warn('Medication id: {}; date not available.'.format(medication['order_natural_key']))
        # name and code
        if 'medicationCodeableConcept' in resource:
            system_codes = resource['medicationCodeableConcept']['coding']
            code = None
            rx_code = None
            rx_name = None
            no_rx_name = "Name awaiting api lookup"
            for sc in system_codes:
                if 'code' in sc and 'system' in sc:
                    code = sc['code']
                    system = sc['system']
                    if system in rxnorm_sys: # check rxnorm first
                        rx_code = 'rxn:' + code
                        if code.isdigit():
                            rxnorm, created = Rxnorm.objects.get_or_create(rxcui=code,
                                                                defaults= {"name":no_rx_name})
                            rx_name = rxnorm.name
                            break
                    elif system in ndc_sys: # then ndc
                        rx_code = 'ndc:' + code
                        if len(code) == 12 and "-" in code: # 4-4-2, 5-3-2 or 5-4-1 digit NDC
                            c = code.split("-")
                            label4 = c[0] if len(c[0]) == 4 else None
                            label5 = c[0] if len(c[0]) == 5 else None
                            prod3 = c[1] if len(c[1]) == 3 else None
                            prod4 = c[1] if len(c[1]) == 4 else None
                            pkg1 = c[2] if len(c[2]) == 1 else None
                            pkg2 = c[2] if len(c[2]) == 2 else None
                            ndc_code = None
                            if label4 and prod4 and pkg2: # 4-4-2
                                ndc_code = "0" + label4 + prod4 + pkg2
                            elif label5 and prod3 and pkg2: # 5-3-2
                                ndc_code = label5 + "0" + prod3 + pkg2
                            elif label5 and prod4 and pkg1: # 5-4-1
                                ndc_code = label5 + prod4 + "0" + pkg1
                            if ndc_code:
                                ndc, created = Ndc.objects.get_or_create(ndc_code=ndc_code, 
                                                                defaults={"trade_name":no_rx_name})
                                rx_name=ndc.trade_name
                                break
                        elif code.isdigit() and (len(code) == 11 or len(code) == 10): # 10-digit or 11-digit NDC
                            if len(code) == 11: # 11-digit
                                ndc_code = code
                                ndc, created = Ndc.objects.get_or_create(ndc_code=ndc_code, 
                                                                defaults={"trade_name":no_rx_name})
                                rx_name = ndc.trade_name
                                break
                            else: # 10-digit
                                # check all 11-digit formats
                                # 5-3-2 (xxxxx-xxx-xx -> xxxxx-0xxx-xx (11-digit))
                                ndc1 = code[:5] + "0" + code[5:8] + code[8:]
                                # 5-4-1 (xxxxx-xxxx-x -> xxxxx-xxxx-0x)
                                ndc2 = code[:5] + code[5:9] + "0" + code[9:]
                                # 4-4-2 (xxxx-xxxx-xx -> 0xxxx-xxxx-xx)
                                ndc3 = "0" + code[:4] + code[4:8] + code[8:]
                                try:
                                    ndc = Ndc.objects.get(ndc_code__in=[ndc2, ndc2, ndc3])
                                    rx_name = ndc.trade_name
                                except Ndc.DoesNotExist as e:
                                    rx_name = no_rx_name
                                except Ndc.MultipleObjectsReturned as e:
                                    rx_name = no_rx_name
                                    log.warn('Multiple NDC codes in static_ndc for in medrequest ID {0}'.format((medication['order_natural_key'])))

            medication['ndc'] = rx_code
            medication['drug_desc'] = rx_name
            if not code:
                log.warn('Missing RxNorm or NDC code in medrequest ID {0}'.format((medication['order_natural_key'])))
            elif not rx_name:
                log.warn('Invalid RxNorm or NDC code in medrequest ID {0}'.format((medication['order_natural_key'])))

        # directions, route, dose, frequency
        if 'dosageInstruction' in resource:
            try:
                medication['directions'] = resource['dosageInstruction'][0]['text']
            except:
                pass
            try:
                medication['route'] = resource['dosageInstruction'][0]['route']['text']
            except:
                if validate:
                    log.warn('Medication id: {}; route not available.'.format(medication['order_natural_key']))
                pass
            try:
                dose_qty = resource['dosageInstruction'][0]['doseAndRate'][0]['rateQuantity']
                dose = str(dose_qty['value'])
                dose_unit = dose_qty['unit'] if 'unit' in dose_qty else ''
                if dose_unit: dose = str(dose + ' ' + dose_unit)
                medication['dose'] = dose
            except:
                if validate:
                    log.warn('Medication id: {}; dose not available.'.format(medication['order_natural_key']))
                pass
            try:
                repeat = resource['dosageInstruction'][0]['timing']['repeat']
                frequency = str(repeat['frequency'])
                period = str(repeat['period']) if 'period' in repeat else ''
                period_unit = repeat['periodUnit'] if 'periodUnit' in repeat else ''
                period_unit = med_period_units[period_unit] if period_unit in med_period_units else ''
                period = period + ' ' + period_unit if period and period_unit else '' 
                medication['frequency'] = frequency +' times per ' + period
            except:
                pass
        # quantity, refills, start_date, end_date
        if 'dispenseRequest' in resource:
            try:
                medication['quantity'] = str(resource['dispenseRequest']['quantity']['value'])
            except:
                if validate:
                    log.warn('Medication id: {}; quantity not available.'.format(medication['order_natural_key']))
                pass
            try:
                medication['refills'] = str(resource['dispenseRequest']['numberOfRepeatsAllowed'])
            except:
                if validate:
                    log.warn('Medication id: {}; refills not available.'.format(medication['order_natural_key']))
                pass
            try:
                medication['start_date'] = resource['dispenseRequest']['validityPeriod']['start']
            except:
                if validate:
                    log.warn('Medication id: {}; start date not available.'.format(medication['order_natural_key']))
                pass
            try:
                medication['end_date'] = resource['dispenseRequest']['validityPeriod']['end']
            except:
                if validate:
                    log.warn('Medication id: {}; end date not available.'.format(medication['order_natural_key']))
                pass
        return medication

    @staticmethod
    def get_lab_observation(fields, resource, codes, validate):
        lab_obs = dict.fromkeys(fields)
        category = 'none'
        if 'category' in resource:
            try:
                category = resource['category'][0]['coding'][0]['code']
            except:
                pass
        # lab observation
        if (category == 'laboratory'):
            # observation id - natural key
            try:
                lab_obs['natural_key'] = resource['id']
            except:
                if validate:
                    log.warn('Lab observation id not valid.')
                pass
            # patient id
            try:
                patient_id = resource['subject']['reference']
                lab_obs['patient_id'] = patient_id.replace("Patient/","")
            except:
                if validate:
                    log.warn('Lab Observation id: {}; patient id not valid.'.format(lab_obs['natural_key']))
                pass
            # lab code
            try:
                lab_obs['component'] = resource['code']['coding'][0]['code']
            except:
                if validate:
                    log.warn('Lab Observation id: {}; lab code not available.'.format(lab_obs['natural_key']))
                pass
            # lab name
            try:
                lab_obs['component_name'] = resource['code']['coding'][0]['display']
            except:
                if validate:
                    log.warn('Lab Observation id: {}; lab name not available.'.format(lab_obs['natural_key']))
                pass
            # result date
            try:
                lab_obs['result_date'] = resource['effectiveDateTime'].replace("-","")[0:8]
            except:
                if validate:
                    log.warn('Lab Observation id: {}; lab date not available.'.format(lab_obs['natural_key']))
                pass
            # result
            try:
                result_val = resource['valueQuantity']['value']
                lab_obs['result_string'] = str(result_val) if result_val else None
            except:
                pass
            try:
                lab_obs['result_string'] = str(resource['valueString'])
            except:
                pass
            try:
                lab_obs['result_string'] = str(resource['valueBoolean'])
            except:
                pass
            try:
                lab_obs['result_string'] = str(resource['valueInteger'])
            except:
                pass
            try:
                lab_obs['result_string'] = str(resource['valueCodeableConcept']['text'])
            except:
                pass

        return lab_obs

    @staticmethod
    def get_immunization(fields, resource, codes, validate):
        immunization = dict.fromkeys(fields)
        # id
        if 'id' in resource:
            try:
                immunization['natural_key'] = resource['id']
            except:
                if validate:
                    log.warn('Immunization id not valid.')
                pass
        # patient id
        if 'patient' in resource:
            try:
                patient_id = resource['patient']['reference']
                immunization['patient_id'] = patient_id.replace("Patient/","")
            except:
                if validate:
                    log.warn('Immunization id: {}; patient id not valid.'.format(immunization['natural_key']))
                pass
        # name
        try:
            if 'coding' in resource['vaccineCode'] and 'display' in resource['vaccineCode']['coding'][0]:
                immunization['name'] = resource['vaccineCode']['coding'][0]['display']
            elif 'text' in resource['vaccineCode']:
                immunization['name'] = resource['vaccineCode']['text']
            elif validate:
                log.warn('Immunization id: {}; name not available.'.format(immunization['natural_key']))
        except:
            if validate:
                log.warn('Immunization id: {}; name not available.'.format(immunization['natural_key']))
            pass
        # type
        try:
            immunization['type'] = resource['vaccineCode']['coding'][0]['code']
        except:
            if validate:
                log.warn('Immunization id: {}; type (code) not available.'.format(immunization['natural_key']))
            pass
        # date
        try:
            immunization['date'] = resource['occurrenceDateTime'].replace("-","")[0:8]
        except:
            if validate:
                log.warn('Immunization id: {}; date not available.'.format(immunization['natural_key']))
            pass
        # manufacturer
        try:
            immunization['manufacturer'] = resource['manufacturer']['reference']
        except:
            pass
        # lot
        try:
            immunization['lot'] = resource['lotNumber']
        except:
            pass

        return immunization

    @staticmethod
    def get_socialhistory_observation(fields, resource, codes, validate):
        soc_hist = dict.fromkeys(fields)
        smoking_loinc_codes = codes['loinc-smk']
        alcohol_loinc_codes = codes['loinc-alc']
        smoking_snomed_codes = codes['snomed-smk']
        # loinc
        loinc = ''
        try:
            loinc = resource['code']['coding'][0]['code']
        except:
            pass
        # tobacco use
        if loinc in smoking_loinc_codes:
            try:
                smoking_snomed = str(resource['valueCodeableConcept']['coding'][0]['code'])
                soc_hist['tobacco_use'] = smoking_snomed_codes[smoking_snomed]
            except:
                pass

        # alcohol use
        elif loinc in alcohol_loinc_codes:
            try:
                n_drinks = int(resource['valueQuantity']['value'])
                if n_drinks > 0:
                    soc_hist['alcohol_use'] = "Yes"
                elif n_drinks == 0:
                    soc_hist['alcohol_use'] = "No"
            except:
                pass
        if soc_hist['tobacco_use'] or soc_hist['alcohol_use']:
            try:
                soc_hist['natural_key'] = resource['id']
            except:
                if validate:
                    log.warn('Social history observation id not valid.')
                pass
            try:
                patient_id = resource['subject']['reference']
                soc_hist['patient_id'] = patient_id.replace("Patient/","")
            except:
                if validate:
                    log.warn('Social history observation id: {}; patient id not valid.'.format(soc_hist['order_natural_key']))
                pass
            try:
                soc_hist['date_noted'] = resource['effectiveDateTime'].replace("-","")[0:8]
            except:
                if validate:
                    log.warn('Social history observation id: {}; date not valid.'.format(soc_hist['order_natural_key']))
                pass

        return soc_hist

