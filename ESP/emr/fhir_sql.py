'''
                                  ESP Health
                            EMR ETL Infrastructure
                         Raw SQL queries for loading Bulk FHIR resources


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics - http://www.commoninf.com
@copyright: (c) 2022 Commonwealth Informatics
'''
import subprocess, os, json
from django.db import connection
from ESP.utils import log
from ESP.settings import DATABASES


def load_resource_table(table_name, file_name):
    """Creates a table and loads it with a FHIR resource file (ndjson)."""
    with connection.cursor() as cursor:
        # create resource table
        try:
            sql = "CREATE TABLE {} (j jsonb)".format(table_name)
            cursor.execute(sql)
        except:
            log.warning("Table {} already exists.".format(table_name))
            return None

        # Use  psql \copy for remote servers and run in subprocess
        sql = "\COPY {} FROM '{}'".format(table_name,file_name)
        user = DATABASES['default']['USER']
        host = DATABASES['default']['HOST']
        db = DATABASES['default']['NAME']
        pw = DATABASES['default']['PASSWORD']

        my_env = os.environ
        my_env['PGPASSWORD'] = pw
        cmd = ["psql", "-U", user, "-h", host, "-d", db, "-c", sql]
        subprocess.run(cmd, env=my_env, check=True)

def table_exists(table_name):
    with connection.cursor() as cursor:
        try:
            sql = "SELECT 1 FROM {} LIMIT 1".format(table_name)
            cursor.execute(sql)
            return True
        except:
            return False

def drop_table(table_name):
    with connection.cursor() as cursor:
        try:
            sql = "DROP TABLE {}".format(table_name)
            cursor.execute(sql)
        except:
            log.warning("Table does not exist")
            pass
    
def create_preg_edd_table():
    """Create preg_edd table """
    with connection.cursor() as cursor:
        sql = (
            """CREATE TABLE preg_edd(
                patient_id VARCHAR NOT NULL,
                date DATE NOT NULL,
                edd DATE,
                UNIQUE (patient_id, date))"""
            )
        cursor.execute(sql)

def load_preg_edd():
    """Load FHIR observation (social history) pregnancy edd into preg_edd table"""
    log.info('loading preg edd')
    sql = (
            """INSERT INTO preg_edd(patient_id, date, edd)
            SELECT
                j->'subject'->>'reference' AS patient_id,
                CAST(j->>'effectiveDateTime' AS DATE) as date, 
                CAST(j->>'valueDateTime' AS DATE) as edd
            FROM observation_socialhistory
            WHERE j->'code'->'coding' @> '[{"code":"11778-8"}]' AND j->>'valueDateTime' IS NOT NULL
            ON CONFLICT (patient_id, date) DO NOTHING"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def create_preg_edd_encounters(provider_id, provenance_id):
    """Creates new (fake) ESP encounters with pregnancy edd from preg_edd table"""

    sql = """INSERT INTO emr_encounter( 
                                        natural_key, 
                                        date, 
                                        raw_date, 
                                        patient_id, 
                                        created_timestamp,
                                        updated_timestamp,
                                        pregnant,
                                        edd,
                                        provider_id, 
                                        provenance_id,
                                        priority,
                                        primary_payer
                                        )
             SELECT DISTINCT ON (e.patient_id, e.date)
                    MD5(e.patient_id || to_char(e.date, 'YYYYMMDD')),
                    e.date, 
                    to_char(e.date,'YYYYMMDD'), 
                    p.id, 
                    NOW(),
                    NOW(),
                    true,
                    e.edd,
                    {0},{1},
                    '',
                    tpp.primary_payer
            FROM preg_edd e
            INNER JOIN emr_patient p
                ON p.natural_key=replace(e.patient_id,'Patient/','')
            LEFT JOIN temp_primary_payer tpp
                ON tpp.patient_natural_key = replace(e.patient_id,'Patient/','')
                AND e.date >= tpp.start_date
                AND e.date <= tpp.end_date
            ON CONFLICT (natural_key)
            DO UPDATE SET edd = excluded.edd""".format(provider_id, provenance_id)
    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_vital_table():
    with connection.cursor() as cursor:
        sql = (
            """CREATE TABLE vital(
                patient_id VARCHAR NOT NULL,
                date DATE NOT NULL,
                bp_systolic FLOAT,
                bp_diastolic FLOAT,
                temperature FLOAT,
                height FLOAT,
                weight FLOAT,
                bmi FLOAT,
                UNIQUE (patient_id, date))"""
            )
        cursor.execute(sql)

def load_avg_temperature():
    log.info('loading avg temperature')
    sql = (
            """INSERT INTO vital(patient_id, date, temperature)
            SELECT
                j->'subject'->>'reference' AS patient_id,
                CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END as date,
                AVG(CASE
                    WHEN j->'valueQuantity'->>'unit' = 'Cel' THEN CAST(j->'valueQuantity'->>'value' as FLOAT )*1.8 + 32
                    ELSE CAST(j->'valueQuantity'->>'value' as FLOAT )
                  END
                ) AS temperature
            FROM observation
            WHERE j->'code'->'coding' @> '[{"code":"8331-1"}]' OR j->'code'->'coding' @> '[{"code":"8310-5"}]'
            AND CAST(j->'valueQuantity'->>'value' as FLOAT ) > 10
            AND CAST(j->'valueQuantity'->>'value' as FLOAT ) < 150
            GROUP BY date, patient_id
            ON CONFLICT (patient_id, date)
            DO UPDATE SET temperature = excluded.temperature"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def load_avg_weight():
    log.info('loading avg weight')
    sql = (
            """INSERT INTO vital(patient_id, date, weight)
            SELECT
                j->'subject'->>'reference' AS patient_id,
                CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END as date,
                AVG(CASE
                    WHEN j->'valueQuantity'->>'unit' = 'lb_av' THEN CAST(j->'valueQuantity'->>'value' as FLOAT )*0.4536
                    ELSE CAST(j->'valueQuantity'->>'value' as FLOAT )
                  END
                ) AS weight
            FROM observation
            WHERE j->'code'->'coding' @> '[{"code":"29463-7"}]'
            AND CAST(j->'valueQuantity'->>'value' as FLOAT ) > 1
            AND CAST(j->'valueQuantity'->>'value' as FLOAT ) < 500
            GROUP BY date, patient_id
            ON CONFLICT (patient_id, date)
            DO UPDATE SET weight = excluded.weight"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def load_avg_height():
    log.info('loading avg height')
    sql = (
            """INSERT INTO vital(patient_id, date, height)
            SELECT
                j->'subject'->>'reference' AS patient_id,
                CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END as date,
                AVG(CASE
                    WHEN j->'valueQuantity'->>'unit' = 'lb_av' THEN CAST(j->'valueQuantity'->>'value' as FLOAT )*2.54*12
                    ELSE CAST(j->'valueQuantity'->>'value' as FLOAT )
                  END
                ) AS height
            FROM observation
            WHERE j->'code'->'coding' @> '[{"code":"8302-2"}]' OR j->'code'->'coding' @> '[{"code":"8306-3"}]'
            AND CAST(j->'valueQuantity'->>'value' as FLOAT ) > 10
            AND CAST(j->'valueQuantity'->>'value' as FLOAT ) < 300
            GROUP BY date, patient_id
            ON CONFLICT (patient_id, date)
            DO UPDATE SET height = excluded.height"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def load_avg_bmi():
    log.info('loading avg bmi')
    sql = (
            """INSERT INTO vital(patient_id, date, bmi)
            SELECT
                j->'subject'->>'reference' AS patient_id,
                CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END as date,
                AVG(CASE
                        WHEN j->>'valueString' is not null THEN 
                          CASE
                            WHEN j->>'valueString' = '374042' THEN 30.1
                            WHEN j->>'valueString' = '270247' THEN 40.1
                            WHEN j->>'valueString' = '76866' THEN 28.9
                            WHEN j->>'valueString' = '39787' THEN 23.9
                            WHEN j->>'valueString' = '27537' THEN 19.9
                          END
                        ELSE 
                          CASE
                            WHEN j->'code'->'coding' @> '[{"code":"39156-5"}]'
                              THEN CAST(j->'valueQuantity'->>'value' as FLOAT )
                            WHEN j->'code'->'coding' @> '[{"code":"59576-9"}]' THEN
                              CASE
                                WHEN CAST(j->'valueQuantity'->'value' AS FLOAT) < 5 THEN 19.9
                                WHEN CAST(j->'valueQuantity'->'value' AS FLOAT) >= 5 
                                  AND  CAST(j->'valueQuantity'->'value' AS FLOAT) < 85 THEN 23.9
                                WHEN CAST(j->'valueQuantity'->'value' AS FLOAT) >= 85 
                                  AND CAST(j->'valueQuantity'->'value' AS FLOAT)  < 95 THEN 28.9
                                WHEN CAST(j->'valueQuantity'->'value' AS FLOAT) >= 95 
                                  AND CAST(j->'valueQuantity'->'value' AS FLOAT) < 120 THEN 30.1
                                WHEN CAST(j->'valueQuantity'->'value' AS FLOAT) >= 120 THEN 40.1
                              END
                          END
                    END
                ) AS bmi
            FROM observation
            WHERE j->'code'->'coding' @> '[{"code":"59576-9"}]' OR j->'code'->'coding' @> '[{"code":"39156-5"}]'
            GROUP BY date, patient_id
            ON CONFLICT (patient_id, date)
            DO UPDATE SET bmi = excluded.bmi"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def load_avg_systolic():
    log.info('loading avg systolic bp')
    sql = (
            """INSERT INTO vital(patient_id, date, bp_systolic)
            SELECT 
                j->'subject'->>'reference' AS patient_id, 
                CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END as date,
                AVG(CASE
		        WHEN j->'component'->0->'code'->'coding' @> '[{"code":"8480-6"}]' THEN
			    CAST(j->'component'->0->'valueQuantity'->>'value' as FLOAT )
		        WHEN j->'component'->1->'code'->'coding' @> '[{"code":"8480-6"}]' THEN
			    CAST(j->'component'->1->'valueQuantity'->>'value' as FLOAT )
		        WHEN j->'valueQuantity' IS NOT NULL THEN
			    CAST(j->'valueQuantity'->>'value' as FLOAT )
		        END
	            ) AS bp_systolic
            FROM observation
            WHERE (j->'code'->'coding' @> '[{"code":"8480-6"}]'
		OR j->'code'->'coding' @> '[{"code":"85354-9"}]' 
		OR j->'code'->'coding' @> '[{"code":"55284-4"}]' )
            GROUP BY date, patient_id
            ON CONFLICT (patient_id, date)
            DO UPDATE SET bp_systolic = excluded.bp_systolic"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def load_avg_diastolic():
    log.info('loading avg diastolic bp')
    sql = (
            """INSERT INTO vital(patient_id, date, bp_diastolic)
            SELECT 
                j->'subject'->>'reference' AS patient_id, 
                CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END as date,
                AVG(CASE
		        WHEN j->'component'->0->'code'->'coding' @> '[{"code":"8462-4"}]' THEN
			    CAST(j->'component'->0->'valueQuantity'->>'value' as FLOAT )
		        WHEN j->'component'->1->'code'->'coding' @> '[{"code":"8462-4"}]' THEN
			    CAST(j->'component'->1->'valueQuantity'->>'value' as FLOAT )
		        WHEN j->'valueQuantity' IS NOT NULL THEN
			    CAST(j->'valueQuantity'->>'value' as FLOAT )
		        END
	            ) AS bp_diastolic
            FROM observation
            WHERE (j->'code'->'coding' @> '[{"code":"8462-4"}]'
		OR j->'code'->'coding' @> '[{"code":"85354-9"}]' 
		OR j->'code'->'coding' @> '[{"code":"55284-4"}]' )
            GROUP BY date, patient_id
            ON CONFLICT (patient_id, date)
            DO UPDATE SET bp_diastolic = excluded.bp_diastolic"""
        )
    with connection.cursor() as cursor:
        cursor.execute(sql)

def create_temp_primary_payer():
    sql = """CREATE TABLE temp_primary_payer(
                                patient_natural_key VARCHAR(128) NOT NULL,
                                primary_payer VARCHAR(100) NOT NULL,
                                start_date DATE NOT NULL,
                                end_date DATE)"""
    with connection.cursor() as cursor:
        cursor.execute(sql)

def load_temp_primarypayer(tablename, fhir_codes):
    """Load temp primary payer table from FHIR Coverage."""
    coverage_codes = fhir_codes['covg-type']
    if len(coverage_codes) > 0:
        commercial = []
        workcomp = []
        medicaid = []
        medicare = []
        unknown = []
        for k,v in coverage_codes.items():
            if v == 'BCBSCOM':
                commercial.append(k)
            elif v == 'WCOMPAUTO':
                workcomp.append(k)
            elif v == 'MEDICAID':
                medicaid.append(k)
            elif v == 'MEDICARE':
                medicare.append(k)
            elif v == 'SELFPAY':
                unknown.append(k)
            else:
                unknown.append(k)
        sql = """INSERT INTO temp_primary_payer(patient_natural_key, 
                                            primary_payer, 
                                            start_date, 
                                            end_date)
             SELECT
                 p.natural_key,
                 CASE
                    WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{1}'::jsonb THEN 'MEDICARE'
                    WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{2}'::jsonb THEN 'MEDICAID'
                    WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{3}'::jsonb THEN 'COMMERCIAL'
                    WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{4}'::jsonb THEN 'WORKCOMP'
                    WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{5}'::jsonb THEN 'UNKNOWN'
                    ELSE 'UNKNOWN'
                 END,
                 CAST(j->'period'->>'start' AS DATE),
                 CAST(j->'period'->>'end' AS DATE)
            FROM {0}, emr_patient p 
            WHERE 
                p.natural_key=replace(j->'beneficiary'->>'reference','Patient/','')
            """.format(tablename, 
                json.dumps(medicare),
                json.dumps(medicaid),
                json.dumps(commercial),
                json.dumps(workcomp),
                json.dumps(unknown))
    else:
        sql = """INSERT INTO temp_primary_payer(patient_natural_key,
                               primary_payer,
                               start_date,
                               end_date)
                SELECT
                    p.natural_key,
                    j->'payor'->0->>'display' AS primary_payer,
                    CAST(j->'period'->>'start' AS DATE) AS start_date,
                    CAST(j->'period'->>'end' AS DATE) AS end_date
                FROM {0}, emr_patient p
                WHERE
                    p.natural_key=replace(j->'beneficiary'->>'reference','Patient/','') AND
                    j->'period'->>'start' IS NOT NULL AND
	            j->'payor'->0->>'display' IS NOT NULL""".format(tablename)
    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def load_primarypayer(tablename, fhir_codes):
    """Load FHIR Coverage into ESP Encounter primary payer."""
    coverage_codes = fhir_codes['covg-type']
    commercial = []
    workcomp = []
    medicaid = []
    medicare = []
    unknown = []
    for k,v in coverage_codes.items():
        if v == 'BCBSCOM':
            commercial.append(k)
        elif v == 'WCOMPAUTO':
            workcomp.append(k)
        elif v == 'MEDICAID':
            medicaid.append(k)
        elif v == 'MEDICARE':
            medicare.append(k)
        elif v == 'SELFPAY':
            unknown.append(k)
        else:
            unknown.append(k)
    sql = """UPDATE emr_encounter e 
             SET primary_payer = CASE
               WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{1}'::jsonb THEN 'MEDICARE'
               WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{2}'::jsonb THEN 'MEDICAID'
               WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{3}'::jsonb THEN 'COMMERCIAL'
               WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{4}'::jsonb THEN 'WORKCOMP'
               WHEN to_jsonb(string_to_array(j->'type'->'coding'->0->>'code',';')) <@ '{5}'::jsonb THEN 'UNKNOWN'
               ELSE 'UNKNOWN'
              END
            FROM {0}, emr_patient p 
            WHERE 
                p.natural_key=replace(j->'beneficiary'->>'reference','Patient/','') AND
                e.patient_id=p.id AND
                e.date >= CAST(j->'period'->>'start' AS DATE) AND
                e.date <= CAST(j->'period'->>'end' AS DATE)
            """.format(tablename, 
                json.dumps(medicare),
                json.dumps(medicaid),
                json.dumps(commercial),
                json.dumps(workcomp),
                json.dumps(unknown))
    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_socialhistory(tablename, provider_id, provenance_id, fhir_codes):
    """Create ESP social history (smoking status) from FHIR Observation social history"""
    smoking_loinc_codes = fhir_codes['loinc-smk']
    smoking_snomed_codes = fhir_codes['snomed-smk']
    smoke_loinc = []
    smoke_yes = []
    smoke_quit = []
    smoke_never = []
    smoke_na = []
    smoke_passive = []
    for k,v in smoking_loinc_codes.items():
        smoke_loinc.append(k)
    for k,v in smoking_snomed_codes.items():
        if v == 'Yes':
            smoke_yes.append(k)
        elif v == 'Quit':
            smoke_quit.append(k)
        elif v == 'Never':
            smoke_never.append(k)
        elif v == 'Not Asked':
            smoke_na.append(k)
        elif v == 'Passive':
            smoke_passive.append(k)

    if len(smoking_loinc_codes) > 0:
        sql = """INSERT INTO emr_socialhistory(
                            natural_key,
                            created_timestamp,
                            updated_timestamp,
                            date,
                            tobacco_use,
                            patient_id,
                            provenance_id,
                            provider_id)
            SELECT
                j->>'id',
                NOW(),
                NOW(),
                (CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END),
                CASE
                    WHEN to_jsonb(string_to_array(j->'valueCodeableConcept'->'coding'->0->>'code',';')) <@ '{4}'::jsonb THEN 'Yes'
                    WHEN to_jsonb(string_to_array(j->'valueCodeableConcept'->'coding'->0->>'code',';')) <@ '{5}'::jsonb THEN 'Quit'
                    WHEN to_jsonb(string_to_array(j->'valueCodeableConcept'->'coding'->0->>'code',';')) <@ '{6}'::jsonb THEN 'Never'
                    WHEN to_jsonb(string_to_array(j->'valueCodeableConcept'->'coding'->0->>'code',';')) <@ '{7}'::jsonb THEN 'Not Asked'
                    WHEN to_jsonb(string_to_array(j->'valueCodeableConcept'->'coding'->0->>'code',';')) <@ '{8}'::jsonb THEN 'Passive'
                    ELSE NULL
                END,
                p.id,
                {1},
                {2}
            FROM {0}, emr_patient p 
            WHERE 
                p.natural_key=replace(j->'subject'->>'reference','Patient/','') AND
                to_jsonb(string_to_array(j->'code'->'coding'->0->>'code',';')) <@ '{3}'::jsonb
        """.format(tablename, provenance_id, provider_id, 
                    json.dumps(smoke_loinc), 
                    json.dumps(smoke_yes),
                    json.dumps(smoke_quit),
                    json.dumps(smoke_never),
                    json.dumps(smoke_na),
                    json.dumps(smoke_passive)
                    )
    else:
        sql = """INSERT INTO emr_socialhistory(
                            natural_key,
                            created_timestamp,
                            updated_timestamp,
                            date,
                            tobacco_use,
                            patient_id,
                            provenance_id,
                            provider_id)
            SELECT
                j->>'id',
                NOW(),
                NOW(),
                (CASE
                    WHEN j->>'effectiveDateTime' IS NOT NULL
                        THEN CAST(j->>'effectiveDateTime' AS DATE)
                    WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                        THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                    ELSE NULL
                END),
                coding->>'display',
                p.id,
                {1},
                {2}
            FROM emr_patient p, {0}, jsonb_array_elements(j->'valueCodeableConcept'->'coding') coding 
            WHERE 
                p.natural_key=replace(j->'subject'->>'reference','Patient/','') AND
                (coding->>'display' ilike '%smok%' OR coding->>'display' ilike '%tobac%')
        """.format(tablename, provenance_id, provider_id)

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_labresults(tablename, provider_id, facility_provider_id, provenance_id):
    """Create lab results from Observation labs"""
    sql = """INSERT INTO emr_labresult(
                            natural_key, 
                            order_natural_key, 
                            created_timestamp,
                            updated_timestamp,
                            date, 
                            result_date, 
                            cresult_date, 
                            native_code,
                            native_name,
                            result_string,
                            result_float,
			    provider_id,
                            patient_id,
                            facility_provider_id,
                            provenance_id)
             SELECT 
                    CASE
                        WHEN j->>'id' IS NOT NULL THEN j->>'id'
                        ELSE to_char(NOW(), 'YYYYMMDD HH12:MI:SS')
                    END,
                    CASE
                        WHEN j->>'id' IS NOT NULL THEN j->>'id'
                        ELSE to_char(NOW(), 'YYYYMMDD HH12:MI:SS')
                    END,
                    NOW(),
                    NOW(),
                    (CASE
                        WHEN j->>'effectiveDateTime' IS NOT NULL
                            THEN CAST(j->>'effectiveDateTime' AS DATE)
                        WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                            THEN CAST(j->'effectivePeriod'->>'start' AS DATE)
                        ELSE NULL
                    END),
                    (CASE
                        WHEN j->>'effectiveDateTime' IS NOT NULL
                            THEN CAST(j->>'effectiveDateTime' AS TIMESTAMP)
                        WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                            THEN CAST(j->'effectivePeriod'->>'start' AS TIMESTAMP)
                        ELSE NULL
                    END),
                    (CASE
                        WHEN j->>'effectiveDateTime' IS NOT NULL
                            THEN to_char(CAST(j->>'effectiveDateTime' AS DATE), 'YYYYMMDD')
                        WHEN j->'effectivePeriod'->>'start' IS NOT NULL
                            THEN to_char(CAST(j->'effectivePeriod'->>'start' AS DATE), 'YYYYMMDD')
                        ELSE NULL
                    END),
                    (CASE
                        WHEN j->'code'->'coding'->0->>'code' IS NOT NULL
                            THEN CONCAT('--', j->'code'->'coding'->0->>'code')
                        WHEN j->'code'->'coding'->1->>'code' IS NOT NULL
                            THEN CONCAT('--', j->'code'->'coding'->1->>'code')
                        ELSE NULL
                    END),
                    (CASE
                        WHEN j->'code'->'coding'->0->>'display' IS NOT NULL
                            THEN CONCAT('--', j->'code'->'coding'->0->>'display')
                        WHEN j->'code'->'coding'->1->>'display' IS NOT NULL
                            THEN CONCAT('--', j->'code'->'coding'->1->>'display')
                        ELSE NULL
                    END),
                    (CASE
                        WHEN j->>'valueString' is not null THEN j->>'valueString'
                        WHEN j->'valueQuantity' is not null THEN j->'valueQuantity'->>'value'
                        WHEN j->'valueCodeableConcept' is not null THEN j->'valueCodeableConcept'->>'text'
                        ELSE NULL
                    END),
                    (CASE
                        WHEN j->>'valueString' is not null AND j->>'valueString' ~ '^[+-]?([0-9]*[.])?[0-9]+$' THEN 
                            CAST(j->>'valueString' AS FLOAT)
                        WHEN j->'valueQuantity' is not null AND j->'valueQuantity'->>'value' ~ '^[+-]?([0-9]*[.])?[0-9]+$' THEN 
                            CAST(j->'valueQuantity'->>'value' AS FLOAT)
                        WHEN j->'valueCodeableConcept' is not null AND j->'valueCodeableConcept'->>'text' ~ '^[+-]?([0-9]*[.])?[0-9]+$' THEN
                            CAST(j->'valueCodeableConcept'->>'text' AS FLOAT)
                        ELSE NULL
                    END),
		    {3},
                    p.id,
                    {1},
                    {2}
            FROM {0}, emr_patient p 
            WHERE 
                p.natural_key=replace(j->'subject'->>'reference','Patient/','') AND
                j->'category'->0->'coding'->0->>'code' = 'laboratory'
        """.format(tablename, facility_provider_id, provenance_id, provider_id)

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_prescriptions2(medreq_tablename, provider_id, managing_provider_id, facility_provider_id, provenance_id):
    """Create prescriptions from MedicationRequest"""
    sql = """INSERT INTO emr_prescription(
                            natural_key, 
                            order_natural_key, 
                            created_timestamp,
                            updated_timestamp,
                            date, 
                            patient_id,
                            provider_id,
                            managing_provider_id,
                            facility_provider_id,
                            provenance_id,
                            name,
                            code,
                            directions,
                            route,
                            dose,
                            frequency,
                            quantity,
                            quantity_float,
                            refills,
                            start_date,
                            end_date)
             SELECT 
                    CASE
                      WHEN mr.j->>'id' IS NOT NULL THEN mr.j->>'id'
                      ELSE to_char(NOW(), 'YYYYMMDD HH12:MI:SS')
                    END,
                    CASE
                      WHEN mr.j->>'id' IS NOT NULL THEN mr.j->>'id'
                      ELSE to_char(NOW(), 'YYYYMMDD HH12:MI:SS')
                    END,
                    NOW(),
                    NOW(),
                    CAST(mr.j->>'authoredOn' AS DATE), 
                    p.id,
                    {1},
                    {2},
                    {3},
                    {4},
                    CASE 
                        WHEN mr.j->'medicationCodeableConcept'->'coding'->0->>'display' IS NOT NULL
                        THEN mr.j->'medicationCodeableConcept'->'coding'->0->>'display'
                        ELSE mr.j->'medicationCodeableConcept'->>'text'
                    mr.j->'medicationCodeableConcept'->'coding'->0->>'code',
                    mr.j->'dosageInstruction'->0->>'text',
                    mr.j->'dosageInstruction'->0->'route'->>'text',
                    CASE
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'unit' IS NOT NULL AND
                        mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value', ' ',
                                   mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'unit')
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value' IS NOT NULL
                          THEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value'
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'unit' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'value', ' ',
                                   mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'unit')
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'value' IS NOT NULL
                          THEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'value'
                        ELSE NULL
                    END,
                    CASE
                        WHEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period' IS NOT NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'periodUnit' IS NOT NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency', ' times per ',
                                    mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period',' ',
                                    mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'periodUnit'
                                   )
                        WHEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period' IS NOT NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'periodUnit' IS NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency', ' times per ',
                                    mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period'
                                   )
                        WHEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period' IS NULL AND
                             mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency' IS NOT NULL
                          THEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency'
                        ELSE NULL
                    END,
                    mr.j->'dispenseRequest'->'quantity'->>'value',
                    CASE
                      WHEN mr.j->'dispenseRequest'->>'quantity' is not null AND
                                mr.j->'dispenseRequest'->>'quantity' ~ '^[+-]?([0-9]*[.])?[0-9]+$' 
                        THEN CAST(mr.j->'dispenseRequest'->>'quantity' AS FLOAT)
                      ELSE NULL
                    END,
                    mr.j->'dispenseRequest'->>'numberOfRepeatsAllowed',
                    CAST(mr.j->'dispenseRequest'->'validityPeriod'->>'start' AS DATE),
                    CAST(mr.j->'dispenseRequest'->'validityPeriod'->>'end' AS DATE)
             FROM {0} mr, emr_patient p 
             WHERE 
                p.natural_key=replace(mr.j->'subject'->>'reference','Patient/','')
        """.format(medreq_tablename,
                    provider_id, managing_provider_id, 
                    facility_provider_id, provenance_id)

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_prescriptions(medreq_tablename, med_tablename, provider_id, managing_provider_id, facility_provider_id, provenance_id):
    """Create prescriptions from MedicationRequest"""
    sql = """INSERT INTO emr_prescription(
                            natural_key, 
                            order_natural_key, 
                            created_timestamp,
                            updated_timestamp,
                            date, 
                            patient_id,
                            provider_id,
                            managing_provider_id,
                            facility_provider_id,
                            provenance_id,
                            name,
                            code,
                            directions,
                            route,
                            dose,
                            frequency,
                            quantity,
                            quantity_float,
                            refills,
                            start_date,
                            end_date)
             SELECT 
                    CASE
                      WHEN mr.j->>'id' IS NOT NULL THEN mr.j->>'id'
                      ELSE to_char(NOW(), 'YYYYMMDD HH12:MI:SS')
                    END,
                    CASE
                      WHEN mr.j->>'id' IS NOT NULL THEN mr.j->>'id'
                      ELSE to_char(NOW(), 'YYYYMMDD HH12:MI:SS')
                    END,
                    NOW(),
                    NOW(),
                    CAST(mr.j->>'authoredOn' AS DATE), 
                    p.id,
                    {2},
                    {3},
                    {4},
                    {5},
                    m.j->'code'->'coding'->0->>'display',
                    m.j->'code'->'coding'->0->>'code',
                    mr.j->'dosageInstruction'->0->>'text',
                    mr.j->'dosageInstruction'->0->'route'->>'text',
                    CASE
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'unit' IS NOT NULL AND
                        mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value', ' ',
                                   mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'unit')
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value' IS NOT NULL
                          THEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'doseQuantity'->>'value'
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'unit' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'value', ' ',
                                   mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'unit')
                        WHEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'value' IS NOT NULL
                          THEN mr.j->'dosageInstruction'->0->'doseAndRate'->0->'rateQuantity'->>'value'
                        ELSE NULL
                    END,
                    CASE
                        WHEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period' IS NOT NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'periodUnit' IS NOT NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency', ' times per ',
                                    mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period',' ',
                                    mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'periodUnit'
                                   )
                        WHEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period' IS NOT NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'periodUnit' IS NULL AND
                           mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency' IS NOT NULL
                          THEN CONCAT(mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency', ' times per ',
                                    mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period'
                                   )
                        WHEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'period' IS NULL AND
                             mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency' IS NOT NULL
                          THEN mr.j->'dosageInstruction'->0->'timing'->'repeat'->>'frequency'
                        ELSE NULL
                    END,
                    mr.j->'dispenseRequest'->'quantity'->>'value',
                    CASE
                      WHEN mr.j->'dispenseRequest'->>'quantity' is not null AND
                                mr.j->'dispenseRequest'->>'quantity' ~ '^[+-]?([0-9]*[.])?[0-9]+$' 
                        THEN CAST(mr.j->'dispenseRequest'->>'quantity' AS FLOAT)
                      ELSE NULL
                    END,
                    mr.j->'dispenseRequest'->>'numberOfRepeatsAllowed',
                    CAST(mr.j->'dispenseRequest'->'validityPeriod'->>'start' AS DATE),
                    CAST(mr.j->'dispenseRequest'->'validityPeriod'->>'end' AS DATE)
             FROM {0} mr, {1} m, emr_patient p 
             WHERE 
                p.natural_key=replace(mr.j->'subject'->>'reference','Patient/','') AND
                m.j->>'id' = replace(mr.j->'medicationReference'->>'reference', 'Medication/','')
        """.format(medreq_tablename, med_tablename, 
                    provider_id, managing_provider_id, 
                    facility_provider_id, provenance_id)

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def insert_new_dxcodes(tablename, fhir_codes):
    """Insert new dx codes into ESP static_dx_code table from Condition resource"""
    icd10_sys = fhir_codes['icd10-sys']
    icd9_sys = fhir_codes['icd9-sys']
    icd10 = []
    icd9 = []
    for k,v in icd10_sys.items(): 
        icd10.append(k)
    for k,v in icd9_sys.items(): 
        icd9.append(k)
    
    if len(icd10) > 0 or len(icd9) > 0:
        sql ="""INSERT INTO static_dx_code(combotypecode, code, type, name, longname)
            SELECT 
                CASE
                    WHEN to_jsonb(string_to_array(coding->>'system',';')) <@ '{1}'::jsonb 
                        THEN CONCAT('icd10:',coding->>'code') 
                    WHEN to_jsonb(string_to_array(coding->>'system',';')) <@ '{2}'::jsonb 
                        THEN CONCAT('icd9:',coding->>'code') 
                    ELSE NULL
                END,
                coding->>'code', 
                CASE
                    WHEN to_jsonb(string_to_array(coding->>'system',';')) <@ '{1}'::jsonb 
                        THEN 'icd10'
                    WHEN to_jsonb(string_to_array(coding->>'system',';')) <@ '{2}'::jsonb 
                        THEN 'icd9' 
                    ELSE NULL
                END,
                CASE
                    WHEN coding->>'display' IS NOT NULL
                        THEN left(coding->>'display', 150)
                    ELSE 'NONE'
                END,
                CASE
                    WHEN coding->>'display' IS NOT NULL
                        THEN coding->>'display'
                    ELSE 'NONE'
                END
              FROM {0}, jsonb_array_elements(j->'code'->'coding') coding
              WHERE 
                coding->>'display' IS NOT NULL AND
                coding->>'code' IS NOT NULL AND
                (to_jsonb(string_to_array(coding->>'system',';')) <@ '{1}'::jsonb OR
                to_jsonb(string_to_array(coding->>'system',';')) <@ '{2}'::jsonb)
              ON CONFLICT(combotypecode) DO NOTHING
            """.format(tablename,
                    json.dumps(icd10),
                    json.dumps(icd9),
                    ) 
    else: # for HDC (Colorado) 
        sql ="""INSERT INTO static_dx_code(combotypecode, code, type, name, longname)
            SELECT DISTINCT ON (j->'code'->'coding'->0->>'code')  
                CONCAT('icd10:',j->'code'->'coding'->0->>'code'), 
                    j->'code'->'coding'->0->>'code', 
                    'icd10', 
                    left(j->'code'->'coding'->0->>'display', 150),
                    j->'code'->'coding'->0->>'display'
              FROM {0}
              WHERE 
                j->'code'->'coding'->0->>'system' = 'http://hl7.org/fhir/sid/icd-10-cm' AND
                j->'code'->'coding'->0->>'code' IS NOT NULL
              ON CONFLICT(combotypecode) DO NOTHING
            """.format(tablename) 

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_encounter_dxcodes(tablename):
    """Create ESP encounter dx codes from FHIR Condition resource"""
    sql ="""INSERT INTO emr_encounter_dx_codes(encounter_id, dx_code_id)
            SELECT e.id,  CONCAT('icd10:',j->'code'->'coding'->0->>'code') 
            FROM emr_encounter e, {0}
            WHERE 
                j->'code'->'coding'->0->>'system' = 'http://hl7.org/fhir/sid/icd-10-cm' AND
                j->'code'->'coding'->0->>'code' IS NOT NULL AND
                e.natural_key=replace(j->'encounter'->>'reference','Encounter/', '')
            ON CONFLICT(encounter_id, dx_code_id) DO NOTHING
    """.format(tablename) 

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_encounter_dxcodes2(tablename, fhir_codes):
    """Create ESP encounter dx codes from FHIR Condition resource and encounter_condition table"""

    icd10_sys = fhir_codes['icd10-sys']
    icd9_sys = fhir_codes['icd9-sys']
    icd10 = []
    icd9 = []
    for k,v in icd10_sys.items(): 
        icd10.append(k)
    for k,v in icd9_sys.items(): 
        icd9.append(k)

    sql ="""INSERT INTO emr_encounter_dx_codes(encounter_id, dx_code_id)
            SELECT 
                e.id,  
                CASE
                    WHEN to_jsonb(string_to_array(coding->>'system',';')) <@ '{1}'::jsonb 
                        THEN CONCAT('icd10:',coding->>'code')
                    WHEN to_jsonb(string_to_array(coding->>'system',';')) <@ '{2}'::jsonb 
                        THEN CONCAT('icd9:',coding->>'code')
                    ELSE NULL 
                END
            FROM emr_encounter e, encounter_condition ec, {0}, jsonb_array_elements(j->'code'->'coding') coding
            WHERE
                e.natural_key=ec.encounter_id AND
                ec.condition_id=j->>'id' AND
                coding->>'code' IS NOT NULL AND
                (to_jsonb(string_to_array(coding->>'system',';')) <@ '{1}'::jsonb OR
                to_jsonb(string_to_array(coding->>'system',';')) <@ '{2}'::jsonb)
            ON CONFLICT ON CONSTRAINT emr_encounter_dx_codes_encounter_id_dx_code_id_5cd7e491_uniq DO NOTHING
    """.format(tablename,
               json.dumps(icd10),
               json.dumps(icd9),
               ) 

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def update_raw_enc_type(tablename):
    """Update emr_encounter raw_encounter_type"""
    sql = """UPDATE emr_encounter 
             SET raw_encounter_type = t0.raw_type
             FROM (SELECT j->>'id' as natural_key, j->'class'->>'code' as raw_type FROM {0}) t0
             WHERE emr_encounter.natural_key = t0.natural_key
             """.format(tablename)

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_encounter_conditions(tablename):
    """Create and load temp encounter_condition table"""
    with connection.cursor() as cursor:
        # create table
        sql1 = (
            """CREATE TABLE encounter_condition(
                encounter_id VARCHAR NOT NULL,
                condition_id VARCHAR NOT NULL,
                UNIQUE (encounter_id, condition_id))"""
            )
        cursor.execute(sql1)
        # load table
        sql2 = """INSERT INTO encounter_condition(encounter_id, condition_id)
                SELECT
                    j->>'id' AS encounter_id,
                    replace(diagnosis->'condition'->>'reference','Condition/','') as condition_id
                    FROM {0}, jsonb_array_elements(j->'diagnosis') diagnosis
                    ON CONFLICT ON CONSTRAINT encounter_condition_encounter_id_condition_id_key DO NOTHING
                """.format(tablename)
        cursor.execute(sql2)

        # drop table if there are no diagnosis conditions
        cursor.execute("SELECT count(*) from encounter_condition")
        row = cursor.fetchone()
        if row[0] > 0:
            log.info("Created and loaded encounter_conditions")
            return True
        else:
            drop_table('encounter_condition')
            return False



def create_encounters(tablename, provider_id, provenance_id):
    """Create ESP encounters with FHIR Encounters and Coverage."""
    sql = """INSERT INTO emr_encounter( 
                                        natural_key, 
                                        date, 
                                        raw_date, 
                                        hosp_admit_dt,
                                        hosp_dschrg_dt,
                                        patient_id, 
                                        created_timestamp,
                                        updated_timestamp,
                                        pregnant,
                                        provider_id, 
                                        provenance_id,
                                        raw_encounter_type,
                                        priority,
                                        primary_payer
                                        )
             SELECT DISTINCT ON (j->>'id')
                    j->>'id',
                    CAST(j->'period'->>'start' AS DATE),
                    to_char(CAST(j->'period'->>'start' AS DATE), 'YYYYMMDD'),
                    CAST(j->'period'->>'start' AS DATE),
                    CAST(j->'period'->>'end' AS DATE),
                    p.id,
                    NOW(),
                    NOW(),
                    false,
                    {1},
		    '{2}',
		    left(j->'class'->>'code', 100),
                    '',
                    tpp.primary_payer
            FROM {0}
            INNER JOIN emr_patient p
                ON p.natural_key=replace(j->'subject'->>'reference','Patient/','')
            LEFT JOIN temp_primary_payer tpp
                ON tpp.patient_natural_key = replace(j->'subject'->>'reference','Patient/','')
                AND CAST(j->'period'->>'start' AS DATE) >= tpp.start_date
                AND (CAST(j->'period'->>'start' AS DATE) <= tpp.end_date OR tpp.end_date IS NULL)
            ORDER BY j->>'id', CAST(j->'period'->>'start' AS DATE)
            """.format(tablename, 
                    provider_id, 
                    provenance_id)

    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def create_vital_encounters(provider_id, provenance_id):
    """Create encounters with AVG vitals"""
    sql = """INSERT INTO emr_encounter( 
                                        natural_key, 
                                        date, 
                                        raw_date, 
                                        patient_id, 
                                        created_timestamp,
                                        updated_timestamp,
                                        pregnant,
                                        provider_id, 
                                        provenance_id,
                                        temperature,
                                        raw_temperature,
                                        weight,
                                        raw_weight,
                                        height,
                                        raw_height,
                                        bmi,
                                        raw_bmi,
                                        bp_systolic,
                                        raw_bp_systolic,
                                        bp_diastolic,
                                        raw_bp_diastolic,
                                        priority,
                                        primary_payer
                                        )
             SELECT DISTINCT ON (v.patient_id, v.date)
                    MD5(v.patient_id || to_char(v.date, 'YYYYMMDD')),
                    v.date, 
                    to_char(v.date,'YYYYMMDD'), 
                    p.id, 
                    NOW(),
                    NOW(),
                    false,
                    {0},{1},
                    v.temperature, v.temperature,
                    v.weight, v.weight,
                    v.height, v.height,
                    v.bmi, v.bmi, 
                    v.bp_systolic, v.bp_systolic,
                    v.bp_diastolic, v.bp_diastolic,
                    '',
                    tpp.primary_payer
            FROM vital v
            INNER JOIN emr_patient p
                ON p.natural_key=replace(v.patient_id,'Patient/','')
            LEFT JOIN temp_primary_payer tpp
                ON tpp.patient_natural_key = replace(v.patient_id,'Patient/','')
                AND v.date >= tpp.start_date
                AND (v.date <= tpp.end_date OR tpp.end_date IS NULL)
            ORDER BY v.patient_id, v.date""".format(provider_id, provenance_id)
    with connection.cursor() as cursor:
        cursor.execute(sql)
    return True

def load_encounter_vitals(resourcefile, provider_id, provenance_id): 
    """Loads FHIR Observation AVG vitals into new ESP encounters."""

    tablename = 'observation'
    log.info("Loading table {0} with resource file: {1}".format(tablename, resourcefile))
    load_resource_table(tablename, resourcefile)
    log.info("Loading vitals table")
    create_vital_table()
    load_avg_systolic()
    load_avg_diastolic()
    load_avg_temperature()
    load_avg_height()
    load_avg_weight()
    load_avg_bmi()
    log.info("Creating encounters with AVG vitals")
    status = create_vital_encounters(provider_id, provenance_id)
    # drop tables
    if status:
        log.info("Dropping observation, vital, and coverge tables")
        drop_table('vital')
        drop_table('observation')
        drop_table('coverage')

def update_encounters(resourcefile):
    """Update ESP encounters raw_encounter_type with FHIR Encounter file"""
    tablename = 'encounter'
    log.info("Loading table {0} with resource file: {1}".format(tablename, resourcefile))
    load_resource_table(tablename, resourcefile)
    log.info("Updateing ESP encounters with raw_encounter_type...")
    status = update_raw_enc_type(tablename)
    if status: 
        log.info("Dropping table {}".format(tablename))
        drop_table(tablename)

def load_encounters(resourcefile, 
                    provider_id, 
                    provenance_id,
                    fhir_codes):
    """Loads FHIR Encounters into ESP encounters with FHIR Encounters and Coverage."""
    # load FHIR encounter resource file
    enc_tablename = 'encounter'
    log.info("Loading table {0} with resource file: {1}".format(enc_tablename, resourcefile))
    load_resource_table(enc_tablename, resourcefile)
    # load FHIR coverage resource file
    coverage_resourcefile = resourcefile.replace('vis', 'cvg')
    if not os.path.isfile(coverage_resourcefile):
        log.error("Error loading FHIR Coverage resource. File is not available.")
        return False
    cov_tablename = 'coverage'
    log.info("Loading table {0} with resource file: {1}".format(cov_tablename, coverage_resourcefile))
    load_resource_table(cov_tablename, coverage_resourcefile)
    # load temp_primary_payer table with FHIR coverage
    log.info("Loading temp_primary_payer table...")
    create_temp_primary_payer()
    load_temp_primarypayer(cov_tablename, fhir_codes)
    # create temp encounter_conditions table from Encounter resource diagnosis conditions (if available)
    create_encounter_conditions(enc_tablename) 
    log.info("Creating ESP encounters...")
    status = create_encounters(enc_tablename, 
                                provider_id, 
                                provenance_id)
    if status: 
        log.info("Dropping table {}".format(enc_tablename))
        drop_table(enc_tablename)

def load_labresults(resourcefile, provider_id, facility_provider_id, provenance_id):
    """Loads FHIR Observation labs into ESP labresult table"""

    tablename = 'observation_lab'
    log.info("Loading table {0} with resource file: {1}".format(tablename, resourcefile))
    load_resource_table(tablename, resourcefile)
    log.info("Creating ESP lab results...")
    status = create_labresults(tablename, provider_id, facility_provider_id, provenance_id)
    if status: 
        log.info("Dropping table {}".format(tablename))
        drop_table(tablename)

def load_socialhistory(resourcefile, provider_id, provenance_id, fhir_codes):
    """Loads FHIR Observation social history into ESP socialhistory and
    ESP encounter EDD"""

    tablename = 'observation_socialhistory'
    log.info("Loading table {0} with resource file: {1}".format(tablename, resourcefile))
    load_resource_table(tablename, resourcefile)
    log.info("Creating ESP social history...")
    status1 = create_socialhistory(tablename, provider_id, provenance_id, fhir_codes)
    log.info("Loading pregnancy edd table")
    create_preg_edd_table()
    load_preg_edd()
    log.info("Creating encounters with pregnancy EDD")
    status2 = create_preg_edd_encounters(provider_id, provenance_id)
    if status1 and status2: 
        log.info("Dropping table {}".format(tablename))
        drop_table(tablename)
        log.info("Dropping preg_edd and temp_primary_payer tables")
        drop_table('preg_edd')
        drop_table('temp_primary_payer')

def load_conditions(resourcefile, fhir_codes):
    """Loads FHIR Conditions into ESP emr_encounter table"""

    tablename = 'condition'
    log.info("Loading table {0} with resource file: {1}".format(tablename, resourcefile))
    load_resource_table(tablename, resourcefile)
    log.info("Inserting new dx codes from Conditions into static_dx_code table")
    insert_new_dxcodes(tablename, fhir_codes)
    encounter_condition_exists = table_exists('encounter_condition')
    if encounter_condition_exists:
        log.info("Creating ESP encounter dx codes from FHIR Condition resource and encounter_condition table...")
        status = create_encounter_dxcodes2(tablename, fhir_codes)
        if status:
            log.info("Dropping table {}".format('encounter_condition'))
            drop_table('encounter_condition')
            log.info("Dropping table {}".format(tablename))
            drop_table(tablename)
        else: log.error("Error creating ESP encounter dx codes.")
    else:
        log.info("Creating ESP encounter dx codes from FHIR Condition resource...")
        status = create_encounter_dxcodes(tablename)
        if status: 
            log.info("Dropping table {}".format(tablename))
            drop_table(tablename)
        else: log.error("Error creating ESP encounter dx codes.")

def load_prescriptions(resourcefile, provider_id, managing_provider_id, facility_provider_id, provenance_id):
    """Loads FHIR MedicationRequest & Medication resources into ESP emr_prescription table"""
    med_resourcefile = resourcefile.replace('med', 'md1')
    medication_exists = os.path.isfile(med_resourcefile)
    if medication_exists:
        ## load FHIR medication resource into a temp table
        med_resourcefile = resourcefile.replace('med', 'md1')
        med_tablename = 'medication'
        log.info("Loading table {0} with resource file: {1}".format(med_tablename, med_resourcefile))
        load_resource_table(med_tablename, med_resourcefile)
        ## load FHIR MedicationRequest resource into a temp table
        medrequest_tablename = 'medrequest'
        log.info("Loading table {0} with resource file: {1}".format(medrequest_tablename, resourcefile))
        load_resource_table(medrequest_tablename, resourcefile)
        ## load ESP prescriptions from FHIR MedicationRequest & Medication resources
        log.info("Creating ESP prescriptions...")
        status = create_prescriptions(medrequest_tablename, med_tablename, 
                                provider_id, managing_provider_id, 
                                facility_provider_id, provenance_id)
    else:
        ## load FHIR MedicationRequest resource into a temp table
        medrequest_tablename = 'medrequest'
        log.info("Loading table {0} with resource file: {1}".format(medrequest_tablename, resourcefile))
        load_resource_table(medrequest_tablename, resourcefile)
        ## load ESP prescriptions from FHIR MedicationRequest
        log.info("Creating ESP prescriptions...")
        status = create_prescriptions2(medrequest_tablename,
                                provider_id, managing_provider_id, 
                                facility_provider_id, provenance_id)
    if status:
        if medication_exists:
            log.info("Dropping table {}".format(med_tablename))
            drop_table(med_tablename)
        log.info("Dropping table {}".format(medrequest_tablename))
        drop_table(medrequest_tablename)
    else: log.error("Error creating ESP prescriptions.")

def load_encounter_primarypayer(resourcefile, fhir_codes):
    """Loads FHIR Coverage into ESP encounter table"""
    tablename = 'coverage'
    log.info("Loading table {0} with resource file: {1}".format(tablename, resourcefile))
    #load_resource_table(tablename, resourcefile)
    log.info("Loading ESP encounter primary payer...")
    status = load_primarypayer(tablename, fhir_codes)
    if status: 
        log.info("Dropping table {}".format(tablename))
        drop_table(tablename)

