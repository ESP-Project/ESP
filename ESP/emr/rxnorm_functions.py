import json
import urllib.request
from ESP.utils import log

def get_rxnorm_name(rxcui):
    '''Retrieves from the getRxNormName API the Name corresponding to the given ID, or None if not found.
    '''
    if not isinstance(rxcui, str):
        return None
    with urllib.request.urlopen(f'https://rxnav.nlm.nih.gov/REST/rxcui/{rxcui}.json') as conn:
        try:
            results_json = json.loads(conn.read())
            if 'idGroup' in results_json and 'name' in results_json['idGroup']:
                return results_json['idGroup']['name']
        except:
            log.warning(f'Could not parse results of https://rxnav.nlm.nih.gov/REST/rxcui/{rxcui}.json')


def get_rxnorm_id(ndc_id):
    '''Retrieves from the findRxcuiById API the Rxcui that matches the given NDC ID. If multiple are found, returns the one at the 0th index. Returns None if not found.
    '''
    if not isinstance(ndc_id, str):
        return None
    with urllib.request.urlopen(f'https://rxnav.nlm.nih.gov/REST/rxcui.json?idtype=NDC&id={ndc_id}') as conn:
        try:
            results_json = json.loads(conn.read())
            if 'idGroup' in results_json and 'rxnormId' in results_json['idGroup']:
                return results_json['idGroup']['rxnormId'][0]
        except:
            log.warning(f'Could not parse results of https://rxnav.nlm.nih.gov/REST/rxcui.json?idtype=NDC&id={ndc_id}')

def get_ndc_name(ndc):
    '''Retrieves from the getNDCStatus API the Name corresponding to the given NDC, or None if not found.
    '''
    if not isinstance(ndc, str):
        return None
    with urllib.request.urlopen(f'https://rxnav.nlm.nih.gov/REST/ndcstatus.json?ndc={ndc}') as conn:
        try:
            results_json = json.loads(conn.read())
            if 'ndcStatus' in results_json and 'conceptName' in results_json['ndcStatus']:
                return results_json['ndcStatus']['conceptName']
        except:
            log.warning(f'Could not parse results of https://rxnav.nlm.nih.gov/REST/ndcstatus.json?ndc={ndc}')


