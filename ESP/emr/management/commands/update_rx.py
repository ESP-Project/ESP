from django.core.management.base import BaseCommand
from ESP.static.models import Rxnorm, Ndc
from ESP.emr.models import Prescription
from ESP.emr.rxnorm_functions import get_ndc_name, get_rxnorm_name
from ESP.utils import log

def update_static_ndc(ndc_code, no_name):
    if len(ndc_code) != 11:
        log.warning("NDC code: {} is not 11 digits".format(ndc_code))
        return None
    try:
        # check if static_ndc already has updated name
        ndc = Ndc.objects.exclude(trade_name=no_name).get(ndc_code=ndc_code)
        return ndc.trade_name
    except Ndc.DoesNotExist:
        # get name from RxNorm API
        ndc_name = get_ndc_name(ndc_code)
        # update or create static_ndc
        if ndc_name:
            name_str = ndc_name[:200]
            ndc, created = Ndc.objects.update_or_create(
                                            ndc_code=ndc_code, 
                                            defaults={"trade_name":name_str})
            log.info("Updated static_ndc table with Rx name: {0}, for NDC code: {1} ".format(name_str, ndc_code))
            return ndc.trade_name
        else:
            log.info("No NDC name returned from RxNorm API for NDC code: {0}".format(ndc_code))
            return None


def update_static_rxnorm(rxcui, no_name):
    if not rxcui.isdigit():
        log.warning("RxNorm RxCUI: {} is not a digit".format(rxcui))
        return None
    try:
        # check if static_rxnorm already has updated name
        rxnorm = Rxnorm.objects.exclude(name=no_name).get(rxcui=rxcui)
        return rxnorm.name
    except Rxnorm.DoesNotExist:
        # get name from RxNorm API
        rxnorm_name = get_rxnorm_name(rxcui)
        # update or create static_rxnorm
        if rxnorm_name:
            name_str = rxnorm_name[:200]
            rxnorm, created = Rxnorm.objects.update_or_create(
                                            rxcui=rxcui, 
                                            defaults={"name":name_str})
            log.info("Updated static_rxnorm table with Rx name: {0}, for RxCUI: {1} ".format(name_str, rxcui))
            return rxnorm.name
        else:
            log.info("No RxNorm name returned from RxNorm API for RxCUI: {0}".format(rxcui))
            return None

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--ndc',
            action='store_true',
            help='Update emr_prescription and static_ndc tables with Rx names from RxNorm API',
        )
        parser.add_argument(
            '--rxnorm',
            action='store_true',
            help='Update emr_prescription and static_rxnorm tables with Rx names from RxNorm API',
        )
    def handle(self, *args, **options):
        no_name = "Name awaiting api lookup" 
        if options['ndc']:
            rx_no_name = Prescription.objects.filter(name=no_name,
                                                code__startswith="ndc:").order_by("code").distinct("code")
            for rx in rx_no_name:
                # update static_ndc
                ndc_name = None
                rx_code = rx.code
                ndc_code = rx_code.split(":")[1]
                if "-" in ndc_code:
                    c = ndc_code.split("-")
                    label4 = c[0] if len(c[0]) == 4 else None
                    label5 = c[0] if len(c[0]) == 5 else None
                    prod3 = c[1] if len(c[1]) == 3 else None
                    prod4 = c[1] if len(c[1]) == 4 else None
                    pkg1 = c[2] if len(c[2]) == 1 else None
                    pkg2 = c[2] if len(c[2]) == 2 else None
                    ndc11 = None
                    if label4 and prod4 and pkg2: # 4-4-2
                        ndc11 = "0" + label4 + prod4 + pkg2
                    elif label5 and prod3 and pkg2: # 5-3-2
                        ndc11 = label5 + "0" + prod3 + pkg2
                    elif label5 and prod4 and pkg1: # 5-4-1
                        ndc11 = label5 + prod4 + "0" + pkg1
                    if ndc11:
                        ndc_name = update_static_ndc(ndc11, no_name)

                elif len(ndc_code) == 11:
                    ndc_name = update_static_ndc(ndc_code, no_name)
                elif len(ndc_code) == 10:
                    # 5-3-2 (xxxxx-xxx-xx -> xxxxx-0xxx-xx (11-digit))
                    ndc11 = ndc_code[:5] + "0" + ndc_code[5:8] + ndc_code[8:]
                    ndc_name = update_static_ndc(ndc11, no_name)
                    # 5-4-1 (xxxxx-xxxx-x -> xxxxx-xxxx-0x)
                    if not ndc_name:
                        ndc11 = ndc_code[:5] + ndc_code[5:9] + "0" + ndc_code[9:]
                        ndc_name = update_static_ndc(ndc11, no_name)
                    # 4-4-2 (xxxx-xxxx-xx -> 0xxxx-xxxx-xx)
                    if not ndc_name:
                        ndc11 = "0" + ndc_code[:4] + ndc_code[4:8] + ndc_code[8:]
                        ndc_name = update_static_ndc(ndc11, no_name)

                # update emr_prescription
                if ndc_name:
                    name_str = ndc_name[:200]
                    Prescription.objects.filter(code=rx_code).update(name=name_str)
                    log.info("Updated emr_prescription table with NDC name {0}, for {1} ".format(name_str, rx_code))

        elif options['rxnorm']:
            rx_no_name = Prescription.objects.filter(name=no_name,
                                                code__startswith="rxn:").order_by("code").distinct("code")
            for rx in rx_no_name:
                # update static_rxnorm
                rxnorm_name = None
                rx_code = rx.code
                rxcui = rx_code.split(":")[1]
                if rxcui.isdigit():
                    rxnorm_name = update_static_rxnorm(rxcui, no_name)


                # update emr_prescriptions
                if rxnorm_name:
                    name_str = rxnorm_name[:200]
                    Prescription.objects.filter(code=rx_code).update(name=name_str)
                    log.info("Updated emr_prescription table with RxNorm name {0}, for {1} ".format(name_str, rx_code))

        else:
            self.stderr.write('Arguments must be included. See --help')

