'''
                                  ESP Health
                            EMR ETL Infrastructure
                         Bulk FHIR Extract File Loader


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics - http://www.commoninf.com
@contact: http://esphealth.org
@copyright: (c) 2021 Commonwealth Informatics
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

import collections
import datetime
import os
import pprint
import re
import socket
import string
import sys
import time
import traceback
import json
from decimal import Decimal
import multiprocessing as mp
import hashlib

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives

from django.db import transaction, connections
from django.utils.encoding import DjangoUnicodeDecodeError
from ESP.conf.models import VaccineCodeMap
from ESP.emr.base import SiteDefinition
from ESP.emr.management.commands.common import LoaderCommand
from ESP.emr.models import EtlError
from ESP.emr.models import Immunization
from ESP.emr.models import LabResult, LabInfo
from ESP.emr.models import Encounter, EncounterTypeMap
from ESP.emr.models import Patient
from ESP.emr.models import Prescription
from ESP.emr.models import Provenance
from ESP.emr.models import Provider
from ESP.emr.models import SocialHistory
from ESP.settings import ETL_MEDNAMEREVERSE, ROW_LOG_COUNT
from ESP.settings import SITE_NAME, EMAIL_SUBJECT_PREFIX, SERVER_EMAIL, ADMINS, LOAD_REPORT_DIR
from ESP.settings import USE_FILENAME_DATE, ICD10_SUPPORT
from ESP.static.models import Dx_code, hl7_vocab
from ESP.utils import date_from_str
from ESP.utils import ga_str_to_days
from ESP.utils import height_str_to_cm
from ESP.utils import log
from ESP.utils import weight_str_to_kg
from ESP.utils.utils import float_or_none
from ESP.utils.utils import sanitize_str
from ESP.utils.utils import str_remainder
from ESP.utils.utils import string_or_none
from ESP.utils.utils import truncate_str

from ESP.emr.fhir import Fhir
from ESP.emr.fhir_sql import load_resource_table, load_encounters, load_encounter_vitals
from ESP.emr.fhir_sql import load_labresults, load_conditions, load_prescriptions
from ESP.emr.fhir_sql import load_socialhistory, load_encounter_primarypayer, update_encounters

#
# Set global values that will be used by all functions
#
global UPDATED_BY, TIMESTAMP, UNKNOWN_PROVIDER
UPDATED_BY = 'load_fhir'
TIMESTAMP = datetime.datetime.now()
UNKNOWN_PROVIDER = Provider.objects.get(natural_key='UNKNOWN')
ETL_FILE_REGEX = re.compile(r'^fhir\D\D\D\.esp\.(\d\d)(\d\d)(\d\d\d\d)$')
DXPAT_REGEX = re.compile(r'''
((icd9\:) (E|V)? \d+ (\.\d+)?)  | (icd10\:[A-Z](\d{2}|\d[A-Z])(\.[A-Z0-9]+)?[A-Z]?$) | ([^\s]+)
''',
re.IGNORECASE | re.VERBOSE)
#The third group is the un-verified codes.  It would match non-icd code.  Should use it to note problems.

def fhir_valuesets():
    fhir_codes = hl7_vocab.objects.filter(codesys__in=['omb-race',
                                                        'omb-eth',
                                                        'snomed-prg',
                                                        'snomed-smk',
                                                        'snomed-bmi',
                                                        'loinc-vit',
                                                        'loinc-smk',
                                                        'loinc-alc',
                                                        'ucum-time',
                                                        'covg-type',
                                                        'icd10-sys',
                                                        'icd9-sys',
                                                        'rxnorm-sys',
                                                        'ndc-sys',
                                                        ])
    loinc_vit = {}
    loinc_smk = {}
    loinc_alc = {}
    snomed_smk = {}
    snomed_prg = {}
    snomed_bmi = {}
    omb_race = {}
    omb_eth = {}
    ucum_time = {}
    covg_type = {}
    icd10_sys = {}
    icd9_sys = {}
    rxnorm_sys = {}
    ndc_sys = {}
    for c in fhir_codes:
        if ":" in c.description:
            desc = c.description.split(":")
            status_type = desc[0]
            text = desc[1]
        else:
            status_type = None
            text = c.description
        if c.codesys == 'loinc-vit':
            loinc_vit[c.value] = status_type
        if c.codesys == 'loinc-smk':
            loinc_smk[c.value] = status_type
        if c.codesys == 'loinc-alc':
            loinc_alc[c.value] = status_type
        if c.codesys == 'snomed-smk':
            snomed_smk[c.value] = status_type
        if c.codesys == 'snomed-prg':
            snomed_prg[c.value] = status_type
        if c.codesys == 'snomed-bmi':
            snomed_bmi[c.value] = status_type
        if c.codesys =='omb-race':
            omb_race[c.value] = text
        if c.codesys =='omb-eth':
            omb_eth[c.value] = text
        if c.codesys =='ucum-time':
            ucum_time[c.value] = text
        if c.codesys =='covg-type':
            covg_type[c.value] = text
        if c.codesys =='icd10-sys':
            icd10_sys[c.value] = text
        if c.codesys =='icd9-sys':
            icd9_sys[c.value] = text
        if c.codesys =='rxnorm-sys':
            rxnorm_sys[c.value] = text
        if c.codesys =='ndc-sys':
            ndc_sys[c.value] = text
    return {'loinc-vit':loinc_vit, 
            'loinc-smk':loinc_smk, 
            'loinc-alc':loinc_alc, 
            'snomed-smk':snomed_smk, 
            'snomed-prg':snomed_prg, 
            'snomed-bmi':snomed_bmi, 
            'omb-race':omb_race, 
            'omb-eth':omb_eth, 
            'ucum-time':ucum_time,
            'covg-type':covg_type,
            'icd10-sys':icd10_sys,
            'icd9-sys':icd9_sys,
            'rxnorm-sys':rxnorm_sys,
            'ndc-sys':ndc_sys,
            }

def date_from_filepath(filepath):
    '''
    Extracts datestamp from ETL file path
    '''
    filename = os.path.basename(filepath)
    match = ETL_FILE_REGEX.match(filename)
    if not match:
        raise ValueError("{} does not match the expected structure {}".format(filepath, ETL_FILE_REGEX.pattern))
    month = int(match.groups()[0])
    day = int(match.groups()[1])
    year = int(match.groups()[2])
    return datetime.date(day=day, month=month, year=year)


def datestring_from_filepath(filepath):
    '''
    Extracts datestring, in YYYYMMDD format, from ETL file path
    '''
    filename = os.path.basename(filepath)
    match = ETL_FILE_REGEX.match(filename)
    if not match:
        raise ValueError("{} does not match the expected structure {}".format(filepath, ETL_FILE_REGEX.pattern))
    month = match.groups()[0]
    day = match.groups()[1]
    year = match.groups()[2]
    return year + month + day

def file_len(file):
    for i, _ in enumerate(file, 1):
        pass
    return i

class LoadException(Exception):
    '''
    Raised when there is a problem loading data into db
    '''


class BaseLoader(object):
    #
    # Caching Note
    #
    # Note: this is a primitive cache -- *all* lookups are cached, for the
    # duration of the script's run.  Thus it can consume considerable memory
    # when loading a large data set.  For a set with ~118k patients, Python
    # consumed 1.1G.  However, caching (plus some fine tuning of PostgreSQL, so
    # YMMV) on that same data set showed a 5x increase in load performance.  
    # 
    # If you cannot accept the cache's memory requirements, you can add
    # a preference toggle -- or file a ticket w/ the project requesting
    # the same.
    #
    __dx_code_cache = {}       # {combotypecode: dx_codes instance}
    __patient_cache = {}       # {patient_id: Patient instance}
    __provider_cache = {}      # {provider_id: Provider instance}
    __labOrd_cache = {}        # {order_natural_key: order instance}
    __labRes_cache = {}        # {lab_natural_key: result instance}
    __labSpec_cache = {}       # {specimen_num: Specimen instance}
    __labCLIA_cache = {}       # {CLIA_id: LabInfo instance}
    __encounter_cache = {}

    def __init__(self, filepath, options, codes):
        if not os.path.isfile(filepath):
            raise ValueError("{} is not a file".format(filepath))
        path, filename = os.path.split(filepath)
        self.filename = filename
        self.filepath = filepath
        file_handle = open(filepath)
        self.line_count = file_len(file_handle)
        self.sid = None
        self.codes = codes
        
        prov, created = Provenance.objects.get_or_create(source=filename)
        prov.wtimestamp = TIMESTAMP
        prov.hostname = socket.gethostname()
        prov.data_date = date_from_filepath(filepath)
        prov.status = 'attempted'
        prov.raw_rec_count = self.line_count
        if created:
            log.debug('Creating new provenance record #%s for %s' % (prov.pk, filename))
        else:
            log.debug('Updating existing provenance record #%s for %s' % (prov.pk, filename))
            #set counters to zero
            prov.valid_rec_count = 0 
            prov.insert_count = 0 
            prov.update_count = 0 
            prov.post_load_count = 0
            prov.error_count = 0 
            
        prov.save()
        
        # redmine #476  
        if self.line_count == 0:
            # send email to managers as high priority saying no data found
            report = ' found no data to load for #%s for %s' % (prov.pk, filename)
            log.info(report)
            msg = EmailMultiAlternatives(
               EMAIL_SUBJECT_PREFIX + ' Load_Epic Report for ' + SITE_NAME + ', Source file: ' + self.filename,
               report,
               SERVER_EMAIL, 
               [a[1] for a in ADMINS],
               )
            html_content = '<pre>\n%s\n</pre>' % report
            msg.attach_alternative(html_content, "text/html")
            msg.send()
      
      
        self.provenance = prov
        file_handle.seek(0) # Reset file position after counting lines
        self.reader = file_handle
        self.created_on = datetime.datetime.now()
        self.inserted = 0
        self.updated = 0
        self.control = 0
        self.options = options
        self.loading_errors = []

    def get_patient(self, natural_key):
        if not natural_key:
            raise LoadException('Called get_patient() with empty patient_id')
        if not natural_key in self.__patient_cache:
            try:
                p = Patient.objects.get(natural_key=natural_key)
            except Patient.DoesNotExist:
                p = Patient(
                    natural_key=natural_key,
                    provenance = self.provenance,
                    )
                p.save()
            self.__patient_cache[natural_key] = p
        return self.__patient_cache[natural_key]

    def get_encounter(self, natural_key):
        if not natural_key:
            raise LoadException('Called get_encounter() with empty natural_key')
        if not natural_key in self.__encounter_cache:
            try:
                e = Encounter.objects.get(natural_key=natural_key)
            except Encounter.DoesNotExist:
                log.warn('Encounter with natural key {} does not exist'.format(natural_key))
                raise
            self.__encounter_cache[natural_key] = e
        return self.__encounter_cache[natural_key]

    def get_provider(self, natural_key):
        
        if not natural_key:
            return UNKNOWN_PROVIDER

        #truncate the key some provider keys were too long
        natural_key = truncate_str(natural_key, 'natural_key', 128)

        if not natural_key in self.__provider_cache:
            try:
                p = Provider.objects.get(natural_key=natural_key)
            except Provider.DoesNotExist:
                p = Provider(natural_key=natural_key)
                p.provenance = self.provenance
                p.save()
            self.__provider_cache[natural_key] = p
        return self.__provider_cache[natural_key]

    def create_or_update_encounter(self, encounter, field_values, row):
        '''
        Creates a new encounter if the encounter object does not exist.
        If an encounter exists, update the encounter vital only if it's none/null.
        If the vital is not none, create a new encounter with the new vital.
        '''
        son = string_or_none
        dton = self.date_or_none
        up = self.up
        if row['natural_key']:
            # use observation encounter id
            field_values['natural_key'] = str(row['natural_key'])
            try:
                enc_obj = encounter.objects.get(natural_key = field_values['natural_key'])
                # get vital from observation
                vitals = ['bp_systolic','bp_diastolic','temperature']
                vital_name = None
                obs_vital_value = None
                # create fake encounter if existing encounter vital is not none/null
                for vital_name in vitals:
                    if vital_name in field_values and field_values[vital_name]:
                        if getattr(enc_obj, vital_name):
                            oid = str(row['observation_id']) if 'observation_id' in row else 'no-oid'
                            field_values['natural_key'] = enc_obj.natural_key + '-' + oid
                            field_values['date'] = enc_obj.date
                            field_values['raw_date'] = enc_obj.raw_date
                            field_values['hosp_admit_dt'] = enc_obj.hosp_admit_dt
                            field_values['hosp_dschrg_dt'] = enc_obj.hosp_dschrg_dt
                            field_values['patient_id'] = enc_obj.patient_id
                            field_values['provider_id'] = enc_obj.provider_id
                            field_values['provenance'] = self.provenance
                            field_values['encounter_type'] = enc_obj.encounter_type
                            field_values['raw_encounter_type'] = enc_obj.raw_encounter_type
                            field_values['priority'] = enc_obj.priority
                            break
            except ObjectDoesNotExist:
                pass
        else:
            # fake encounter for observations without an encounter id
            pid = str(row['patient_id']) if 'patient_id' in row else 'no-pid'
            oid = str(row['observation_id']) if 'observation_id' in row else 'no-oid'
            odt = str(row['observation_datetime']) if 'observation_datetime' in row else 'no-odt'
            id_str = pid + oid + odt
            field_values['natural_key'] = hashlib.md5(id_str.encode()).hexdigest()
            obs_datetime = row['observation_datetime']
            obs_date = obs_datetime.replace("-","")[0:8]
            field_values['date'] = dton(obs_date)
            field_values['raw_date'] = son(obs_date)
            field_values['patient'] = self.get_patient(row['patient_id'])
            field_values['provider'] = self.get_provider(None)
            field_values['provenance'] = self.provenance
            raw_enc_type = 'AMB'
            field_values['raw_encounter_type'] = raw_enc_type
            field_values['encounter_type'] = self.mapper_dict.get(up(raw_enc_type))[0].mapping
            field_values['priority'] = self.mapper_dict.get(up(raw_enc_type))[0].priority

        e, created = self.insert_or_update(encounter, field_values, ['natural_key']) 
        e.bmi = e._calculate_bmi()
        e.save()
        log.debug('Saved encounter vital: %s' % e)

    @transaction.atomic
    def insert_or_update(self, model, field_values, key_fields):
        '''
        Attempts to create a new instance of model using field_values.  If 
        create fails due to constraint (e.g. unique key), fetches existing 
        object using fields named in key_fields.
        
        @param model: Model to insert/update 
        @type model:  Django ORM Model
        @param field_values: Field names/values to create/update
        @type field_values:  Dict {field_name: value}
        @type key_fields: The field(s) to use as lookup key for updates
        @type key_fields: List [field_name, field_name, ...]
        @return: (obj, created)  Where obj is an instance of model, and created is boolean
        '''
        keys = {}
        for field_name in key_fields:
            keys[field_name] = field_values[field_name]
            del field_values[field_name]

        created = False
        try:
            obj = model.objects.get(**keys)
        except ObjectDoesNotExist:
            obj = model(**keys)
            created = True
        for field_name in field_values:
            setattr(obj, field_name, field_values[field_name])
        obj.save()

        if created:
            self.inserted += 1
        else:
            self.updated +=1
        return obj, created
    
    def date_or_none(self, str):
        #TODO: the date_from_str util needs to be beefed up to handle the various date strings in the NIST test data
        if not str:
            return None
        try:
            return date_from_str(str)
        except ValueError:
            return None
        
    def decimal_or_none(self, str):
        return Decimal(str) if str else None
        
    def capitalize(self, s):
        '''
        Returns a capitalized, Django-safe version of string s.  
        Returns None if s evaluates to None, including blank string.
        '''
        if s:
            return string.capwords( sanitize_str(s) )
        else:
            return None
        
    def up(self, s):
        '''
        Returns a all upper case string, . 
        Returns None if s evaluates to None, including blank string.
        '''
        if s:
            return sanitize_str(s).upper()
        else:
            return None

    def generateNaturalkey(self, natural_key):
        if not natural_key:
            log.info('Record has blank natural_key, which is required, creating new one')
            return str(int(time.time()*1000))
        else:
            return str(natural_key)
               
    def get_dx_code(self, code, code_type, name, cache):
    
        '''
        Given an diagnostic code and code type as strings, return a Dx_code model instance
        '''
        if not code:
            log.info("Dx code is empty")
            return None
        code = code.upper()
        match = DXPAT_REGEX.match(code_type + ':' + code)
        if match:
            dx_code = match.group()
            #BZ not sure why match.group() is being used here.  Isn't that the same as match?
            if not dx_code in cache:
                i, created = Dx_code.objects.get_or_create(combotypecode__exact=dx_code, defaults={
                     'combotypecode':dx_code, 'type':code_type,'code': code,'name':name + ' (Added by load_fhir.py)'})

                if created:
                    log.warning('Could not find dx code "%s" - creating new dx_code entry.' % dx_code  )
                cache[dx_code] = i

            return cache[dx_code]
        else:
            log.info('Could not extract dx code: "%s"' % code)
            return None


    def load(self, nprocs, loadsql=False):
        try:
            log.info('Loading file "%s" with %s' % (self.filepath, self.__class__))
            if loadsql:
                self.load_resource_file()
            else:
                self.load_file(nprocs)
            self.provenance.status = 'loaded' if not self.loading_errors else 'errors'
            self.provenance.valid_rec_count = self.line_count - (len(self.loading_errors) + self.control)
            self.provenance.error_count = len(self.loading_errors)
            self.provenance.post_load_count = self.prov_count()
    
            log.info('File %s loaded %s records with %s errors.' % (self.filepath, 
                                                                    self.provenance.valid_rec_count, 
                                                                    len(self.loading_errors)))
        except LoadException:
            log.critical('Exception loading file "%s":' % self.filepath)
            log.critical('\t%s' % traceback.format_exc())
            self.provenance.status = 'failure'
            self.provenance.comment = traceback.format_exc()
    
        self.provenance.save()
        for err in self.loading_errors:
            err.save()

        return "success" if self.provenance.status == 'loaded' else self.provenance.status


    def load_file(self, nprocs):
        self.control = 0
        start_time = datetime.datetime.now()

        # debug without processes - uncomment
        #for line_no, line in enumerate(self.reader, start=1):
        #    self.load_line(line_no, line, start_time)
        #return False

        connections.close_all()
        # this limits the amount of memory required
        qsize = 1000
        data_queue = mp.Queue(qsize)
        num_procs =  nprocs if nprocs else 1
        # start processes
        pool = []
        for i in range(0,num_procs):
            p = mp.Process(target=self.process_line, args=(data_queue, start_time))
            p.start()
            pool.append(p)
        # put lines in the queue
        # a line is put in the queue when it's not full, and is blocked when full
        for num_and_line in enumerate(self.reader, start=1):
            data_queue.put(num_and_line)

        for p in pool:
            p.join()

    def process_line(self, in_queue, start_time):
        ecount = 0
        max_ecount = 10
        while (True):
            try:
                # return an item if available, else raise queue.Empty exception
                line_no, line = in_queue.get(False)
                # load/update line in db
                self.load_line(line_no, line, start_time)
            except:
                # queue should be empty, but empty() method is not reliable 
                # because of multithreading/multiprocessing semantics - see mp docs.
                if in_queue.empty(): # is empty
                    ecount +=1
                else: # not empty, but should be
                    ecount = 0
                # end the process after empty() returns true multiple times
                if ecount > max_ecount:
                    return
                time.sleep(1)

    def load_line(self, cur_row, line, start_time):
        resource = json.loads(line)
        row = self.resource_row(resource)
        if len(self.fields) < len(row):
            e = 'Skipping row. More items in row than fields'
            self.loading_errors.append(EtlError(provenance = self.provenance,
                                                line = cur_row,
                                                err_msg = e,
                                                data = pprint.pformat(row),
                                                timestamp = datetime.datetime.now()))
            log.error('Skipping row %s. More than %s items in row.  Item count: %s '
                % (cur_row, len(self.fields) , len(row) ))
            if not self.options['load_with_errors']:
                raise LoadException(e)
            return None
        if not row:
            return None # Skip None objects
        for key in row:
            if row[key]:
                try:
                    if not key:
                        e = ('There is a value that does not correspond to any field in line: %s for value: %s' % (cur_row,row[key]))
                        self.loading_errors.append(EtlError(provenance = self.provenance,
                                                            line = cur_row,
                                                            err_msg = e,
                                                            data = pprint.pformat(row),
                                                            timestamp = datetime.datetime.now()))
                        log.error(e)
                        if not self.options['load_with_errors']:
                            raise LoadException(e)
                        return None
                    else:
                        row[key] = sanitize_str( row[key].strip() )
                except DjangoUnicodeDecodeError as e:
                    # Log character set errors to db
                    self.loading_errors.append(EtlError(provenance = self.provenance,
                                                        line = cur_row,
                                                        err_msg = str(e)[:512],
                                                        data = pprint.pformat(row),
                                                        timestamp = datetime.datetime.now()))

                    if not self.options['load_with_errors']:
                        raise LoadException(e)
                    return None
        # Load the data, with error handling
        if self.options['validate']:
            return None
        try:
            self.load_row(row)
        except Exception as e:
            # Log ETL errors to db
            self.loading_errors.append(EtlError(provenance = self.provenance,
                                                line = cur_row,
                                                err_msg = str(e)[:512],
                                                data = pprint.pformat(row),
                                                timestamp = datetime.datetime.now()))
            if not self.options['load_with_errors']:
                raise LoadException(e)
            log.error('Caught Exception:')
            log.error('  File: %s' % self.filename)
            log.error('  Line: %s' % cur_row)
            log.error('  Exception: \n%s' % traceback.format_exc())
            log.error(pprint.pformat(row))
            return None
        if (ROW_LOG_COUNT == -1) or (ROW_LOG_COUNT and (cur_row % ROW_LOG_COUNT == 0) ):
            now = datetime.datetime.now()
            elapsed_time = now - start_time
            elapsed_seconds = elapsed_time.total_seconds() or 1 # Avoid divide by zero on first few records if startup is quick
            rows_per_sec = float(cur_row) / elapsed_seconds
            log.info('Loaded %s of %s rows:  %s %s (%.2f rows/sec)' % (cur_row, self.line_count, now, self.filename, rows_per_sec))


    def prov_count(self):
        '''
        For a given provenance ID
        provides a count of records in the primary target table with that ID.
        NB: This is expensive for large DB tables.
        '''
        return self.model.objects.filter(provenance=self.provenance.provenance_id).count()


    def load_summary(self):
        '''
        Writes and optionally Emails a set of summary info for each file
        '''
        db_count = self.provenance.post_load_count
        report = '+' * 80 + '\n'
        report = report + 'Load summary for source file: ' + self.filename + '\n'
        report = report +  'Number of rows in source file: ' + str(self.provenance.raw_rec_count) + '\n'
        report = report +  'Number of rows processed without error: ' + str(self.provenance.valid_rec_count+self.control) + '\n'
        report = report +  'Number or rows processed with error: ' + str(self.provenance.error_count) + '\n'
        if self.provenance.error_count > 0:
            report = report +  '  Here is a list of data row errors:' + '\n'
            etlerrs = EtlError.objects.filter(provenance=self.provenance.provenance_id)
            for etlerror in etlerrs:
                report = report + 'On row ' + str(etlerror.line) + ', error message was: ' + etlerror.err_msg + '\n'
        report = report + '+' * 80 + '\n'
        report = report +  'Number of database rows created (inserted): ' + str(self.inserted) + '\n'
        report = report +  'Number of updates performed on existing rows: ' + str(self.updated) + '\n'
        report = report +  'Number of rows in the EMR table having the current Provenance ID ' + str(self.provenance.provenance_id) + ': ' + str(db_count) + '\n'
        # basic checks for conditions that shouldn't occur, with appropriate messages if they do
        if ((self.provenance.error_count + self.provenance.valid_rec_count+self.control) != self.provenance.raw_rec_count):
            report = report +  'The count of file rows does not equal the count of rows processed (error rows + valid rows). Review carefully -- this should not be possible.' + '\n'
        if (((self.inserted + self.updated) != self.provenance.valid_rec_count)):
            report = report +  'The number of rows processed without error does not equal the sum of rows created plus updates. Review carefully -- this should not be possible.' + '\n'
        if (db_count != self.provenance.valid_rec_count):
            report = report +  'The number of DB rows with the current provenance ID does not equal the number of rows processed without error.' + '\n'
            report = report +  '  This is possible if there are duplicate natural keys, meaning the same DB row gets updated more than once.' + '\n'
            report = report +  '  If your data was pulled with multiple updates per record in the same file, this can be acceptable.' + '\n'
            report = report +  '  If the natural key is being duplicated incorrectly, this is not acceptable.  Review carefully.'     + '\n'
        if self.options['email_admin_reports']:
            msg = EmailMultiAlternatives(
               EMAIL_SUBJECT_PREFIX + ' Load_Epic Report for ' + SITE_NAME + ', Source file: ' + self.filename,
               report,
               SERVER_EMAIL, 
               [a[1] for a in ADMINS],
               )
            html_content = '<pre>\n%s\n</pre>' % report
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        fpath=LOAD_REPORT_DIR
        log.info('Writing load report.' )
        efile=open(fpath+"load_fhir_report_for_"+self.filename+'.txt','w')
        efile.write(report)
        efile.close()
        
class PatientLoader(BaseLoader):
    
    fields = [
        'natural_key',            # 1
        'mrn',
        'last_name',
        'first_name',
        'middle_name',            # 5
        'address1',
        'address2',
        'city',
        'state',
        'zip',                    # 10
        'country',
        'areacode',
        'tel',
        'tel_ext',
        'date_of_birth',            # 15
        'gender',
        'race',
        'home_language',
        'ssn',
        'pcp_id',                  # 20
        'marital_stat',
        'religion',
        'aliases',
        'mother_mrn',
        'date_of_death',            # 25
        'center_id',
        'ethnicity',
        'mother_maiden_name',
        'last_update',
        'last_update_site',         # 30
        'suffix',
        'title',
        'remark',
        'income_level',
        'housing_status',           # 35
        'insurance_status',
        'birth_country',
        'vital_status',
        'next_appt_provider',
        'next_appt_date',           # 40
        'next_appt_fac_provider',   
        'gender_identity',
        'sex_orientation',
        'birth_sex',                 #44
        ]
    
    model = Patient
    
    def load_row(self, row):
        natural_key = self.generateNaturalkey(row['natural_key'])
        
        values = {
        'natural_key': natural_key,
        'provenance' : self.provenance,
        'mrn' : row['mrn'],
        'last_name' : string_or_none(row['last_name']),
        'first_name' : string_or_none(row['first_name']),
        'middle_name' : string_or_none(row['middle_name']),
        'pcp' : self.get_provider(row['pcp_id']),
        'address1' : string_or_none(row['address1']),
        'address2' : string_or_none(row['address2']),
        'city' : row['city'],
        'state' : row['state'],
        'zip' : row['zip'],
        'country' : row['country'],
        'areacode' : row['areacode'],
        'tel' : row['tel'],
        'tel_ext' : row['tel_ext'],
        'date_of_birth' : self.date_or_none(row['date_of_birth']),
        'cdate_of_birth' : row['date_of_birth'],
        'date_of_death' : self.date_or_none(row['date_of_death']),
        'cdate_of_death' : row['date_of_death'],
        'gender' : row['gender'],
        'race' : string_or_none(row['race']),
        'home_language' : string_or_none(row['home_language']),
        'ssn' : string_or_none(row['ssn']),
        'marital_stat' : string_or_none(row['marital_stat']),
        'religion' : string_or_none(row['religion']),
        'aliases' : string_or_none(row['aliases']),
        'mother_mrn' : row['mother_mrn'],
        'center_id' : row['center_id'],
        'ethnicity' :string_or_none(row['ethnicity']),
        'mother_maiden_name' : string_or_none(row['mother_maiden_name']),
        'last_update' : self.date_or_none(row['last_update']),
        'clast_update' : row['last_update'],
        'last_update_site' : string_or_none(row['last_update_site']),
        'suffix' : string_or_none(row['suffix']),
        'title' : string_or_none(row['title']),
        'remark' : string_or_none(row['remark']),
        'income_level': string_or_none(row['income_level']),
        'housing_status': string_or_none(row['housing_status']),
        'insurance_status': string_or_none(row['insurance_status']),
        'birth_country': string_or_none(row['birth_country']),
        'vital_status' : string_or_none(row['vital_status']),
        'next_appt_provider' : self.get_provider(row['next_appt_provider']),
        'next_appt_date' : self.date_or_none(row['next_appt_date']),
        'next_appt_fac_provider' : self.get_provider(row['next_appt_fac_provider']),
        'gender_identity' : string_or_none(row['gender_identity']),
        'sex_orientation' : string_or_none(row['sex_orientation']),
        'birth_sex' : string_or_none(row['birth_sex']),
        }
        
        p, created = self.insert_or_update(Patient, values, ['natural_key'])
        p.zip5 = p._calculate_zip5()
        p.save()
        p = self.get_patient(natural_key)
        log.debug('Saved patient object: %s' % p)

    def resource_row(self,resource):
        return Fhir.get_patient(self.fields, resource, self.codes, self.options['validate'])

class SocialHistoryLoaderSQL(BaseLoader):

    model = SocialHistory

    def load_resource_file(self):
        provenance = self.provenance
        provider = self.get_provider(None)
        fhir_codes = self.codes
        load_socialhistory(self.filepath, 
                        provider.id, 
                        provenance.provenance_id,
                        fhir_codes,)

class LabResultLoaderSQL(BaseLoader):

    model = LabResult

    def load_resource_file(self):
        provenance = self.provenance
        provider = self.get_provider(None)
        facility_provider = self.get_provider(None)
        load_labresults(self.filepath, 
                        provider.id, 
                        facility_provider.id,
                        provenance.provenance_id,)

class EncounterConditionLoaderSQL(BaseLoader):

    model = Encounter

    def load_resource_file(self):
        fhir_codes = self.codes
        load_conditions(self.filepath, 
                        fhir_codes,)

class EncounterCoverageLoaderSQL(BaseLoader):

    model = Encounter

    def load_resource_file(self):
        fhir_codes = self.codes
        load_encounter_primarypayer(self.filepath, 
                        fhir_codes,)

class LabResultLoader(BaseLoader):
    
    fields = [
        'patient_id',           # 1
        'mrn',                  # 2
        'order_natural_key',    # 3
        'order_date',           # 4
        'result_date',          # 5
        'provider_id',          # 6
        'order_type',           # 7
        'cpt',                  # 8
        'component',            # 9
        'component_name',       # 10
        'result_string',        # 11
        'normal_flag',          # 12
        'ref_low',              # 13
        'ref_high',             # 14
        'unit',                 # 15
        'status',               # 16
        'note',                 # 17
        'specimen_num',         # 18
        'impression',           # 19
        'specimen_source',      # 20
        'collection_date',      # 21
        'procedure_name' ,      # 22
        'natural_key',          # 23 added in 3
        'patient_class',        # 24 added in 3
        'patient_status',       # 25 added in 3
        'CLIA_ID', 
        'collection_date_end', 
        'status_date', 
        'interpreter', 
        'interpreter_id',       # 30 
        'interp_id_auth', 
        'interp_uid',
        'lab_method', 
        'ref_text',
        'facility_provider_id', # 35
        ]

    model = LabResult
    
    def get_LabCLIA(self, CLIA_ID):
        
        if not CLIA_ID in self._BaseLoader__labCLIA_cache:
            if not CLIA_ID:
                CLIA_ID=''
            i, created = LabInfo.objects.get_or_create(CLIA_ID__exact=CLIA_ID, defaults={
                'CLIA_ID': CLIA_ID, 'laboratory_name':' (Added by load_fhir.py)','provenance': self.provenance })
            self._BaseLoader__labCLIA_cache[CLIA_ID] = i
            if created:
                log.warning('Could not find CLIA ID "%s" - creating new entry.' % CLIA_ID)
                
        return self._BaseLoader__labCLIA_cache[CLIA_ID]
    
    def load_row(self, row):

        # check valid patient and naturual key
        if row['patient_id'] is None or row['natural_key'] is None:
            return None
        
        # set date based on the collection date or result date, or elt file name
        if not row['order_date']:
            if row['result_date']:
                date  = row['result_date']
            elif USE_FILENAME_DATE:
                log.info('Empty date not allowed, using date from the ETL file name')
                date = datestring_from_filepath(self.filename)
        else:
            date = row['order_date']
            
        # We use the first 70 characters of component, since some lab 
        # results (always types unimportant to ESP) have quite long 
        # component values, yet we need the native_code field to be a 
        # reasonable width for indexing.  
        component = string_or_none(row['component'])  
        component = truncate_str(component, 'Component field', 70)
            
        cpt = string_or_none(row['cpt'])  
        if component and cpt:
            native_code = cpt + '--' + component
        elif cpt:
            native_code=cpt + '--'
        elif component:
            native_code = '--' + component
        else:
            native_code = None
        if not row['natural_key']:
            natural_key = self.generateNaturalkey(row['order_natural_key'])
            if native_code:
                natural_key = natural_key.__str__() + native_code
        else:
            natural_key = self.generateNaturalkey(row['natural_key'])
            
        # if no order natural key then assign the same as natural key
        if not row['order_natural_key']:
            row['order_natural_key'] = natural_key   

        values = {
        'provenance' : self.provenance,
        'patient' : self.get_patient(row['patient_id']),
        'provider' : self.get_provider(row['provider_id']),
        'order_type' : string_or_none(row['order_type']),
        'mrn' : row['mrn'],
        'order_natural_key' : row['order_natural_key']  ,
        'date' : self.date_or_none(date),
        'result_date' : self.date_or_none(row['result_date']),
        'cresult_date': row['result_date'],
        'native_code' : native_code,
        'native_name' : string_or_none(row['component_name']),
        'result_string' : row['result_string'],
        'result_float' : float_or_none(row['result_string']),
        'ref_text' : row['ref_text'],
        'ref_low_string' : row['ref_low'],
        'ref_high_string' : row['ref_high'],
        'ref_low_float' : float_or_none(row['ref_low']),
        'ref_high_float' : float_or_none(row['ref_high']),
        'ref_unit' : string_or_none(row['unit']),
        'abnormal_flag' : row['normal_flag'],
        'status' : string_or_none(row['status']),
        'comment' : string_or_none(row['note']),
        'specimen_num' : string_or_none(row['specimen_num']),
        'impression' : string_or_none(row['impression']),
        'specimen_source' : string_or_none(row['specimen_source']),
        'collection_date' : self.date_or_none(row['collection_date']),
        'ccollection_date' : row['collection_date'],
        'procedure_name' : string_or_none(row['procedure_name']),
        'natural_key' : natural_key,
        'patient_class' : string_or_none(row['patient_class']),
        'patient_status' : string_or_none(row['patient_status']),
        'collection_date_end' : self.date_or_none(row['collection_date_end']),
        'ccollection_date_end' : row['collection_date_end'],
        'status_date' : self.date_or_none(row['status_date']),
        'cstatus_date' : row['status_date'],
        'interpreter' : string_or_none(row['interpreter']),
        'interpreter_id' : string_or_none(row['interpreter_id']),
        'interp_id_auth' : string_or_none(row['interp_id_auth']),
        'interp_uid' : string_or_none(row['interp_uid']),
        'CLIA_ID' : self.get_LabCLIA(row['CLIA_ID']),
        'lab_method' : string_or_none(row['lab_method']),
        'facility_provider': self.get_provider(row['facility_provider_id']),
         }

        lx, created = self.insert_or_update(LabResult, values, ['natural_key'])
        log.debug('Saved Lab Result Observation object: %s' % lx)

    def resource_row(self,resource):
        return Fhir.get_lab_observation(self.fields, resource, self.codes, self.options['validate'])

class EncounterClaimLoader(BaseLoader):
    fields = [
            'natural_key',
            'primary_payer',
            ]
    model = Encounter

    def load_row(self, row):
        natural_key = self.generateNaturalkey(row['natural_key'])
        if row['primary_payer'] and natural_key:
            values = {}
            values['natural_key'] = natural_key
            values['primary_payer'] = row['primary_payer']
            e, created = self.insert_or_update(Encounter, values, ['natural_key'])
            log.debug('Saved encounter primary payer: %s' % e)

    def resource_row(self,resource):
        return Fhir.get_encounter_claim(self.fields, resource, self.codes, self.options['validate'])

class EncounterConditionLoader(BaseLoader):
    fields = [
            'natural_key',
            'pregnant',
            'edd',
            'dx_code',
            ]
    model = Encounter

    def load_row(self, row):
        natural_key = self.generateNaturalkey(row['natural_key'])
        dton = self.date_or_none
        values = {}
        values['natural_key'] = natural_key
        code = None
        code_type = None
        code_text = ''
        for k,v in row.items():
            if k == 'pregnant':
                values['pregnant'] = True if v else False
            if k == 'edd':
                edd = dton(v) if v else None
                values['edd'] = edd
                values['raw_edd'] = edd
            if k == 'dx_code':
                dx_code = v.split(":") if v else None
                if dx_code and len(dx_code) > 2:
                    code_type = dx_code[0]
                    code = dx_code[1]
                    code_text = dx_code[2] if dx_code[2] else ''
        e, created = self.insert_or_update(Encounter, values, ['natural_key'])
        log.debug('Saved encounter condition: %s' % e)

        # add dx code
        if not created and e and code and code_type:
            e.dx_codes.clear()
            e.dx_codes.add(self.get_dx_code(code, code_type, code_text, self._BaseLoader__dx_code_cache))

    def resource_row(self,resource):
        return Fhir.get_encounter_conditions(self.fields, resource, self.codes, self.options['validate'])

class EncounterUpdateSQL(BaseLoader):

    model = Encounter

    def load_resource_file(self):
        update_encounters(self.filepath)

class EncounterLoaderSQL(BaseLoader):

    model = Encounter

    def load_resource_file(self):
        provenance = self.provenance
        provider = self.get_provider(None)
        fhir_codes = self.codes
        load_encounters(self.filepath,
                              provider.id, 
                              provenance.provenance_id,
                              fhir_codes,)

class EncounterVitalLoaderSQL(BaseLoader):

    model = Encounter

    def load_resource_file(self):
        provenance = self.provenance
        provider = self.get_provider(None)
        load_encounter_vitals(self.filepath,
                              provider.id, 
                              provenance.provenance_id)

class EncounterVitalLoader(BaseLoader):

    fields = [
        'natural_key',
        'observation_id',
        'observation_datetime',
        'patient_id',
        'temp',
        'weight',
        'height',
        'bp_systolic',
        'bp_diastolic',
        'o2_sat',           
        'peak_flow',
        'bmi',
        ]

    model = Encounter

    #this gets used for default encounter type mapping
    mapper_dict = collections.defaultdict(list)
    mapper_qs = EncounterTypeMap.objects.all()
    for encmap in mapper_qs:
        mapper_dict[encmap.raw_encounter_type].append(encmap)
    
    def load_row(self, row):
        son = string_or_none
        flon = float_or_none
        values = {}
        # get vitals
        for k,v in row.items():
            if v and k == 'temp':
                values['raw_temperature'] = son(v)
                values['temperature'] = flon(v)
            if v and k == 'bp_systolic':
                values['raw_bp_systolic'] =  son(v)
                values['bp_systolic'] =  flon(v)
            if v and k == 'bp_diastolic':
                values['raw_bp_diastolic'] =  son(v)
                values['bp_diastolic'] =  flon(v)
            if v and k == 'weight':
                values['raw_weight'] = son(v)
                values['weight'] = flon(v)
            if v and k == 'height':
                values['raw_height'] = son(v)
                values['height'] = flon(v)
            if v and k == 'bmi':
                values['raw_bmi'] = son(v)
        # create or update encounter with vitals
        self.create_or_update_encounter(Encounter, 
                                        values, 
                                        row)

    def resource_row(self,resource):
        return Fhir.get_encounter_observations(self.fields, resource, self.codes, self.options['validate'])

class EncounterLoader(BaseLoader):
    
    fields = [
        'patient_id',
        'mrn',
        'natural_key',
        'encounter_date',
        'is_closed',
        'date_closed',
        'provider_id',
        'site_natural_key',
        'site_name',       
        'event_type',
        'cpt',             
        'dxlist',
        'hosp_admit_dt',
        'hosp_dschrg_dt',
        'general_use1',
        'general_use2',
        'general_use3',
        'general_use4',
        'general_use5',
        'general_use6'
        ]

    model = Encounter

    #this gets used for default encounter type mapping
    mapper_dict = collections.defaultdict(list)
    mapper_qs = EncounterTypeMap.objects.all()
    for encmap in mapper_qs:
        mapper_dict[encmap.raw_encounter_type].append(encmap)

    def load_row(self, row):
        #overriding the dx code to unknown problem if it is empty
        if not row['dxlist'] or row['dxlist'] =='':
            row['dxlist'] = 'icd9:799.9'

        # Util methods
        cap = self.capitalize
        up = self.up
        son = string_or_none
        dton = self.date_or_none
        natural_key = self.generateNaturalkey(row['natural_key'])
        encounter_date = dton(row['encounter_date'])
        values = {
            'natural_key': natural_key,
            'provenance': self.provenance,
            'patient': self.get_patient(row['patient_id']),
            'mrn' : row['mrn'],
            'provider': self.get_provider(row['provider_id']),
            'date': encounter_date,
            'raw_date': son(row['encounter_date']),
            'site_natural_key': son( row['site_natural_key'] ),
            'raw_encounter_type': up(row['event_type']),
            'date_closed': dton(row['date_closed']),
            'raw_date_closed': son(row['date_closed']),
            'site_name': cap(row['site_name']),
            'cpt': son(row['cpt']),
            'hosp_admit_dt': dton(row['hosp_admit_dt']),
            'hosp_dschrg_dt': dton(row['hosp_dschrg_dt']),
            'general_use1': son(row['general_use1']),
            'general_use2': dton(row['general_use2']),
            'general_use3': son(row['general_use3']),
            'general_use4': dton(row['general_use4']),
            'general_use5': son(row['general_use5']),
            'general_use6': son(row['general_use6']),
            }

        #fill out encounter_type and priority from mapping table if it has a value
        if  row['event_type'] and not option_site:
            try:
                values['encounter_type'] = self.mapper_dict.get(up(row['event_type']))[0].mapping
                values['priority'] = self.mapper_dict.get(up(row['event_type']))[0].priority
            except:
                log.debug('Unable to map encounter type for row %s' % self.line_count)
        elif option_site:
            site = SiteDefinition.get_by_short_name(option_site)
            values['encounter_type'], values['priority'] = site.set_enctype(values['raw_encounter_type'], values['site_name'])

        if not self.options['validate']:
            e, created = self.insert_or_update(Encounter, values, ['natural_key'])
            #
            # dx Codes
            #
        
            if not created: # If updating the record, purge old dx list
                e.dx_codes.clear()
        
            # code_strings are separated by semi-colon
            # within a code string, the code and optional text are separated by white space 
            for code_string in row['dxlist'].strip().split(';'):
                code_string = code_string.strip()
                # find new code types
                type = code_string.find(':')
                if type >= 0:
                    code_type = code_string[:type].strip().lower()
                    if  code_type == 'icd10' and not ICD10_SUPPORT :
                        log.error('ICD10 codes are not allowed code %s' % code_string )
                        raise LoadException('ICD10 codes are not allowed code %s' % code_string)
            
                    if code_type in ['icd9','icd10']:
                        firstspace = code_string.find(' ',type)
                        if firstspace>type:
                            code = code_string[type+1:firstspace].strip()
                            diagnosis_text = code_string[firstspace:].strip()
                        else:
                            # no diagnosis text
                            code = code_string[type+1:].strip()
                        diagnosis_text = ''  
                    else:
                        log.info('Could not parse dx code string, unsupported dx type %s' % code_string)
                        continue
                # no new format : found, old icd9 format
                else:
                    code_type='icd9'
                    firstspace = code_string.find(' ')
                    if firstspace>= 0:
                        code = code_string[:firstspace].strip()
                        diagnosis_text = code_string[firstspace:].strip()
                    else:
                        # no diagnosis text
                        code = code_string
                        diagnosis_text = ''
                    
                if len(code) >= 1 :
                    e.dx_codes.add(self.get_dx_code(code, code_type, diagnosis_text, self._BaseLoader__dx_code_cache))
            e.save()
            log.debug('Saved encounter object: %s' % e)

    def resource_row(self,resource):
        return Fhir.get_encounter(self.fields, resource, self.codes, self.options['validate'])

class PrescriptionLoaderSQL(BaseLoader):

    model = Prescription

    def load_resource_file(self):
        provenance = self.provenance
        provider = self.get_provider(None)
        managing_provider = self.get_provider(None)
        facility_provider = self.get_provider(None)
        load_prescriptions(self.filepath, 
                        provider.id, 
                        managing_provider.id, 
                        facility_provider.id,
                        provenance.provenance_id,)

class PrescriptionLoader(BaseLoader):

    fields = [
        'patient_id',                  # 1
        'mrn',
        'order_natural_key',
        'provider_id',
        'order_date',                  # 5
        'status',
        'directions',                  # field 7
        'ndc',
        'drug_desc',                   # name, field 9
        'quantity',
        'refills',
        'start_date',
        'end_date',
        'route',                        # added in 3
        'dose',                         # 15  added in 3
        'patient_class',                # added in 3
        'patient_status',               # added in 3
        'managing_provider_id',         # 18
        'facility_provider_id',         # 19
        'frequency',
    ]
    
    model = Prescription

    def load_row(self, row, ):
        
        # check valid patient and naturual key
        if row['patient_id'] is None or row['order_natural_key'] is None or row['order_date'] is None:
            return None

        # check valid code and name
        if row['ndc'] is None or row['drug_desc'] is None:
            return None

        name = string_or_none(row['drug_desc'])
        directions = string_or_none(row['directions'])
        if not name:
            name = 'UNKNOWN (value supplied by ESP)'  
              
        natural_key = self.generateNaturalkey(row['order_natural_key'] + row['order_date'])
        
        # some data sources have dirty (and lengthy) data in the 'refills' field.
        # Truncate the field at 200 characters
        refills = string_or_none(row['refills'])
        refills = truncate_str(refills, 'refills', 200)                                     

        # some data sources have dirty (and lengthy) data in the 'quantity' field.
        # Truncate the field at 200 characters
        quantity = string_or_none(row['quantity'])
        quantity = truncate_str(quantity, 'quantity', 200) 
        
        qty_float = float_or_none(quantity)      
        qty_type = str_remainder(quantity, qty_float)                           

        values = {
        'provenance' : self.provenance,
        'order_natural_key' : row['order_natural_key'],
        'patient' : self.get_patient(row['patient_id']),
        'mrn' : row['mrn'],
        'provider' : self.get_provider(row['provider_id']),
        'natural_key' : natural_key,
        'date' : self.date_or_none(row['order_date']),
        'status' : string_or_none(row['status']),
        'name' : name,
        'directions' : directions,
        'code' : row['ndc'],
        'quantity' : quantity,
        'quantity_float' : qty_float,
        'refills' : refills,
        'start_date' : self.date_or_none(row['start_date']),
        'end_date' : self.date_or_none(row['end_date']),
        'route' : string_or_none(row['route']),
        'dose' : string_or_none(row['dose']),
        'patient_class' : string_or_none(row['patient_class']),
        'patient_status' : string_or_none(row['patient_status']),
        'quantity_type' : qty_type,
        'managing_provider': self.get_provider(row['managing_provider_id']),
        'facility_provider': self.get_provider(row['facility_provider_id']),
        'frequency' : string_or_none(row['frequency']),
        }
        p, created = self.insert_or_update(Prescription, values, ['natural_key'])
        log.debug('Saved prescription object: %s' % p)

    def resource_row(self,resource):
        return Fhir.get_medication(self.fields, resource, self.codes, self.options['validate'])

class ImmunizationLoader(BaseLoader):
    
    fields = [
        'patient_id', 
        'type', 
        'name',
        'date',
        'dose',
        'manufacturer',
        'lot',
        'natural_key',
        'mrn', #added in 3
        'provider_id', # added in 3
        'visit_date', # added in 3
        'imm_status', # added in 3
        'cpt_code', # added in 3
        'patient_class', # added in 3
        'patient_status' # added in 3
        ]
    
    model = Immunization
    
    def load_row(self, row):
        # check valid patient and naturual key
        if row['patient_id'] is None or row['natural_key'] is None:
            return None

        # check valid type and name
        if row['type'] is None or row['name'] is None:
            return None

        natural_key = self.generateNaturalkey(row['natural_key'])
        values = {
        'provenance' : self.provenance,
        'patient' : self.get_patient(row['patient_id']),
        'imm_type' : string_or_none(row['type']),
        'name' : string_or_none(row['name']),
        'date' : self.date_or_none(row['date']),
        'dose' : string_or_none(row['dose']),
        'manufacturer' : string_or_none(row['manufacturer']),
        'lot' : string_or_none(row['lot']),
        'natural_key' : natural_key,
        'mrn' : row['mrn'],
        'provider' : self.get_provider(row['provider_id']),
        'visit_date' : self.date_or_none(row['visit_date']),
        'imm_status' : string_or_none(row['imm_status']),
        'cpt_code' : string_or_none(row['cpt_code']),
        'patient_class' : string_or_none(row['patient_class']),
        'patient_status' : string_or_none(row['patient_status']),
        }
        if USE_FILENAME_DATE and not values['date'] :            
            log.info('Empty date not allowed, using date from the ETL file name')
            values['date'] = datetime.datetime.strptime(datestring_from_filepath(self.filename), "%Y%m%d").strftime("%Y-%m-%d") 

        i, created = self.insert_or_update(Immunization, values, ['natural_key'])
        log.debug('Saved immunization object: %s' % i)

        nativevax, vax_created = VaccineCodeMap.objects.get_or_create(
            native_code=string_or_none(row['type']), native_name=string_or_none(row['name']))
        if vax_created:
            log.debug('Saved new VaccineCodeMap entry: %s' %nativevax)

    def resource_row(self,resource):
        return Fhir.get_immunization(self.fields, resource, self.codes, self.options['validate'])

class SocialHistoryLoader(BaseLoader):

    fields = [
        'patient_id',
        'mrn',
        'tobacco_use',
        'alcohol_use',
        'date_noted',                # 5  added in 3
        'natural_key',               # 6  added in 3
        'provider_id',               # 7  added in 3 cch added
        'sex_partner_gender',        # 8  added in 3.4.13
        'alcohol_oz_per_week',       # 9  added in 3.4.13
        'ill_drug_use',              # 10 added in 3.4.13
        'sexually_active',           # 11 added in 3.4.13
        'birth_control_method',      # 12 added in 3.4.13
        'housing',
        'food',
        'utilities',
        'medical_transportation',
        'transportation',
        ]
    
    model = SocialHistory
    
    def load_row(self, row):
        # check valid patient and naturual key
        if row['patient_id'] is None or row['natural_key'] is None:
            return None

        if USE_FILENAME_DATE and not row['date_noted'] :
            log.info('Empty date not allowed, using date from the ETL file name')
            date = datestring_from_filepath(self.filename)
        else:
            date = row['date_noted']
            
        natural_key = self.generateNaturalkey(row['natural_key'])
        
        values = {
            'natural_key': natural_key,
            'provenance' : self.provenance,
            'date' : self.date_or_none(date),
            'patient' : self.get_patient(row['patient_id']),
            'mrn' : string_or_none(row['mrn']),
            'tobacco_use' : string_or_none(row['tobacco_use']),
            'alcohol_use' : string_or_none(row['alcohol_use']),
            'provider' : self.get_provider(row['provider_id']),
            'sex_partner_gender' : string_or_none(row['sex_partner_gender']),
            'alcohol_oz_per_week' : string_or_none(row['alcohol_oz_per_week']),
            'ill_drug_use' : string_or_none(row['ill_drug_use']),
            'sexually_active' : string_or_none(row['sexually_active']),
            'birth_control_method' : string_or_none(row['birth_control_method']),
            'housing' : string_or_none(row['housing']),
            'food' : string_or_none(row['food']),
            'utilities' : string_or_none(row['utilities']),
            'medical_transportation' : string_or_none(row['medical_transportation']),
            'transportation' : string_or_none(row['transportation']),
            }

        s, created = self.insert_or_update(SocialHistory, values, ['natural_key'])
        log.debug('Saved social history object: %s' % s)

    def resource_row(self,resource):
        return Fhir.get_socialhistory_observation(self.fields, resource, self.codes, self.options['validate'])

class Command(LoaderCommand):
    #
    # Parse command line options
    #
    help = 'Loads data from Bulk FHIR files'

    def add_arguments(self, parser):
        parser.add_argument('-l', '--load_with_errors', dest='load_with_errors' , action='store_true', default=False,
            help='Load skips bad input records, but does not fail')
        parser.add_argument('-e', '--email_admin_reports', dest='email_admin_reports' , action='store_true', default=False,
            help='Sends file-level load reports to administrator email list')
        parser.add_argument('--validate', dest='validate' , action='store_true', default=False,
            help='Validate Bulk FHIR files')
        parser.add_argument('--proc', type=int, help='The number of processes (threads) used to load data.')
        super(Command, self).add_arguments(parser)

    def handle(self, *fixture_labels, **options):
        self.folder_check()
        #
        # Sort files by type
        #
        input_filepaths = []
        global option_site
        option_site=None
        if options['single_file']:
            if not os.path.isfile(options['single_file']):
                sys.stderr.write('Invalid file path specified: %s' % options['single_file'])
            input_filepaths = [options['single_file']]
        else:
            if options['site_name']:
                option_site=options['site_name']
            dir_contents = os.listdir(options['input_folder'])
            dir_contents.sort()
            for item in dir_contents:
                if not ETL_FILE_REGEX.match(item):
                    log.debug('Invalid filename: %s' % item)
                    continue
                filepath = os.path.join(options['input_folder'], item)
                if not os.path.isfile(filepath):
                    continue
                input_filepaths.append(filepath)
        # number of processes
        nprocs = options['proc'] if options['proc'] else 1

        conf = [
            ('fhirmem', PatientLoader),
            ('fhirvis', EncounterLoaderSQL),
            # ('fhirenc', EncounterUpdateSQL), # only for updating raw_encounter_type with firvis(copy fhirvis to fhirenc)
            ('fhircnd', EncounterConditionLoaderSQL),
            ('fhirobs', EncounterVitalLoaderSQL),
            ('fhirres', LabResultLoaderSQL),
            ('fhirimm', ImmunizationLoader),
            ('fhirmed', PrescriptionLoaderSQL), # used by HDC
            ('fhirmdr', PrescriptionLoader),    # used by others
            ('fhirsoc', SocialHistoryLoaderSQL),
                ]

        loader = {}
        filetype = {}
        valid_count = {}
        error_count = {}
        load_order = []
        ft_sql = ['fhirobs', 'fhircnd', 'fhirmed', 'fhirres', 
                    'fhirvis', 'fhirsoc']
		 # 'fhirenc'] # only for updating raw_encounter_type
        for item in conf:
            load_order.append(item[0])
            loader[item[0]] = item[1]
            filetype[item[0]] = []
            valid_count[item[0]] = 0
            error_count[item[0]] = 0
        if options['reload']:
            log.warning('You specified --reload, so files will be reloaded if already in database')
        for filepath in input_filepaths:
            path, filename = os.path.split(filepath)
            if (options['reload'] == False) and Provenance.objects.filter(source=filename, status__in=('loaded', 'errors')):
                log.info('File "%s" has already been loaded; skipping' % filename)
                self.archive(options, filepath, 'success')
                continue
            try:
                filetype[filename.split('.')[0]] += [filepath]
            except KeyError:
                log.warning('Unrecognized file type: "%s"' % filename)
        log.debug('Files to load by type: \n%s' % pprint.pformat(filetype))
        #
        # Load data
        #
        fhir_codes = fhir_valuesets()
        for ft in load_order:
            filepath_list = filetype[ft]
            filepath_list.sort(key=lambda filepath: date_from_filepath(filepath))
            for filepath in filepath_list:
                loader_class = loader[ft]
                l = loader_class(filepath, options, fhir_codes) # BaseLoader child instance\
                loadsql = ft in ft_sql
                disposition = l.load(nprocs, loadsql)

                valid_count[ft] += l.provenance.valid_rec_count
                error_count[ft] += l.provenance.error_count

                self.archive(options, filepath, disposition, options['validate'])
                # Archive additional medication file (fhirmd1.esp.date) associated with load prescriptions
                if 'fhirmed' in filepath:
                    md1_file = filepath.replace('med', 'md1')
                    if os.path.isfile(md1_file):
                        self.archive(options, md1_file, disposition, options['validate'])

                # Archive additional coverage file (fhircvg.esp.date) associated with load encounters.
                if 'fhirvis' in filepath:
                    cvg_file = filepath.replace('vis', 'cvg')
                    if os.path.isfile(cvg_file):
                        self.archive(options, cvg_file, disposition, options['validate'])

                if disposition == 'failure':
                    sys.exit()
                l.load_summary()
        #
        # Print job summary
        #
        if not options['validate']:
            print('+' * 80)
            print('Valid records loaded:')
            pprint.pprint(valid_count)
            print('-' * 80)
            print('Errors:')
            pprint.pprint(error_count)
