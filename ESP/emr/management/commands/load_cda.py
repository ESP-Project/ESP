### for debugging
import pdb
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from ESP.emr.models import Diagnosis
from ESP.emr.models import Encounter
from ESP.emr.models import Immunization
from ESP.emr.models import LabResult
from ESP.emr.models import Patient
from ESP.emr.models import Prescription
from ESP.emr.models import Provenance
from ESP.emr.models import Pregnancy
from ESP.emr.models import SocialHistory
from ESP.conf.models import CdaError
from ESP.conf.models import CdaMapping
from ESP.conf.models import CdaLoadInstruction
from ESP.conf.models import CdaXpath
from ESP.conf.models import OpMapping
from ESP.settings import DATA_DIR
from ESP.static.models import Dx_code
from ESP.static.models import SnomedToIcd10
from ESP.utils import log
from ESP.utils.utils import float_or_none
from ESP.utils.utils import strip_int_text
from ESP.utils.utils import strip_float_text
from ESP.utils.utils import temp_str_to_c
from ESP.utils.utils import weight_str_to_kg
from ESP.utils.utils import height_str_to_cm
import multiprocessing as mp
import queue
import traceback
from django import db
import os
import shutil
from django.db import connection
import xml.etree.ElementTree as ET
from datetime import datetime
from enum import IntEnum
import socket
import time
import re
import pandas

# Session info for provenance
TIMESTAMP = datetime.now()
HOSTNAME = socket.gethostname()

# process-local variables
class ProcLocals():
    iProc = None
    sPatientUniversalId = ''
    patient = None
    patient_id = None
    provider = ''
    provenance = None
    provenance_id = None
    dRegx = None
    lValidationError = []
    lCdaError = []
    fileName = None
    valErrorsIgnorable = True
    tobaccoIncludeRegx = ""
    tobaccoExcludeRegx = ""
    encountersdict = {}

    def __init__(self, iProc, fileName):
        self.iProc = iProc
        self.fileName = fileName

class Command(BaseCommand):

    help = 'Import a set of CDA HL7 XML files and populate the emr tables with the data the CDAs contain.'

    ns = '{urn:hl7-org:v3}'

    def add_arguments(self, parser):
        parser.add_argument('--proc', type=int, help='The number of processes to be spawned')

    def handle(self, *fixture_labels, **options):
        count = options['proc']
        nProcess = 1
        if(count):
            nProcess = count
        print("processes: " + str(nProcess))
        self.importAll(nProcess)

    def importAll(self, nProcess):
        # for each file in the data/cda/incoming directory
        srcDir = DATA_DIR + 'cda/incoming'
        lFile = os.listdir(srcDir)
        lFile.sort()

        # put each file in a thread-safe queue
        qJob = mp.Queue()
        for xmlFile in lFile:
            qJob.put(xmlFile)
            """ for debugging (no process spawning) uncomment the following lines
            pl = ProcLocals(0, xmlFile)
            self.importFile(pl, srcDir)
            """

        #"""for debugging, comment out the following multiprocessing code to the end of the method
        # close database connections, each process will create its own connection
        db.connections.close_all()

        # launch a series of processes that pull filenames from 
        # the queue and import them
        lProcess = [mp.Process(target=self.importFilesFromQueue, args=(i, qJob, srcDir,)) for i in range(nProcess)]

        for proc in lProcess:
            proc.start()

        for proc in lProcess:
            proc.join()
        #"""

    def importFilesFromQueue(self, iProc, qFile, srcDir):

        cFail = 0;

        ### print("process: " + str(iProc) + " qFile size: " + str(qFile.qsize()))
        while(True):
            try:
                xmlFile = qFile.get(False)
                pl = ProcLocals(iProc, xmlFile)
                print('| '.join("%s: %s" % item for item in vars(pl).items()))
                self.importFile(pl, srcDir)

            except:
                # we got here because the queue was blocked or empty
                # Because of multithreading/multiprocessing semantics, qFile.empty() is not reliable
                if qFile.empty():
                    cFail += 1
                else:
                    cFail = 0
                ### print("CFAIL: " + str(cFail))
                if cFail > 4:
                    # assume the queue has been emptied, we are done
                    return
                # wait 1 seconds before we try again
                time.sleep(1)

    def importFile(self, pl, srcDir):
        ns = self.ns

        xmlFullFile = srcDir + '/' + pl.fileName
        root = None

        pl.lValidationError = []
        pl.lCdaError = []
        pl.valErrorsIgnorable = True

        try:
            # the universal patient id is the string following the last dash (-) in the filename and preceeding .xml
            lMatch = re.search('\-([^-]+)(\.xml)', pl.fileName)
            if (lMatch):
                pl.sPatientUniversalId = lMatch[1]

            # universal patient ids need to be >= 6 characters long and not contain dashes (-)
            if(len(pl.sPatientUniversalId) >= 6):
                try:
                    root = ET.parse(xmlFullFile)
                except Exception as ex:
                    self.saveException(pl, '**Error in importAll(), Could not parse XML file:', 'xmlFullFile', '', ex)
                    self.writeValidationLog(pl)
                    xmlDest = DATA_DIR + 'cda/error/' + pl.fileName
                    shutil.copyfile(xmlFullFile, xmlDest)
                    os.remove(xmlFullFile)
                    return

                pl.provenance, created = Provenance.objects.get_or_create(source=pl.fileName) 
                pl.provenance.hostname=HOSTNAME 
                pl.provenance.status='attempted'
                pl.provenance.timestamp=TIMESTAMP
                pl.provenance.save()
                pl.provenance_id = pl.provenance.provenance_id

                # get the provider name for this CDA
                pl.provider = 'Generic'
                node = root.find(f'./{ns}author/{ns}assignedAuthor/{ns}assignedAuthoringDevice/{ns}softwareName')
                if node is not None:
                    pl.provider = node.text
                else:
                    element = root.find(f'./{ns}id')
                    if 'assigningAuthorityName' in element.attrib.keys():
                        pl.provider = element.attrib['assigningAuthorityName']
                # try to match the provider we found with what is in our mapping tables
                lMapping = CdaMapping.objects.exclude(id=0).values('provider').distinct()
                for mapping in lMapping:
                    length = len(mapping['provider'])
                    if (len(pl.provider) >= length and mapping['provider'].lower() == pl.provider[:length].lower()):
                        pl.provider = mapping['provider']
                        break

                pl.dRegx = self.getProviderRegxDict(pl.provider)
                self.initTobaccoRegx(pl)

                # validate this CDA
                self.importFromTree(pl, root, True)
            else:  # missing universal patient id
                self.addValidationError(pl, '**Missing or invalid universal patient id: ' + pl.sPatientUniversalId)

            sDir = 'error/'
            # if the CDA is valid
            if((pl.valErrorsIgnorable or len(pl.lValidationError) == 0) and len(pl.lCdaError) == 0):
                # import this CDA
                self.importFromTree(pl, root, False)

                # move the file to the archive or error directory
                sDir = 'archive/'
                ### if(len(pl.lCdaError) > 0):
                if(len(pl.lCdaError) > 0 or len(pl.lValidationError) > 0):
                    sDir = 'error/'

            self.writeValidationLog(pl)

            # copy the file to the appropriate directory
            if(len(pl.lCdaError) > 0 or len(pl.lValidationError) > 0):
                xmlDest = DATA_DIR + 'cda/' + sDir + pl.fileName
                shutil.copyfile(xmlFullFile, xmlDest)

            # delete the file
            os.remove(xmlFullFile)

        except Exception as ex:
            self.saveException(pl, '**Error in importAll()', '', '', ex)
            self.writeValidationLog(pl)
            xmlDest = DATA_DIR + 'cda/error/' + pl.fileName
            shutil.copyfile(xmlFullFile, xmlDest)
            os.remove(xmlFullFile)

        finally:
            if root is not None:
                del root

    def writeValidationLog(self, pl):
        if(len(pl.lValidationError) > 0 or len(pl.lCdaError) > 0):
            logDest = DATA_DIR + 'cda/error/' + pl.fileName + '.error.log'
            file = open(logDest, "w")
            if(len(pl.lValidationError) > 0):
                file.write('****Validation errors found in ' + pl.fileName + ':\n')
                file.write('Provider: ' + pl.provider + '\n')
                file.writelines(pl.lValidationError)
            if(len(pl.lCdaError) > 0):
                file.write('****Exceptions found in ' + pl.fileName + ':\n')
                file.write('Provider: ' + pl.provider + '\n')
                for error in pl.lCdaError:
                    file.write(error.section + ':' + error.errMsg + ':' + error.data + '\n')
            file.close()


    def importFromTree(self, pl, root, validate):
        ns = self.ns

        try:
            # if the patient is already in the system
            # NOTE: patient id is stored in XML as follows, but this may vary between vendors
            # So the Customer global universal patient id comes as part of the filename
            # element = root.find(f'./{ns}recordTarget/{ns}patientRole/{ns}id')
            # pl.sPatientUniversalId = element.attrib['extension']
            db='pat'
            pl.patient, is_new_patient = Patient.objects.get_or_create(natural_key=pl.sPatientUniversalId, 
                defaults={'provenance':pl.provenance})
            pl.patient.save()

            # import Demographics
            db='demog'
            self.importDemographics(pl, root, validate)

            # import immunizations
            db='imm'
            self.importImmunizations(pl, root, validate)

            # import prescriptions
            db='med'
            self.importPrescriptions(pl, root, validate)

            # import encounters/visits
            db='enc'
            self.importEncounters(pl, root, validate)
            db='vit'
            self.importVitals(pl, root, validate)
            db='diag'
            self.importDiagnostics(pl, root, validate)

            # import social history
            db='soc'
            self.importSocialHistory(pl, root, validate)

            # import lab test results
            db='lab'
            self.importLabs(pl, root, validate)


        except Exception as ex:
            self.saveException(pl, 'Error in importFromTree()', db, '', ex)

    def getDate(self, pl, sDate, validate):
        if(sDate is None):
            return None
        dt = None
        sPreParse = sDate

        # look for a date range seperated by " - ", in which case we will use the part before the hyphen
        lMatch = re.search('^(.+)( - )(.+)$', sDate)
        ###print("lMatch:"+str(lMatch) + " lmatch1:" + str(lMatch1)+" date:"+sDate)
        if (lMatch): 
            sPreParse = lMatch[1]

        try:   
            dt = pandas.to_datetime(sPreParse, errors='raise').date()
        except Exception as ex:
            log.info(pl.fileName + " getDate() could not parse date: '" + sDate + "'")

        return dt

    def getOrCreateEncounter(self, pl, sDate, sEncDescription, validate):
        dt = self.getDate(pl, sDate, validate)
        sDate = dt.strftime('%Y%m%d')

        # find the first encounter for this date
        if len(pl.encountersdict)>0:
            for i in range(len(pl.encountersdict)):
                if pl.encountersdict[i].date == dt:
                    encounter = pl.encountersdict[i]
                    return encounter
        #if we have a real type, we will make a new enc, otherwise we will try to find one
        if sEncDescription=='Unknown Encounter':
            check4encs = Encounter.objects.filter(patient_id=pl.patient.id,date=dt).exclude(encounter_type='Unknown Encounter').order_by('id')
            for enc in check4encs:
                return enc
        #make one
        naturalKey = sDate + '-' + str(pl.patient.id) + '-' + sEncDescription
         # the encounter may have been created already in another process
        encounter, is_new = Encounter.objects.get_or_create(patient_id=pl.patient.id, natural_key=naturalKey, 
                date=dt, defaults={'provider_id': 1, 'provenance_id': pl.provenance_id,'raw_encounter_type': sEncDescription,
                                   'encounter_type': sEncDescription})
        encounter.save()
        return encounter

    def importPrescriptions(self, pl, root, validate):
        ns = self.ns

        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template = branch.find(f'./{ns}templateId').attrib['root']
            if template in ('2.16.840.1.113883.10.20.22.2.1.1','2.16.840.1.113883.10.20.22.2.1'):
                if template == '2.16.840.1.113883.10.20.22.2.1'  and not self.optionalEntries(template, branch): 
                    #TODO watch immunizations imported from table
                    pdb.set_trace()
                    self.importFromTable(pl, root, Prescription, branch, 
                        self.getProviderRegxList(pl.provider, 'prescripRegx'),
                        self.getFieldPositionDict(pl.provider, 'prescription'), validate)
                else:
                    self.importMedicationsFromMedicationEntries(pl, root, branch, validate)

    def importImmunizations(self, pl, root, validate):
        ns = self.ns

        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template = branch.find(f'./{ns}templateId').attrib['root']
            if template in ('2.16.840.1.113883.10.20.22.2.2.1','2.16.840.1.113883.10.20.22.2.2'):
                if template == '2.16.840.1.113883.10.20.22.2.2' and not self.optionalEntries(template, branch): 
                    pdb.set_trace()
                    #TODO watch immunizations imported from table
                    self.importFromTable(pl, root, Immunization, branch, 
                        self.getProviderRegxList(pl.provider, 'immunRegx'),
                        self.getFieldPositionDict(pl.provider, 'immunization'), validate)
                else:
                    self.importImmunizationsFromImmunizationEntries(pl, root, branch, validate)

    def importEncounters(self, pl, root, validate):
        ns = self.ns

        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template = branch.find(f'./{ns}templateId').attrib['root']
            if template in ('/.16.840.1.113883.10.20.22.2.22.1','2.16.840.1.113883.10.20.22.2.22'):
                if template == '2.16.840.1.113883.10.20.22.2.22' and not self.optionalEntries(template, branch): 
                    pdb.set_trace()
                    #TODO watch immunizations imported from table
                    self.importFromTable(pl, root, Encounter, branch,
                                         self.getProviderRegxList(pl.provider, 'encRegx'),
                                         self.getFieldPositionDict(pl.provider, 'encounter'), validate)
                else:
                    self.importEncounterFromEncounterEntries(pl, root, branch, validate)

    def importVitals(self, pl, root, validate):
        ns = self.ns

        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template = branch.find(f'./{ns}templateId').attrib['root']
            if template in ('2.16.840.1.113883.10.20.22.2.4.1','2.16.840.1.113883.10.20.22.2.4'):
                if template == '2.16.840.1.113883.10.20.22.2.4' and not self.optionalEntries(template, branch): 
                    pdb.set_trace()
                    #TODO watch immunizations imported from table
                    self.importVitalsFromTable(pl, root, branch,
                        self.getProviderRegxList(pl.provider, 'vitRegx'),
                        self.getFieldDict(pl.provider, 'vitals'), validate)
                else:
                    self.importVitalsFromVitalsEntries(pl, root, branch, validate)

    def importLabs(self, pl, root, validate):
        ns = self.ns

        #self.importLabResultsFromVitals(pl, root, f'./[{ns}title="Vital Signs"]',  
        #    self.getProviderRegxList(pl.provider, 'vitRegx'),
        #    pl.dRegx['nonVitalLab'], validate)

        #self.importLabResultsFromVitalsEntries(pl, root,  f'./[{ns}title="Vital Signs"]', pl.dRegx['nonVitalLab'], validate)

        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template =  branch.find(f'./{ns}templateId').attrib['root']
            if template in ('2.16.840.1.113883.10.20.22.2.3.1','2.16.840.1.113883.10.20.22.2.3'):
                if template == '2.16.840.1.113883.10.20.22.2.3' and not self.optionalEntries(template, branch): 
                    lregx = self.getProviderRegxList(pl.provider, 'labRegx')
                    lposdct =  self.getFieldPositionDict(pl.provider, 'laboratory')
                    pdb.set_trace()
                    #TODO watch lab results imported from table
                    self.importFromTable(pl, root, Labresult, branch, lregx, lposdct, validate)
                else:
                    self.importLabResultsFromResultsEntries(pl, root, branch, validate)
        

    def importSocialHistory(self, pl, root, validate):
        ns = self.ns

        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template =  branch.find(f'./{ns}templateId').attrib['root']
            if template == '2.16.840.1.113883.10.20.22.2.17':
                if not self.optionalEntries(template, branch): 
                    self.importFromTable(pl, root, SocialHistory, branch,
                        self.getProviderRegxList(pl.provider, 'socialRegx'),
                        self.getFieldPositionDict(pl.provider, 'social'), validate)
                else:
                    self.importSocFromSocEntries(pl, root, branch, validate)

    def importDiagnostics(self, pl, root, validate):
        ns = self.ns
        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        for branch in lBranch:
            if branch.find(f'./{ns}templateId') is None:
                continue
            template =  branch.find(f'./{ns}templateId').attrib['root']
            if template in ('2.16.840.1.113883.10.20.22.2.5.1','2.16.840.1.113883.10.20.22.2.5'):
                if template == '2.16.840.1.113883.10.20.22.2.5' and not self.optionalEntries(template, branch): 
                    self.importDiagnosticsFromProblems(pl, root, branch,
                            self.getProviderRegxList(pl.provider, 'diagProbRegx'), validate)
                else:
                    self.importDiagnosticsFromProblemsEntries(pl, root, branch, validate)

    def getBranchOrderingNodes(self, pl, root, branch, lFieldRegx, dataName, validate):
        ns = self.ns
        headXpath = f'./{ns}text/{ns}table/{ns}thead/{ns}tr'
        fieldXpath = f'./{ns}th'
        nodeXpath = f'./{ns}text/{ns}table/{ns}tbody/{ns}tr'
        lOrdering = None
        lNode = None
        lOrdering = self.getFieldOrdering(pl, root, branch, headXpath, fieldXpath, lFieldRegx, dataName, validate)
        if (len(lOrdering) > 0):
            lNode = branch.findall(nodeXpath)
        return lOrdering, lNode

    def optionalEntries(self, template, branch):
        ns = self.ns
        if template == '2.16.840.1.113883.10.20.22.2.22':
            #this is an encounter, check for encounter entries 
            check = branch.find(f'./{ns}entry/{ns}encounter/{ns}code').attrib['displayName']
            if check:
                return True
            else:
                return False
        if template == '2.16.840.1.113883.10.20.22.2.4':
            #These are vital sign. Check for entries.
            check = branch.find(f'./{ns}entry')
            if check:
                return True
            else:
                return False
        if template == '2.16.840.1.113883.10.20.22.2.2':
            #These are immunizations. Check for entries.
            check = branch.find(f'./{ns}entry/{ns}substanceAdministration')
            if check:
                return True
            else:
                return False
        if template == '2.16.840.1.113883.10.20.22.2.1':
            #These are medications. Check for entries.
            check = branch.find(f'./{ns}entry/{ns}substanceAdministration')
            if check:
                return True
            else:
                return False
        if template == '2.16.840.1.113883.10.20.22.2.3':
            #These are labresults. Check for entries.
            check = branch.find(f'./{ns}entry/{ns}organizer')
            if check:
                return True
            else:
                return False
        if template == '2.16.840.1.113883.10.20.22.2.17':
            #These are social history. Check for entries.
            check = branch.find(f'./{ns}entry')
            if check:
                return True
            else:
                return False
        if template == '2.16.840.1.113883.10.20.22.2.5':
            #These are problems. Check for entries.
            check = branch.find(f'./{ns}entry')
            if check:
                return True
            else:
                return False
 

    def getFieldOrdering(self, pl, root, branch, headXpath, fieldXpath, lFieldRegx, dataName, validate):
            # get the ordering of the data
            ### print('tree:' + ET.tostring(branch, encoding='unicode'))
            lFieldOrdering = []
            lNodeText = []
            headNode = branch.find(headXpath)
            if headNode:
                lNode = headNode.findall(fieldXpath)
                ### print('lNode: ' + str(lNode))
                if lNode:
                    for node in lNode:
                        lNodeText.append(node.text)

                    ### print('fieldRegx: '  + str(lFieldRegx))
                    for fieldRegx in lFieldRegx:
                        index = 0
                        for text in lNodeText:
                            ### print('node, index, regx: ' + text + ',' + str(index) + ',' + fieldRegx)
                            match = re.search(fieldRegx, text, re.IGNORECASE)
                            if(match):
                                lFieldOrdering.append((index, match.group()))
                                break
                            index += 1
                ### print('field ordering:' + str(lFieldOrdering))
                if (validate and len(lFieldOrdering) != len(lFieldRegx)):
                    log.error(pl.fileName + " getFieldOrdering(): list of required fields not found for " + dataName)
                    log.error('Required: ' + str(lFieldRegx))
                    log.error('Found: ' + str(lNodeText))
                    self.addValidationError(pl, 'getFieldOrdering(): list of required fields not found.', True)
                    self.addValidationError(pl, 'Required: ' + str(lFieldRegx), True)
                    self.addValidationError(pl, 'Found: ' + str(lNodeText), True)

            return lFieldOrdering

    def getValuesFromNode(self, pl, node, lFieldOrdering, dataName, validate):
        ns = self.ns

        lValue = len(lFieldOrdering) * [None]
        index = 0
        order = -1
        try: 
            lNodeObj = node.findall(f'./{ns}td')
            for orderName in lFieldOrdering:
                try:
                    lValue[index] = lNodeObj[orderName[0]].text.strip()
                    index += 1
                except Exception as ex:
                    return None
        except Exception as ex:
            if(validate):
                log.info(pl.fileName + " Missing data for " + dataName + ". Field '" + lFieldOrdering[index][1] 
                    + "' not found in data rows.")
            # if there is a missing required field, just skip this Node
            return None

        return lValue

    def importLabResultsFromVitals(self, pl, root, titleXpath, lFieldRegx, nonLabRegx, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='LabResultsFromVitals')
        except ObjectDoesNotExist:
            return

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, titleXpath, lFieldRegx, 'LabResult', validate)
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, 'LabResult', validate)

                if(validate):
                    pass
                else:
                    # lab results ordering is required to be:
                    # Date, Description ('Total Cholesterol'), Value ('55'), code ('LOINC: 4548-4')
                    # these 4 fields are not optional

                    # skip records with missing data
                    if((lValue is None) or len(lValue) < 4):
                        continue
                    try:
                        sDate = lValue[0]
                        description = lValue[1]
                        if(sDate is None or description is None):
                            continue
                        if (not re.search(nonLabRegx, description, re.IGNORECASE)):
                        
                            # the first 2 fields are used to create the naturalKey
                            dt = self.getDate(pl, lValue[0], validate)
                            lValue[0]=dt

                            if (not validate) and description and dt:
                                # create the natural key from the date and the description 
                                naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + description
                                # truncate so we fit into natural key length
                                naturalKey = naturalKey[0:127]

                                lr = LabResult.objects.filter(patient_id = pl.patient.id, 
                                    natural_key = naturalKey).first()

                                # if this record does not already exist
                                if(not lr):
                                    # create it
                                    ### print("new LabResult from vitals: " + naturalKey)
                                    lr, created = LabResult.objects.get_or_create(patient_id=pl.patient.id, 
                                        natural_key=naturalKey, date=dt, result_date=dt,
                                        defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                                lr.native_name = description
                                lr.native_code = lValue[3]
                                lr.result_string = lValue[2]
                                lr.save()

                    except Exception as ex:
                        self.saveException(pl, 'Error in importLabResultsFromVitals()', 'LabResult', str(lValue), ex)
                        return

    def importLabResultsFromVitalsEntries(self, pl, root, titleXpath, nonLabRegx, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='LabResultsFromVitalsEntries')
        except ObjectDoesNotExist:
            return

        diagCodeType = cdaLoad.codeType.lower()
        if(diagCodeType != 'icd10' and diagCodeType != 'snomed'):
            self.saveException(pl, 'Error in importLabResultsFromVitalsEntries(), unsupported code type', 
                'LabResults', diagCodeType, '')
            return

        # find the root node of the correct area
        branchXpath = f'./{ns}component/{ns}structuredBody/{ns}component/{ns}section'
        lBranch = root.findall(branchXpath)
        branch = None
        vitRoot = None
        if lBranch:
            for branch in lBranch:
                node = branch.find(titleXpath)
                if node:
                    vitRoot = branch
                    # for each entry in this Vital Signs section
                    lEntry = vitRoot.findall(f'./{ns}entry')
                    if lEntry:
                        for entry in lEntry:

                            # get the date 
                            node = entry.find(f'./{ns}organizer/{ns}component/{ns}observation/{ns}effectiveTime')
                            sDate = None
                            if node is not None and ('value' in node.attrib.keys()):
                                sDate = node.attrib['value']
 
                            dt = self.getDate(pl, sDate, False)
                            if(dt):
                                # get lab result code, description and results
                                node = entry.find(f'./{ns}organizer/{ns}component/{ns}observation/{ns}code')
                                nodeResult = entry.find(f'./{ns}organizer/{ns}component/{ns}observation/{ns}value')
                                ### print("keys:" + str(node.attrib.keys()))
                                if (node is not None and ('code' in node.attrib.keys()) and ('displayName' in node.attrib.keys())
                                        and nodeResult is not None and ('value' in nodeResult.attrib.keys()) 
                                        and ('unit' in nodeResult.attrib.keys())):
                                    code = node.attrib['code']
                                    description = node.attrib['displayName']
                                    result = nodeResult.attrib['value']
                                    unit = nodeResult.attrib['unit']

                                    # if this is not a vital sign we will treat as a lab result
                                    if (not re.search(nonLabRegx, description, re.IGNORECASE)):

                                        #if we are validating, this is sufficient
                                        if validate:
                                            continue

                                        ### print("lrve code:" + code)
                                        ### print("lrve name:" + description)

                                        # create the natural key from the date and the description 
                                        naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + code
                                        # truncate so we fit into natural key length
                                        naturalKey = naturalKey[0:127]

                                        lr = LabResult.objects.filter(patient_id = pl.patient.id, 
                                            natural_key = naturalKey).first()
                                        ### print("lrve naturalKey: " + naturalKey)

                                        # if this record does not already exist
                                        if(not lr):
                                            # create it
                                            ### print("new LabResult ProblemEntries: " + naturalKey)
                                            lr, created = LabResult.objects.get_or_create(patient_id=pl.patient.id, 
                                                natural_key=naturalKey, date=dt, result_date=dt,
                                                defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                                        lr.native_code = code
                                        lr.native_name = description
                                        lr.result_string = result
                                        lr.result_float = float_or_none(result)
                                        lr.ref_unit = unit
                                        lr.save()
                            else:
                                # bad or missing date  
                                pass

    def importLabResultsFromResultsEntries(self, pl, root, branch, validate):
        ns = self.ns

        # if this provider does not put this data in this section
        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='LabResultsFromResultsEntries')
        except ObjectDoesNotExist:
            return

        labRoot = branch
        # for each entry in this result section
        lEntry = labRoot.findall(f'./{ns}entry')
        for entry in lEntry:
            try:
                nodeDt = entry.find(f'./{ns}organizer/{ns}effectiveTime/{ns}low')
                sDate = None
                orddt = None
                if nodeDt is not None and ('value' in nodeDt.attrib.keys()):
                    sDate = nodeDt.attrib['value']
                    if(sDate):
                        orddt = self.getDate(pl, sDate, False)
                lComp = entry.findall(f'./{ns}organizer/{ns}component')
                for comp in lComp:
                    resdt = None
                    obs=comp.find(f'./{ns}observation')
                    sDate = self.getData(obs, f'./{ns}effectiveTime', 'value')
                    obsNative_code=self.getData(obs, f'./{ns}code', 'code')
                    if obsNative_code is None:
                        if validate:
                            log.warning('No native code for lab observation. Patient {}'.format(pl.patient.id))
                        continue
                    if(sDate):
                        resdt=self.getDate(pl, sDate, False)
                    if not resdt and not orddt:
                        if validate:
                            log.warning('No date for lab observation. Code: {}, Patient {}'.format(obsNative_code,
                                                                                                  pl.patient.id))
                        continue
                    elif validate:
                        continue
                    else:
                        dt = next(x for x in [orddt,resdt] if x is not None)
                    obsIdroot=self.getData(obs, f'./{ns}id','root')
                    obsIdext=self.getData(obs, f'./{ns}id','extension')
                    obsStatus=self.getData(obs, f'./{ns}statusCode', 'code')
                    obsNative_name=self.getData(obs, f'./{ns}code', 'displayName')
                    obsResStr=self.getData(obs, f'./{ns}value', 'value')
                    obsResUnit=self.getData(obs, f'./{ns}value', 'unit')
                    obsRefhigh=self.getData(obs, f'./{ns}referenceRange/{ns}observationRange/{ns}value/{ns}high', 'value')
                    obsReflow=self.getData(obs, f'./{ns}referenceRange/{ns}observationRange/{ns}value/{ns}low', 'value')
                    naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + obsNative_code
                    # truncate so we fit into natural key length
                    naturalKey = naturalKey[0:127]
                    lr, created = LabResult.objects.get_or_create(patient_id=pl.patient.id, 
                            natural_key=naturalKey, date=dt, result_date=resdt, 
                            defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                    lr.native_name = obsNative_name
                    lr.native_code = obsNative_code
                    # MSSQL barfs on a result_string >= 900 bytes (450 chars) 
                    # in size as this is the MSSQL maximum varchar() size of a row that 
                    # will be indexed, so we truncate it
                    lr.result_string = obsResStr
                    if lr.result_string and len(lr.result_string) > 448:
                        lr.result_string = (obsResStr[:442] + "...") 
                    lr.result_float = float_or_none(obsResStr)
                    lr.mrn = pl.patient.natural_key
                    lr.ref_unit = obsResUnit
                    lr.ref_low_string = obsReflow
                    lr.ref_low_float = float_or_none(obsReflow)
                    lr.ref_high_string = obsRefhigh
                    lr.ref_high_float = float_or_none(obsRefhigh)
                    lr.status = obsStatus
                    lr.save()
            except Exception as ex:
                self.saveException(pl, 'Error in importLabResultsFromResultsEntries()', 
                        'LabResult', str(pl.provenance_id), ex)
        return

    def importMedicationsFromMedicationEntries(self, pl, root, branch, validate):
        ns = self.ns

        medRoot = branch
        # for each entry in this result section
        lEntry = medRoot.findall(f'./{ns}entry')
        for entry in lEntry:
            try:
                obs=entry.find(f'./{ns}substanceAdministration')
                obsIdext=self.getData(obs, f'./{ns}id','extension')
                nodeDts = obs.findall(f'./{ns}effectiveTime')
                obsName=self.getData(obs, f'./{ns}consumable/{ns}manufacturedProduct/{ns}manufacturedMaterial/{ns}code', 'displayName')
                obsCode=self.getData(obs, f'./{ns}consumable/{ns}manufacturedProduct/{ns}manufacturedMaterial/{ns}code', 'code')
                if obsName is None:
                    obsTran=obs.find(f'./{ns}consumable/{ns}manufacturedProduct/{ns}manufacturedMaterial/{ns}code/{ns}translation')
                    if obsTran is not None:
                        obsName = obsTran.attrib['displayName']
                        obsCode = obsTran.attrib['code']
                for nodeDt in nodeDts:
                    dtlow, dthigh = self.getDateFromEffectiveTime(pl, nodeDt)
                    if dtlow is not None or dthigh is not None:
                        break
                if (dtlow is None and dthigh is None) or obsName is None:
                    if validate:
                        log.warning('No date or name for Medication entry. ID-ext: {}, Provenance {}'.format(obsIdext,
                            pl.provenance_id))
                    continue
                elif validate:
                    continue
                dt = next(x for x in [dtlow, dthigh] if x is not None)
                naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + obsCode
                # truncate so we fit into natural key length
                naturalKey = naturalKey[0:127]
                med, created = Prescription.objects.get_or_create(patient_id=pl.patient.id, 
                            natural_key=naturalKey, date=dt,  
                            defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                med.name = obsName
                med.start_date=dtlow
                med.end_date=dthigh
                med.code=obsCode
                med.save()
            except Exception as ex:
                self.saveException(pl, 'Error in importMedicationsFromMedicationEntries()', 
                        'Medications', str(pl.provenance_id), ex)
        return
            
    def importImmunizationsFromImmunizationEntries(self, pl, root, branch, validate):
        ns = self.ns

        immRoot = branch
        # for each entry in this result section
        lEntry = immRoot.findall(f'./{ns}entry')
        for entry in lEntry:
            try:
                obs=entry.find(f'./{ns}substanceAdministration')
                if obs is None:
                    continue
                obsCode=self.getData(obs, f'./{ns}consumable/{ns}manufacturedProduct/{ns}manufacturedMaterial/{ns}code', 'code')
                nodeDt = entry.find(f'./{ns}substanceAdministration/{ns}effectiveTime')
                sDate = None
                if nodeDt is not None and ('value' in nodeDt.attrib.keys()):
                    sDate = nodeDt.attrib['value']
                    if(sDate):
                        dt = self.getDate(pl, sDate, False)
                    if not dt:
                        if validate:
                            log.warning('No date for Imm observation. Code: {}, Patient {}'.format(obsCode,
                                                                                                  pl.patient.id))
                        continue
                    elif validate:
                        continue
                obsStatus=self.getData(obs, f'./{ns}statusCode', 'code')
                obsName=self.getData(obs, f'./{ns}consumable/{ns}manufacturedProduct/{ns}manufacturedMaterial/{ns}code', 'displayName')
                if obsName is None:
                    continue
                naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + obsCode
                # truncate so we fit into natural key length
                naturalKey = naturalKey[0:127]
                imm, created = Immunization.objects.get_or_create(patient_id=pl.patient.id, 
                            natural_key=naturalKey, date=dt,  
                            defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                imm.name = obsName
                imm.imm_type = obsCode
                imm.imm_status = obsStatus
                imm.save()
            except Exception as ex:
                self.saveException(pl, 'Error in importImmunizationsFromImmunizationEntries()', 
                        'Immunizations', str(pl.provenance_id), ex)
        return
            
    def importEncounterFromEncounterEntries(self, pl, root, branch, validate):
        ns = self.ns

        encRoot = branch
        # for each entry in this result section
        lEntry = encRoot.findall(f'./{ns}entry')
        enum = 0
        for entry in lEntry:
            try:
                obs=entry.find(f'./{ns}encounter')
                nodeID = obs.find(f'./{ns}id')
                obsIdroot = nodeID.attrib['root']
                obsIdext= nodeID.attrib['extension']
                nodeDt = obs.find(f'./{ns}effectiveTime')
                dtlow, dthigh = self.getDateFromEffectiveTime(pl, nodeDt)
                if not dtlow and not dthigh:
                    if validate:
                        log.warning('No date for Enc observation. ID-ext: {}, Provenance {}'.format(obsIdext,
                            pl.provenance_id))
                    continue
                elif validate:
                    continue
                else:
                    dt = next(x for x in [dtlow, dthigh] if x is not None)
                obsEncTyp=self.getData(obs, f'./{ns}code', 'displayName')
                if obsEncTyp is None:
                    obsEncTyp='Unknown Encounter'
                naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + obsEncTyp
                # truncate so we fit into natural key length
                naturalKey = naturalKey[0:127]
                enc, created = Encounter.objects.get_or_create(patient_id=pl.patient.id, 
                            natural_key=naturalKey, date=dt,  
                            defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                enc.raw_encounter_type = obsEncTyp
                enc.encounter_type = obsEncTyp
                enc.mrn = pl.patient.natural_key 
                enc.save()
                dEntries = obs.findall(f'.{ns}/entryRelationship')
                for diag in dEntries:
                    if diag.find(f'./{ns}act/{ns}code').attrib['displayName']=='Diagnosis':
                        diagcode=diag.find(f'./{ns}act/{ns}entryRelationship/{ns}observation/{ns}value').attrib['code']
                        codetype=diag.find(f'./{ns}act/{ns}entryRelationship/{ns}observation/{ns}value').attrib['codeSystem']
                        if codetype=='2.16.840.1.113883.6.96':
                            dxCode = self.icdFromSnomed(diagcode)
                            encounterDxCode = enc.dx_codes.filter(combotypecode=dxCode.combotypecode)
                            if(not encounterDxCode.exists()):
                                enc.add_diagnosis(dxCode.type, dxCode.combotypecode)
                            # if this was a snomed we mapped to 'Unknown'
                            if (dxCode.code == '799.9'):
                                naturalKey = "enc " + str(enc.id) + ":diag " + diagcode
                                diagnosis, created = Diagnosis.objects.get_or_create(
                                    natural_key=naturalKey, date=dt, mrn=enc.mrn, code=diagCode, 
                                    encounter_id=enc.id, patient_id=enc.patient_id, codeset='snomed',
                                    defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                                diagnosis.save()
                        else:
                            log.warning('Enc diagnosis not SNOMED. Id-ext: {}, Provenance {}'.format(
                                obsIdext, pl.provenance_id))
                    else:
                        log.warning('Enc entryRelationship section but no diagnoses. Id-ext: {}, Provenance {}'.format(
                            obsIdext, pl.provenance_id))
                enc.save()
                pl.encountersdict[enum]=enc
                enum += 1
            except Exception as ex:
                self.saveException(pl, 'Error in importEncounterFromEncounterEntries()', 
                        'Encounters', str(pl.provenance_id), ex)
        return
            
    def importDiagnosticsFromProblems(self, pl, root, branch, lFieldRegx, validate):
        ns = self.ns

        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='DiagnosticsFromProblems')
        except ObjectDoesNotExist:
            return
        
        diagCodeType = cdaLoad.codeType.lower()
        diagRegx = cdaLoad.regx
        if(diagCodeType != 'icd10' and diagCodeType != 'snomed'):
            self.saveException(pl, 'Error in importDiagnosticsFromProblems(), unknown code type', 'Diagnostics', diagCodeType, ex)

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, branch, lFieldRegx, 'Diagnostics', validate)
        ### print("ordering: " + str(lFieldOrdering))
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, 'Diagnosis', validate)

                if(validate):
                    pass
                else:
                    # Date, Codes (snomed or icd10)
                    # these 2 fields are not optional

                    # skip records with missing data
                    if((lValue is None) or len(lValue) < 2):
                        continue
                    try:
                        # if this 'Problem' is a diagnosis
                        problemDate = lValue[0]                   
                        problemCode = lValue[1]
                        lMatch = re.search(diagRegx, problemCode, re.IGNORECASE)
                        if(lMatch and lMatch[1]):
                            diagCode = lMatch[1].upper()
                            # if an icd10 code of interest
                            if(diagCodeType == 'icd10'):
                                dxCode = Dx_code.objects.filter(code=diagCode, type='icd10').first()
                            # else if a snomed code of interest
                            elif(diagCodeType == 'snomed'):
                                dxCode = self.icdFromSnomed(diagCode)
                            # else -- code we don't know how to process
                            else:
                                self.saveException(pl, 'Unknown Diagnosis code type in importDiagnosticsFromProblems()', '', diagCodeType, ex)
                                continue

                            # at this point we should have found an ICD10 or the ICD9 of 799.9
                            if not dxCode:
                                self.saveException(pl, 'Error in importDiagnosticsFromProblems(), icd9/icd10 code not present in static_dx_code', 
                                    'Diagnostics', '799.9/'+diagCode, ex)
                                continue

                            dt = self.getDate(pl, problemDate, False)
                            if(dt):
                                self.addDiagnosis(pl, diagCodeType, diagCode, dt, dxCode)

                    except Exception as ex:
                        self.saveException(pl, 'Error in importDiagnosticsFromProblems()', 'Diagnostics', str(lValue), ex)
                        return

    def getDateFromEffectiveTime(self, pl, nodeDt):
        ns = self.ns
        dtlow = None
        dthigh = None
        if nodeDt is not None and ('value' in nodeDt.attrib.keys()):
            sDate = nodeDt.attrib['value']
            if(sDate):
                dtlow = self.getDate(pl, sDate, False)
        elif nodeDt is not None:
            nodeDtLow = nodeDt.find(f'./{ns}low')
            nodeDtHigh = nodeDt.find(f'./{ns}high')
            if nodeDtLow is not None and ('value' in nodeDtLow.attrib.keys()):
                sDate = nodeDtLow.attrib['value']
                if(sDate):
                    dtlow = self.getDate(pl, sDate, False)
            if nodeDtHigh is not None and ('value' in nodeDtHigh.attrib.keys()):
                sDate = nodeDtHigh.attrib['value']
                if(sDate):
                    dthigh = self.getDate(pl, sDate, False)
        return dtlow, dthigh       

    def addDiagnosis(self, pl, diagCodeType, diagCode, date, dxCode):
        encounter = self.getOrCreateEncounter(pl, str(date), 'Unknown Encounter', False)

        ### print ("add diagnosis date: " + str(date) + " code: " + str(dxCode))
        # if we don't already have this diagnosis for this encounter, add one 
        encounterDxCode = encounter.dx_codes.filter(combotypecode=dxCode.combotypecode)
        if(not encounterDxCode.exists()):
            try:
                encounter.add_diagnosis(dxCode.type, dxCode.combotypecode)

                # if this was a snomed we mapped to 'Unknown'
                if (diagCodeType == 'snomed' and dxCode.code == '799.9'):
                    ### print('Adding a diagnosis for an unmapped snomed ')
                    # add this to the emr_diagnosis table if it doesn't already exist
                    naturalKey = "enc " + str(encounter.id) + ":diag " + diagCode
                    diagnosis = Diagnosis.objects.filter(natural_key=naturalKey).first()
                    if(diagnosis is None):
                        diagnosis, created = Diagnosis.objects.get_or_create(
                            natural_key=naturalKey, date=date, mrn=encounter.mrn, code=diagCode, 
                            encounter_id=encounter.id, patient_id=encounter.patient_id, codeset='snomed',
                            defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})
                        ### print("new Diagnosis: " + naturalKey)
                        diagnosis.save()
            except Exception as ex:
                pass
                # we get here if we clash with another thread that is adding the same diagnosis
                # to the same encounter
        return

    def importSocFromSocEntries(self, pl, root, branch, validate):
        ns = self.ns

        socRoot = branch
        # for each entry in this problem section
        lEntry = socRoot.findall(f'./{ns}entry')
        if lEntry:
            for entry in lEntry:
                # find smoking status
                nodeCode = entry.find(f'./{ns}observation/{ns}code')
                if nodeCode is not None and ('displayName' in nodeCode.attrib.keys()
                    and  re.search(pl.tobaccoIncludeRegx, nodeCode.attrib['displayName'], re.IGNORECASE)):
                # get the date 
                    nodeDt = entry.find(f'./{ns}observation/{ns}effectiveTime')
                    sDate = None
                    dt = None
                    if nodeDt is not None and ('value' in nodeDt.attrib.keys()):
                        sDate = nodeDt.attrib['value']
                        if(sDate):
                            dt = self.getDate(pl, sDate, False)
                            if(dt):
                                # get smoking status 
                                nodeVal = entry.find(f'./{ns}observation/{ns}value')
                                ### print("keys:" + str(node.attrib.keys()))
                                if nodeVal is not None and ('displayName' in nodeVal.attrib.keys()):
                                    smk = nodeVal.attrib['displayName']
                                    ### print("dpe code:" + code)
                                    ### print("dpe name:" + node.attrib['displayName'])
                                    #if we are validating, this is sufficient
                                    if validate:
                                        continue
                                    else:
                                        # create the natural key from the date and the patient id
                                        naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) 
                                        sh, created = SocialHistory.objects.get_or_create(
                                                natural_key=naturalKey,
                                                defaults={'provider_id': 1, 
                                                          'provenance_id': pl.provenance_id,
                                                          'patient_id': pl.patient.id,
                                                          'date': dt})
                                        sh.tobacco_use=smk
                                        if created: 
                                            sh.created_timestamp=TIMESTAMP
                                        sh.updated_timestamp=TIMESTAMP
                                        sh.save()
                    else:
                        log.warning('No date found for SocialHistory smoking entry for provenance ID: {}'.format(pl.provenance_id))         
        return # done with social history entry

    def importDiagnosticsFromProblemsEntries(self, pl, root, branch, validate):
        ns = self.ns

        # if this provider does not put this data in this section

        try:
            cdaLoad = CdaLoadInstruction.objects.get(provider=pl.provider, operation='DiagnosticsFromProblemsEntries')
        except ObjectDoesNotExist:
            return

        diagCodeType = cdaLoad.codeType.lower()
        if(diagCodeType != 'icd10' and diagCodeType != 'snomed'):
            self.saveException(pl, 'Error in importDiagnosticsFromProblemsEntries(), unsupported code type', 
                'Diagnostics', diagCodeType, '')
            return

        probRoot = branch
        # for each entry in this problem section
        lEntry = probRoot.findall(f'./{ns}entry')
        if lEntry:
            for entry in lEntry:
                # get the date 
                nodeLow = entry.find(f'./{ns}act/{ns}effectiveTime/{ns}low')
                nodeHigh = entry.find(f'./{ns}act/{ns}effectiveTime/{ns}high')
                sDate = None
                dt = None
                if nodeLow is not None and ('value' in nodeLow.attrib.keys()):
                    sDate = nodeLow.attrib['value']
                if not sDate and (nodeHigh is not None and ('value' in nodeHigh.attrib.keys())):     
                    sDate = nodeHigh.attrib['value']
                if(sDate):
                    dt = self.getDate(pl, sDate, False)
                if(dt):
                    # get snomed diagnosis code
                    node = entry.find(f'./{ns}act/{ns}entryRelationship/{ns}observation/{ns}value')
                    ### print("keys:" + str(node.attrib.keys()))
                    if node is not None and ('code' in node.attrib.keys()):
                        code = node.attrib['code']
                        ### print("dpe code:" + code)
                        ### print("dpe name:" + node.attrib['displayName'])

                        #if we are validating, this is sufficient
                        if validate:
                            continue
                        else:
                            if diagCodeType == 'snomed':
                                dxCode = self.icdFromSnomed(code)
                            else:
                                # icd10
                                dxCode = Dx_code.objects.filter(code=code, type='icd10').first()
                            self.addDiagnosis(pl, diagCodeType, code, dt, dxCode)
        return 

    def importVitalsFromTable(self, pl, root, branch, lFieldRegx, dCdaToEspField, validate):
        ns = self.ns

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, branch, lFieldRegx, 'Vital Signs', validate)
        ### print("ordering: " + str(lFieldOrdering))
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, 'Vital Signs', validate)

                if(validate):
                    pass

                else:
                    # vital signs ordering is required to be:
                    # Date, Description ('Temperature'), Value ('98.5f')
                    # these 3 fields are not optional

                    # skip records with missing data
                    if((lValue is None) or len(lValue) < 3):
                        continue
                    try:
                        cdaName = lValue[1].lower()
                        ### print('vital: ' + cdaName + ' : ' + lValue[2])
                        if(cdaName in dCdaToEspField):
                            encounter = self.getOrCreateEncounter(pl, lValue[0], 'Unknown Encounter', False)
                            espFields=dCdaToEspField[cdaName].split(',')
                            for espField in espFields:
                            # save the vital sign in the encounter
                                self.insertDBValue(pl, Encounter, encounter, espField, lValue[2])
                            encounter.save()
                    except Exception as ex:
                        self.saveException(pl, 'Error in importVitalsFromTable()', 'Encounter', str(lValue), ex)
        return


    def importVitalsFromVitalsEntries(self, pl, root, branch, validate):
        ns = self.ns

        fieldNameList=self.getFieldDict(pl.provider, 'vitals')
        vRoot = branch
        # for each entry in this problem section
        lEntry = vRoot.findall(f'./{ns}entry')
        if lEntry:
            for entry in lEntry:
                comps = entry.findall(f'./{ns}organizer/{ns}component')
                for comp in comps:
                    obs=comp.find(f'./{ns}observation')
                    obsIdroot=self.getData(obs, f'./{ns}id','root')
                    obsIdext=self.getData(obs, f'./{ns}id','extension')
                    # get the date 
                    nodeDt = obs.find(f'./{ns}effectiveTime')
                    dtlow, dthigh = self.getDateFromEffectiveTime(pl, nodeDt)
                    if not dtlow and not dthigh:
                        if validate:
                            log.warning('No date for vitals observation. ID-ext: {}, Provenance {}'.format(obsIdext,
                                pl.provenance_id))
                        continue
                    elif validate:
                        continue
                    else:
                        dt = next(x for x in [dtlow, dthigh] if x is not None)
                    try:
                        cdaName = self.getData(obs,f'./{ns}code','displayName')
                        cdaValue = self.getData(obs,f'./{ns}value','value')
                        cdaUnit = self.getData(obs,f'./{ns}value','unit')
                        if(cdaName in fieldNameList):
                            encounter = self.getOrCreateEncounter(pl, str(dt), 'Unknown Encounter', False)
                            espFields=fieldNameList[cdaName].split(',')
                            for espField in espFields:
                            # save the vital sign in the encounter
                                if 'raw_' in espField and cdaValue and cdaUnit:
                                    cdaValue = cdaValue + ' ' + cdaUnit
                                self.insertDBValue(pl, Encounter, encounter, espField, cdaValue)
                                #TODO: if we see other units here, we'll need to build a conversion method
                                if espField=='height' and cdaUnit != 'cm':
                                    log.warning('Non-standard height unit for vitals.  ID-ext: {}, Provenance {}'.format(obsIdext,
                                        pl.provenance_id))
                                if espField=='weight' and cdaUnit != 'kg':
                                    log.warning('Non-standard weight unit for vitals.  ID-ext: {}, Provenance {}'.format(obsIdext,
                                        pl.provenance_id))
                            encounter.save()
                    except Exception as ex:
                        log.error('Data processing exception. ID-ext: {}, Provenance {}, exc: {}'.format(obsIdext,pl.provenance_id, ex))
        return

    def importFromTable(self, pl, root, classType, branch, lFieldRegx, dFieldPosition, validate):
        ns = self.ns

        lFieldOrdering, lNode = self.getBranchOrderingNodes(pl, root, branch, lFieldRegx, classType._meta.model_name, validate)
        if (lNode):
            for node in lNode:
                lValue = self.getValuesFromNode(pl, node, lFieldOrdering, classType._meta.model_name, validate)

                # the first 2 fields (date, description) are non-optional and used to create the naturalKey
                # skip records with missing data
                if((lValue is None) or len(lValue) < 2):
                    continue

                dt = None
                try:
                    # the first 2 fields (date, description) are non-optional and used to create the naturalKey
                    sDate = lValue[0]
                    ### print("date: " + sDate)
                    dt = self.getDate(pl, sDate, validate)
                    lValue[0]=dt
                    description = lValue[1]
                except Exception as ex:
                    if(validate):
                        self.addValidationError(pl, 'Error in importFromTable(): ' + classType._meta.model_name + ' ' + str(ex))
                    else:
                        self.saveException(pl, 'Error in importFromTable()', classType._meta.model_name, '', ex)
                    return

                if (not validate) and description and dt and (str(dt) != 'NaT'):
                    # create the natural key from the date and the description 
                    naturalKey = dt.strftime('%Y%m%d') + '-' + str(pl.patient.id) + '-' + description
                    # truncate so we fit into natural key length
                    naturalKey = naturalKey[:127]

                    obj = classType.objects.filter(patient_id = pl.patient.id, 
                        natural_key = naturalKey).first()

                    # if this record does not already exist
                    if(not obj):
                        # create it
                        ### print("new Obj: " + naturalKey)
                        obj, created = classType.objects.get_or_create(patient_id=pl.patient.id, 
                            natural_key=naturalKey, date=dt, 
                            defaults={'provider_id': 1, 'provenance_id': pl.provenance_id})

                    # previously existing objects should have the most recent provenance
                    obj.provenance_id=pl.provenance_id
                    # update the (potentially new) object
                    # add the field values to the object
                    for field in dFieldPosition:
                        self.insertDBValue(pl, classType, obj, field, lValue[dFieldPosition[field]])
                    obj.save()

    def importDemographics(self, pl, root, validate):

        # process xpaths first for all CDAs
        # then for provider specific CDA
        lProv = ['All', pl.provider]

        if(not validate):
            for provider in lProv:
                lCdaXpath = CdaXpath.objects.filter(provider=provider).order_by('id')
                for xPathMapping in lCdaXpath:
                    ### print("mapping: " + str(mapping))
                    if(xPathMapping.provider == provider and
                        xPathMapping.modelName == 'Patient'):
                        try:
                            self.processItem(pl, root, xPathMapping, Patient, pl.patient)

                        except Exception as ex:
                            # we just ignore other missing demographics data
                            pass

            # special handling for a few fields
            pl.patient.mrn = pl.patient.natural_key
            if(pl.patient.zip is not None):
                pl.patient.zip5 = pl.patient.zip[:5]
            pl.patient.save()
                
    def getData(self, node, path, key):
        try:
            data_val=node.find(path).attrib[key]
            return data_val
        except:
            return None


    def processItem(self, pl, root, mapping, classType, obj):
        objDirty = False
        retVal = False

        if mapping.operation == OpMapping.node:
            # fetch the data from the node
            ### print("xpath: " + mapping.xpathFrom)
            node = root.find(mapping.xpathFrom)
            if node is not None:
                value = node.text
                if value is not None:
                    objDirty = True
                    ### print("node value: " + value)
                    self.transformAndInsert(pl, classType, obj, value, mapping.transform,
                        mapping.fieldTo)
                    retVal = True
        
        elif mapping.operation == OpMapping.attrib:
            # fetch the data from the specified attribute of the specified node
            element = root.find(mapping.xpathFrom)
            if element is not None:
                value = element.attrib[mapping.attribute]
                if value is not None:
                    objDirty = True
                    ### print("attrib value: " + value)
                    self.transformAndInsert(pl, classType, obj, value, mapping.transform,
                        mapping.fieldTo)
                    retVal = True
        
        elif mapping.operation == OpMapping.timeNow:
            objDirty=True
            obj.__dict__[mapping.fieldTo] = datetime.now()
            retVal = True

        # only save the object if we actually found data for it in the XML
        if (objDirty):
            obj.save()
        return retVal

    def transformAndInsert(self, pl, classType, obj, value, transform, to):
        if not transform:
            self.insertDBValue(pl, classType, obj, to, value)
            obj.__dict__[to] = value
        else:
            if value:
                # transform the data
                if transform == 'datetime':
                    value = self.getDate(pl, value, False)
                    if (not value):
                        self.saveException(pl, 'Invalid date in transformAndInsert()', classType._meta.model_name, value, '')
                        return
                else:
                    # the transform is a custom function
                    self.saveException(pl, 'Missing custom transform in transformAndInsert()', classType._meta.model_name, 'transform=' + transform, '')
                    return
                obj.__dict__[to] = value

    # Truncate a string to a length that will fit into the 
    # database field
    def truncateDBString(self, pl, classType, field, value):
        if(isinstance(value, str)):
            fieldType = classType._meta.get_field(field).db_type(connection)
            if(fieldType):
                lMatch = re.search('varchar\((\d+)\)', fieldType, re.IGNORECASE)
                if(lMatch):
                    try:
                        length = int(lMatch[1])
                        lengthSrc = len(value)
                        if lengthSrc > length:
                            value = value[0:length-1]
                    except:
                        pass
        return value

    # stip out unit text (98.6 def F) -> (98.6) 
    # from a numeric text string
    def stripNonNumText(self, classType, field, value):
        if(isinstance(value, str)):
            fieldType = classType._meta.get_field(field).db_type(connection)
            ### print(fieldType + ':' + str(value))
            if(fieldType):
                lMatch = re.search('(float|real|numeric|double)', fieldType, re.IGNORECASE)
                if(lMatch):
                    value = strip_float_text(value)
                lMatch = re.search('(int)', fieldType, re.IGNORECASE)
                if(lMatch):
                    value = strip_int_text(value)
        return value

    # ensure that a value will fit in the specified
    # model field and insert it
    def insertDBValue(self, pl, classType, obj, field, value):
        if(not self.insertNormalizeDBValue(pl, classType, obj, field, value)):
            value = self.truncateDBString(pl, classType, field, value)
            value = self.stripNonNumText(classType, field, value)
            obj.__dict__[field] = value

    # normalize certain field values to proper format / units 
    # and update the obj with the normalized value
    # return False if further DB operations are required
    # return True if DB operations for this field are complete
    def insertNormalizeDBValue(self, pl, classType, obj, field, value):
        try:
            if(classType == Encounter):
                if (field == 'raw_weight'):
                    # assume the weight is in kg, check for lbs
                    kg = weight_str_to_kg(value)
                    if(kg == None):
                        # parse out the number
                        kg = strip_float_text(value)
                    obj.weight = kg
                elif (field == 'raw_height'):
                    # assume the height is in cm, check for english units
                    cm = height_str_to_cm(value)
                    if(cm == None):
                        # parse out the number
                        cm = strip_float_text(value)
                    obj.height = cm
                elif (field == 'temperature'):
                    degc = temp_str_to_c(value)
                    obj.temperature = degc
                    return True
                elif (field == 'raw_bmi'):
                    obj.bmi = strip_float_text(value)
                elif (field == 'raw_bp_systolic'):
                    obj.bp_systolic = strip_float_text(value)
                elif (field == 'raw_bp_diastolic'):
                    obj.bp_diastolic = strip_float_text(value)
            elif(classType == SocialHistory):
                if(field == 'tobacco_use'):
                    if((not (len(pl.tobaccoExcludeRegx) > 0 and re.search(pl.tobaccoExcludeRegx, value, re.IGNORECASE))) and
                            (len(pl.tobaccoIncludeRegx) > 0 and re.search(pl.tobaccoIncludeRegx, value, re.IGNORECASE))):
                        obj.tobacco_use = self.truncateDBString(pl, SocialHistory, 'tobacco_use', value.strip())
                    return True

        except Exception as ex:
            self.saveException(pl, 'Error in insertNormalizedDBValue()', classType._meta.model_name, value, ex)

        return False

    def getProviderRegxDict(self, provider):
        d = {}
        lMapping = CdaMapping.objects.filter(provider=provider, section__isnull=True).order_by('id')
        for mapping in lMapping:
            d[mapping.key] = mapping.value
            ### print('provider: ' + provider + ' key: ' + mapping.key + ' value: ' + mapping.value)
        return d

    def getFieldPositionDict(self, provider, section):
        d = {}
        lMapping = CdaMapping.objects.filter(provider=provider, section=section).order_by('id')
        for mapping in lMapping:
            d[mapping.key] = int(mapping.value)
        return d

    def getFieldDict(self, provider, section):
        d = {}
        lMapping = CdaMapping.objects.filter(provider=provider, section=section).order_by('id')
        for mapping in lMapping:
            d[mapping.key] = mapping.value
        return d

    def getProviderRegxList(self, provider, section):
        l = []
        lMapping = CdaMapping.objects.filter(provider=provider, section=section).order_by('id')
        for mapping in lMapping:
            l.append(mapping.value)
        return l

    def saveException(self, pl, description, section, data, ex):
        sErr = description + ':' + section + ': ' + str(ex) + ' ' + traceback.format_exc()
        error = CdaError(fileName = pl.fileName, errMsg = sErr[:512], section=section,
            data = data, timestamp = datetime.now())
        ### Sometimes we get a bolus of errors like when we run out of disk space
        ### The following save() can fill up the database
        ### TODO Jay error.save()
        pl.lCdaError.append(error)
        log.error(description + ':' + section + ': ' + str(ex))

    def addValidationError(self, pl, sError, ignorable=False):
        sPrefix = 'Ignorable: '
        if (not ignorable):
            pl.valErrorsIgnorable = False
            sPrefix = 'NOT Ignorable: '
            log.error(sError)
        ### print('Validation: ' + sPrefix + sError)
        pl.lValidationError.append(sPrefix + sError + '\n')

    def icdFromSnomed(self, snomedCode):
        # if a snomed code of interest
        lIcd10 = SnomedToIcd10.objects.filter(snomed=snomedCode)
        if(lIcd10):
            icd10 = lIcd10[0].icd10.upper()
            ### print('Mapped snomed: ' + diagCode + " to icd10: " + icd10)
            dxCode = Dx_code.objects.filter(code=icd10, type='icd10').first()
        else:
            # didn't find this snomed in the mapping table, meaning it doesn't map to a condition
            # we track
            # use an icd9 code for unknown morbitity/mortality
            dxCode = Dx_code.objects.filter(code='799.9', type='icd9').first()
 
        return dxCode

    def initTobaccoRegx(self, pl):
        # get regx for including / excluding tobacco use from Social History
        dTobaccoRegx = self.getFieldDict(pl.provider, 'tobaccoRegx')
        if('include' in dTobaccoRegx):
            pl.tobaccoIncludeRegx = dTobaccoRegx['include']
        if('exclude' in dTobaccoRegx):
            pl.tobaccoExcludeRegx = dTobaccoRegx['exclude']

