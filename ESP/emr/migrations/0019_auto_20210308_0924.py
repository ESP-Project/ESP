# Generated by Django 2.2.17 on 2021-03-08 09:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0018_auto_20210114_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='socialhistory',
            name='food',
            field=models.CharField(blank=True, max_length=25, null=True, verbose_name='Food'),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='housing',
            field=models.CharField(blank=True, max_length=25, null=True, verbose_name='Housing'),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='medical_transportation',
            field=models.CharField(blank=True, max_length=25, null=True, verbose_name='Medical Transportation'),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='transportation',
            field=models.CharField(blank=True, max_length=25, null=True, verbose_name='Transportation'),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='utilities',
            field=models.CharField(blank=True, max_length=25, null=True, verbose_name='Utilities'),
        ),
    ]
