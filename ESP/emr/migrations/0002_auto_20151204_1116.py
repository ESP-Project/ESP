# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0001_initial'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='encounter',
            index_together=set([('patient', 'date', 'bmi'), ('patient', 'edd'), ('patient', 'date', 'weight'), ('patient', 'date'), ('patient', 'date', 'edd'), ('patient', 'date', 'height')]),
        ),
    ]
