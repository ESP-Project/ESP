# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0007_auto_20180829_1148'),
    ]

    operations = [
        migrations.AddField(
            model_name='provider',
            name='npi',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
