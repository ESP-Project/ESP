# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0012_NewFields_20191219'),
    ]

    operations = [
        migrations.AddField(
            model_name='laborder',
            name='facility_provider',
            field=models.ForeignKey(related_name='laborder_facility_provider', blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='patient',
            name='birth_sex',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Sex at Birth'),
        ),
        migrations.AddField(
            model_name='patient',
            name='gender_identity',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Gender Identity'),
        ),
        migrations.AddField(
            model_name='patient',
            name='sex_orientation',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Sexual Orientation'),
        ),
    ]
