# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0005_auto_20160811_0535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='encounter',
            name='drvs_service_line',
            field=models.CharField(max_length=51, null=True, verbose_name='Service line value from DRVS', blank=True),
        ),
        migrations.AlterField(
            model_name='patient',
            name='insurance_status',
            field=models.CharField(max_length=50, null=True, verbose_name='Insurance Status', blank=True),
        ),
    ]
