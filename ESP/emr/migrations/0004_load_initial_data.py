# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from ESP.utils.utils import LoadFixtureData
from ESP import settings
from django.db import models, migrations
from django.core.management import call_command
from django.conf import settings

db_engine = settings.DATABASES['default']['ENGINE']

if db_engine == 'django.db.backends.postgresql_psycopg2':
  class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0003_auto_20160513_1020'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'emr', 'fixtures', 'data') + ".json")),
        migrations.RunSQL("select setval('emr_allergy_id_seq',(select max(id) from emr_allergy)+1);"),
        migrations.RunSQL("select setval('emr_diagnosis_id_seq',(select max(id) from emr_diagnosis)+1);"),
        migrations.RunSQL("select setval('emr_encounter_dx_codes_id_seq',(select max(id) from emr_encounter_dx_codes)+1);"),
        migrations.RunSQL("select setval('emr_encounter_id_seq',(select max(id) from emr_encounter)+1);"),
        migrations.RunSQL("select setval('emr_encountertypemap_id_seq',(select max(id) from emr_encountertypemap)+1);"),
        migrations.RunSQL("select setval('emr_etlerror_id_seq',(select max(id) from emr_etlerror)+1);"),
        migrations.RunSQL("select setval('emr_hospital_problem_id_seq',(select max(id) from emr_hospital_problem)+1);"),
        migrations.RunSQL("select setval('emr_immunization_id_seq',(select max(id) from emr_immunization)+1);"),
        migrations.RunSQL("select setval('emr_laborder_id_seq',(select max(id) from emr_laborder)+1);"),
        migrations.RunSQL("select setval('emr_labresult_details_id_seq',(select max(id) from emr_labresult_details)+1);"),
        migrations.RunSQL("select setval('emr_labresult_id_seq',(select max(id) from emr_labresult)+1);"),
        migrations.RunSQL("select setval('emr_labtestconcordance_id_seq',(select max(id) from emr_labtestconcordance)+1);"),
        migrations.RunSQL("select setval('emr_order_extension_id_seq',(select max(id) from emr_order_extension)+1);"),
        migrations.RunSQL("select setval('emr_order_idinfo_id_seq',(select max(id) from emr_order_idinfo)+1);"),
        migrations.RunSQL("select setval('emr_patient_addr_id_seq',(select max(id) from emr_patient_addr)+1);"),
        migrations.RunSQL("select setval('emr_patient_extradata_id_seq',(select max(id) from emr_patient_extradata)+1);"),
        migrations.RunSQL("select setval('emr_patient_guardian_id_seq',(select max(id) from emr_patient_guardian)+1);"),
        migrations.RunSQL("select setval('emr_patient_id_seq',(select max(id) from emr_patient)+1);"),
        migrations.RunSQL("select setval('emr_pregnancy_id_seq',(select max(id) from emr_pregnancy)+1);"),
        migrations.RunSQL("select setval('emr_prescription_id_seq',(select max(id) from emr_prescription)+1);"),
        migrations.RunSQL("select setval('emr_problem_id_seq',(select max(id) from emr_problem)+1);"),
        migrations.RunSQL("select setval('emr_provenance_provenance_id_seq',(select max(provenance_id) from emr_provenance)+1);"),
        migrations.RunSQL("select setval('emr_provider_id_seq',(select max(id) from emr_provider)+1);"),
        migrations.RunSQL("select setval('emr_provider_idinfo_id_seq',(select max(id) from emr_provider_idinfo)+1);"),
        migrations.RunSQL("select setval('emr_provider_phones_id_seq',(select max(id) from emr_provider_phones)+1);"),
        migrations.RunSQL("select setval('emr_socialhistory_id_seq',(select max(id) from emr_socialhistory)+1);"),
        migrations.RunSQL("select setval('emr_specimen_id_seq',(select max(id) from emr_specimen)+1);"),
        migrations.RunSQL("select setval('emr_specobs_id_seq',(select max(id) from emr_specobs)+1);"),
        migrations.RunSQL("select setval('emr_stiencounterextended_id_seq',(select max(id) from emr_stiencounterextended)+1);"),
        migrations.RunSQL("select setval('emr_surveyreports_id_seq',(select max(id) from emr_surveyreports)+1);"),
        migrations.RunSQL("select setval('emr_surveyresponse_id_seq',(select max(id) from emr_surveyresponse)+1);"),
    ]

else:
  class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0003_auto_20160513_1020'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'emr', 'fixtures', 'data') + ".json")),
    ]
