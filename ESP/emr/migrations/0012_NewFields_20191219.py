# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0011_Encounter_fields_renamed'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='next_appt_date',
            field=models.DateTimeField(null=True, verbose_name='Date of next appointment', blank=True),
        ),
        migrations.AddField(
            model_name='patient',
            name='next_appt_fac_provider',
            field=models.ForeignKey(related_name='next_appt_fac_prov', blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='patient',
            name='next_appt_provider',
            field=models.ForeignKey(related_name='next_appt_prov', blank=True, to='emr.Provider', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='patient',
            name='vital_status',
            field=models.CharField(max_length=25, null=True, verbose_name='Alive/Deceased', blank=True),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='alcohol_oz_per_week',
            field=models.CharField(max_length=100, null=True, verbose_name='Alcohol Oz per week', blank=True),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='birth_control_method',
            field=models.CharField(max_length=200, null=True, verbose_name='Birth Control Method', blank=True),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='ill_drug_use',
            field=models.CharField(max_length=25, null=True, verbose_name='Illicit Drug Use', blank=True),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='sex_partner_gender',
            field=models.CharField(db_index=True, max_length=25, null=True, verbose_name='Sex Partner Gender', blank=True),
        ),
        migrations.AddField(
            model_name='socialhistory',
            name='sexually_active',
            field=models.CharField(max_length=25, null=True, verbose_name='Sexually Active', blank=True),
        ),
        migrations.AlterModelOptions( name='risk_factors', options={'verbose_name_plural': 'Risk Factors'},),
        migrations.AlterField( model_name='encounter', name='general_use1', field=models.CharField(max_length=50, null=True, verbose_name='Future Use 1 Char', blank=True),),
        migrations.AlterField( model_name='encounter', name='general_use2', field=models.DateField(null=True, verbose_name='Future Use 2 Date', blank=True),),
        migrations.AlterField( model_name='encounter', name='general_use3', field=models.CharField(max_length=50, null=True, verbose_name='Future Use 3 Char', blank=True),),
        migrations.AlterField( model_name='encounter', name='general_use4', field=models.DateField(null=True, verbose_name='Future Use 4 Date', blank=True),),
        migrations.AlterField( model_name='encounter', name='general_use5', field=models.CharField(max_length=20, null=True, verbose_name='Future Use 5 Char', blank=True),),
        migrations.AlterField( model_name='encounter', name='general_use6', field=models.CharField(max_length=51, null=True, verbose_name='Future Use 6 Char', blank=True),),
        migrations.AlterField( model_name='encounter', name='primary_payer', field=models.CharField(max_length=100, null=True, verbose_name='Primary Payer Information', blank=True),),
    ]
