'''
                              ESP Health Project
User Interface Module
                               URL Configuration

@authors: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2010 Channing Laboratory
@license: LGPL
'''


from django.urls import include, re_path
from ESP.ui import views

urlpatterns = [
    re_path(r'^labtest/lookup/$', views.labtest_lookup, name='labtest_lookup'),
    re_path(r'^labtest/detail/$', views.labtest_detail, name='labtest_detail'),
    re_path(r'^labtest/linelist/(?P<native_code>.*)$', views.labtest_csv, name='labtest_csv'),
    re_path(r'^labtest/linelist/(?P<native_code>.*)$', views.labtest_csv, name='labtest_csv'),
    re_path(r'^labtest/ignore_set/$', views.ignore_code_set, name='ignore_code_set'),
    re_path(r'^labtest/map/(?P<native_code>.+)/$', views.map_native_code, name='map_native_code'),
    re_path(r'^labtest/unmapped/$', views.unmapped_labs_report, name='unmapped_labs_report'),
    re_path(r'^labtest/unmapped/(?P<condition>[\w\s]+)/$', views.unmapped_labs_report, name='condition_unmapped_labs_report'),
    #
    #-------------------------------------------------------------------------------
    # Nodis
    #-------------------------------------------------------------------------------
    #
    re_path(r'^cases/$', views.case_list, {'status': 'all'}, name='nodis_cases_all'),
    re_path(r'^cases/list/awaiting_review/$', views.case_list, {'status': 'await'}, name='nodis_cases_awaiting_review'),
    re_path(r'^cases/list/under_review/$', views.case_list, {'status': 'under'}, name='nodis_cases_under_review'),
    re_path(r'^cases/list/queued/$', views.case_list, {'status': 'queued'}, name='nodis_cases_queued'),
    re_path(r'^cases/list/sent/$', views.case_list, {'status': 'sent'}, name='nodis_cases_sent'),
    re_path(r'^cases/list/requeued/$', views.case_list, {'status': 'requeued'}, name='nodis_cases_requeued'),
    #
    # Case Detail
    #
    re_path(r'^cases/view/(?P<case_id>\d+)/$', views.case_detail, name='nodis_case_detail'),
    re_path(r'^cases/viewpert/(?P<case_id>\d+)/$', views.pertussis_detail, name='nodis_pertussis_detail'),
    re_path(r'^cases/update/(?P<case_id>\d+)/$', views.case_status_update, name='nodis_case_update'),
    re_path(r'^cases/transmit/(?P<case_id>\d+)/$', views.case_queue_for_transmit, name='nodis_case_transmit'),
    re_path(r'^provider/(?P<provider_id>\w+)/$', views.provider_detail, name='provider_detail'),
    re_path(r'^patient/(?P<patient_pk>\d+)/records/$', views.all_records, name='all_records'),
    #
    # Validator
    #
    re_path(r'^validate/$', views.validator_summary, name='validator_summary'),
    re_path(r'^validate/missing/$', views.validate_missing, name='validate_missing'),
    re_path(r'^validate/missing/case/(?P<result_id>\d+)/$', views.missing_case_detail, name='missing_case_detail'),
    re_path(r'^validate/new/$', views.validate_new, name='validate_new'),
    re_path(r'^validate/similar/$', views.validate_similar, name='validate_similar'),
    re_path(r'^validate/exact/$', views.validate_exact, name='validate_exact'),

]
