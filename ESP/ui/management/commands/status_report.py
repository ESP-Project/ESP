'''
                                  ESP Health
                             User Interface Module
                            "status_report" Command


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory - http://www.channing.harvard.edu
@copyright: (c) 2010 Channing Laboratory
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''
from django.core.management.base import BaseCommand
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


from ESP.utils.utils import log
from ESP.ui.views import _populate_status_values

from ESP.settings import SITE_NAME
from ESP.settings import EMAIL_SUBJECT_PREFIX
from ESP.settings import SERVER_EMAIL
from ESP.settings import MANAGERS


class Command(BaseCommand):
    help = 'Generate a status report'

    def add_arguments(self, parser):
        parser.add_argument('--send-mail', action='store_true', dest='send_mail', default=False,
            help='Email report to managers')
        parser.add_argument('--reportable', action='store_true', dest='reportable_conditions', default=False,
            help='Reportable conditions only')
    
    def handle(self, *fixture_labels, **options):
        log.info('Generating status report')
        all_conditions = True
        if options['reportable_conditions']:
            all_conditions = False
        values = _populate_status_values(all_conditions)
        report_text_content = render_to_string('ui/status.txt', values)
        report_html_content = render_to_string('ui/status_email.html', values)

        if options['send_mail']:
            log.info('Emailing status report to site managers')
            #mail_managers(
                #'ESP Status Report (%s)' % SITE_NAME,
                #report,
                #)
            msg = EmailMultiAlternatives(
                EMAIL_SUBJECT_PREFIX + ' Status Report -- ' + SITE_NAME,
                report_text_content,
                SERVER_EMAIL, 
                [a[1] for a in MANAGERS],
                )
            msg.attach_alternative(report_html_content, "text/html")
            msg.send()
        else:
            print(report_text_content)