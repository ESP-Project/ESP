from django import forms
from django.db.models import BLANK_CHOICE_DASH
from ESP.emr.models import Provider
from ESP.hiv_risk.models import Static_Dispositions
from ESP.utils import log
from datetime import date, timedelta


class HRPDispositionForm(forms.Form):

    disposition = forms.ChoiceField(choices=BLANK_CHOICE_DASH, required=True)
    prep_date_provided = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}), label='PrEP RX Started/Renewed Date (where applicable)', required=False)
    provider = forms.ChoiceField(label='Managing Provider', choices=BLANK_CHOICE_DASH, required=True)
    disposition_date = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}), label='Disposition Date', required=True)
    comment = forms.CharField(widget=forms.Textarea, required=False)


    # Migrations will fail unless you build the choices in sub
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['disposition'].choices = BLANK_CHOICE_DASH + [(disposition.id, disposition.disposition_name) for disposition in Static_Dispositions.objects.all().order_by('sort_id')]

        #We only want providers that have been updated in the last year
        end_date = date.today() + timedelta(days=1)
        start_date = end_date- timedelta(days=365)

        self.fields['provider'].choices = BLANK_CHOICE_DASH + [(provider.id, provider.last_name +', ' + provider.first_name) for provider in Provider.objects.filter(provider_type=1) \
        .filter(updated_timestamp__range=[start_date, end_date]) \
        .exclude(last_name__isnull=True).order_by('last_name', 'first_name')]

