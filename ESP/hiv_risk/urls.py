# -*- coding: utf-8 -*-

from django.urls import include, re_path
from django.conf.urls import include
from ESP.hiv_risk import views
from django.urls import path

app_name = 'hiv_risk'

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^report/view/(?P<patient_id>\d+)/$', views.hrp_detail, name='hrp_detail'),
    re_path(r'^report/$', views.report, name='report'),
    re_path(r'^show_file/$', views.show_file, name='show_file'),
    re_path(r'ad_hoc_disposition/$', views.ad_hoc_dispo, name='ad_hoc_disposition'),
    re_path(r'^ad_hoc_disposition/view/(?P<patient_id>\d+)/$', views.hrp_history_detail, name='hrp_history_detail'),
]
