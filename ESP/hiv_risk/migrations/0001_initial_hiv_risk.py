# Generated by Django 2.1.15 on 2022-12-21 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('emr', '0020_auto_20220322_1627'),
    ]

    operations = [
        migrations.CreateModel(
            name='Patient_Dispositions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('prep_date_entered', models.DateField(blank=True, null=True)),
                ('disposition_date', models.DateField(blank=True)),
                ('comment', models.CharField(blank=True, max_length=1500, null=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('created_by_user', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Patient_Linelist',
            fields=[
                ('patient', models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, primary_key=True, serialize=False, to='emr.Patient')),
                ('hiv_risk_score', models.DecimalField(decimal_places=6, max_digits=7, null=True)),
                ('highest_risk_variable', models.CharField(blank=True, max_length=100, null=True)),
                ('second_highest_risk_variable', models.CharField(blank=True, max_length=100, null=True)),
                ('previous_prep_order', models.IntegerField(blank=True, null=True)),
                ('last_prep_order_date', models.DateField(blank=True, null=True)),
                ('suboxone_last_2yr', models.IntegerField(blank=True, null=True)),
                ('ill_drug_use_last_2yr', models.IntegerField(blank=True, null=True)),
                ('suboxone_and_std_last_2yr', models.IntegerField(blank=True, null=True)),
                ('std_case_last_2yr', models.IntegerField(blank=True, null=True)),
                ('sexually_active_status', models.IntegerField(blank=True, null=True)),
                ('num_pos_gon_tests_last_2yr', models.IntegerField(blank=True, null=True)),
                ('num_hiv_tst_dates_last_2yr', models.IntegerField(blank=True, null=True)),
                ('bicillin_24_inj_last_2yr', models.IntegerField(blank=True, null=True)),
                ('syph_dx_last_2yr', models.IntegerField(blank=True, null=True)),
                ('syph_dx_name_last_2yr', models.CharField(blank=True, max_length=50, null=True)),
                ('sex_partner_gender', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Patient_Linelist_History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ll_date', models.DateField(blank=True)),
                ('ll_quarter', models.IntegerField(blank=True)),
                ('ll_year', models.IntegerField(blank=True)),
                ('hiv_risk_score', models.DecimalField(decimal_places=6, max_digits=7, null=True)),
                ('highest_risk_variable', models.CharField(blank=True, max_length=100, null=True)),
                ('second_highest_risk_variable', models.CharField(blank=True, max_length=100, null=True)),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='emr.Patient')),
            ],
        ),
        migrations.CreateModel(
            name='Static_Dispositions',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('disposition_name', models.CharField(blank=True, max_length=255)),
                ('sort_id', models.IntegerField(blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='patient_dispositions',
            name='disposition',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hiv_risk.Static_Dispositions'),
        ),
        migrations.AddField(
            model_name='patient_dispositions',
            name='managing_provider',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='emr.Provider'),
        ),
        migrations.AddField(
            model_name='patient_dispositions',
            name='patient',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='emr.Patient'),
        ),
        migrations.AlterUniqueTogether(
            name='patient_linelist_history',
            unique_together={('patient', 'll_quarter', 'll_year')},
        ),
    ]
