from django.db import models
from ESP.emr.models import Patient
from ESP.emr.models import Provider

class Patient_Linelist(models.Model):
    '''
    Table description goes here
    '''
    patient = models.OneToOneField(Patient, on_delete=models.PROTECT, primary_key=True)
    hiv_risk_score = models.DecimalField(max_digits=7, decimal_places=6, null=True)
    highest_risk_variable = models.CharField(max_length=100, blank=True, db_index=False, null=True) 
    second_highest_risk_variable = models.CharField(max_length=100, blank=True, db_index=False, null=True)
    previous_prep_order = models.IntegerField(blank=True, null=True)
    last_prep_order_date = models.DateField(blank=True, null=True)
    suboxone_last_2yr = models.IntegerField(blank=True, null=True)
    ill_drug_use_last_2yr = models.IntegerField(blank=True, null=True)
    suboxone_and_std_last_2yr = models.IntegerField(blank=True, null=True)
    std_case_last_2yr = models.IntegerField(blank=True, null=True)
    sexually_active_status = models.IntegerField(blank=True, null=True)
    num_pos_gon_tests_last_2yr = models.IntegerField(blank=True, null=True)
    num_hiv_tst_dates_last_2yr = models.IntegerField(blank=True, null=True)
    bicillin_24_inj_last_2yr = models.IntegerField(blank=True, null=True)
    syph_dx_last_2yr = models.IntegerField(blank=True, null=True)
    syph_dx_name_last_2yr = models.CharField(max_length=50, blank=True, db_index=False, null=True)
    sex_partner_gender = models.CharField(max_length=50, blank=True, db_index=False, null=True)
    last_mpox_vax_date = models.DateField(blank=True, null=True)
    num_mpox_vax = models.IntegerField(blank=True, null=True)
    last_doxy_rx_date = models.DateField(blank=True, null=True)
    last_hiv_test_date = models.DateField(blank=True, null=True)


class Static_Dispositions(models.Model):
    '''
    Standard Dispositions from DPH
    '''
    id = models.IntegerField(blank=False, null=False, primary_key=True)
    disposition_name = models.CharField(max_length=255, blank=True, db_index=False, null=False)
    sort_id = models.IntegerField(blank=True, null=False)


class Patient_Dispositions(models.Model):
    patient = models.ForeignKey(Patient, blank=False, on_delete=models.PROTECT)
    disposition = models.ForeignKey('Static_Dispositions', on_delete=models.CASCADE)
    prep_date_entered = models.DateField(blank=True, null=True)
    managing_provider = models.ForeignKey(Provider, blank=True, null=False, on_delete=models.CASCADE)
    disposition_date = models.DateField(blank=True, null=False)
    comment = models.CharField(max_length=1500, null=True, blank= True)
    created_timestamp = models.DateTimeField(auto_now_add=True, blank=False)
    created_by_user = models.CharField(max_length=30, blank=False)

class Patient_Linelist_History(models.Model):
    '''
    Table that contains all patients that have appeared on a linelist
    '''
    patient = models.ForeignKey(Patient, blank=False, on_delete=models.PROTECT)
    ll_date = models.DateField(blank=True, null=False)
    ll_quarter = models.IntegerField(blank=True, null=False)
    ll_year = models.IntegerField(blank=True, null=False)
    hiv_risk_score = models.DecimalField(max_digits=7, decimal_places=6, null=True)
    highest_risk_variable = models.CharField(max_length=100, blank=True, db_index=False, null=True)
    second_highest_risk_variable = models.CharField(max_length=100, blank=True, db_index=False, null=True)

    class Meta:
        unique_together = ['patient', 'll_quarter', 'll_year']

class Patient_Linelist_Summary_View(models.Model):
    '''
    A database view for supporting the UI linelist
    '''
    patient = models.OneToOneField(Patient, on_delete=models.DO_NOTHING)
    hiv_risk_score = models.DecimalField(max_digits=7, decimal_places=6)
    highest_risk_variable = models.CharField(max_length=100)
    second_highest_risk_variable = models.CharField(max_length=100)
    previous_prep_order = models.IntegerField()
    last_prep_order_date = models.DateField()
    suboxone_last_2yr = models.IntegerField()
    ill_drug_use_last_2yr = models.IntegerField()
    suboxone_and_std_last_2yr = models.IntegerField()
    std_case_last_2yr = models.IntegerField()
    sexually_active_status = models.IntegerField()
    num_pos_gon_tests_last_2yr = models.IntegerField()
    num_hiv_tst_dates_last_2yr = models.IntegerField()
    bicillin_24_inj_last_2yr = models.IntegerField()
    syph_dx_last_2yr = models.IntegerField()
    syph_dx_name_last_2yr = models.CharField(max_length=50)
    sex_partner_gender = models.CharField(max_length=50)
    latest_disposition = models.CharField(max_length=255)
    first_ll_date = models.DateField()
    new_indicator = models.CharField(max_length=50)
    last_mpox_vax_date = models.DateField()
    num_mpox_vax = models.IntegerField()
    last_doxy_rx_date = models.DateField()
    last_hiv_test_date = models.DateField()


    class Meta:
        managed = False
        db_table = 'v_hiv_risk_patient_linelist_summary'

