import csv
import datetime
import os

import django_tables2 as tables
from dateutil.relativedelta import relativedelta
from django import forms


from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from wsgiref.util import FileWrapper
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, Http404, FileResponse
from django.shortcuts import render
from django.shortcuts import render, get_object_or_404, redirect

from django.db.models import Count
from django.db.models import BLANK_CHOICE_DASH

from django_tables2 import RequestConfig
from django_tables2.utils import A

from ESP.hiv_risk.models import Patient_Linelist, Static_Dispositions, Patient_Dispositions, Patient_Linelist_History,Patient_Linelist_Summary_View
from ESP.hiv_risk.forms import HRPDispositionForm
from ESP.utils import log
from ESP.utils import log_query

from ESP.emr.models import Patient
from ESP.emr.models import Provider
from ESP.settings import PY_DATE_FORMAT
from ESP import settings



#HRP stands for High Risk Patient


PAGE_TEMPLATE_DIR = 'pages/hiv_risk/'


class HRPFilterForm(forms.Form):

    RISK_FACTOR_CHOICES = [
        ('Dx code for HIV counseling in past 2 years', 'Dx code for HIV counseling in past 2 years'),
        ('Dx code for syphilis ever', 'Dx code for syphilis ever'),
        ('Dx for contact or exposure to venereal disease ever', 'Dx for contact or exposure to venereal disease ever'),
        ('EHR history exists for previous 2 years', 'EHR history exists for previous 2 years'),
        ('EHR history exists for previous year', 'EHR history exists for previous year'),
        ('Home Language: English', 'Home Language: English'),
        ('Length of available EHR history', 'Length of available EHR history'),
        ('Patient has had an HIV RNA Viral test ever', 'Patient has had an HIV RNA Viral test ever'),
        ('Patient has had an HIV RNA Viral test in the last 2 years', 'Patient has had an HIV RNA Viral test in the last 2 years'),
        ('Patient has Home Language', 'Patient has Home Language'),
        ('Patient Sex', 'Patient Sex'),
        ('Race: Black', 'Race: Black'),
        ('Race: Caucasian', 'Race: Caucasian'),
        ('Total # of chlamydia tests ever' , 'Total # of chlamydia tests ever' ),
        ('Total # of HIV ELISA or Ab/Ag tests ever', 'Total # of HIV ELISA or Ab/Ag tests ever'),
        ('Total # of HIV RNA tests in the previous year', 'Total # of HIV RNA tests in the previous year'),
        ('Total # of HIV tests ever (ELISA, RNA Viral, Ag/Ab)', 'Total # of HIV tests ever (ELISA, RNA Viral, Ag/Ab)'),
        ('Total # of HIV tests in past 2 years', 'Total # of HIV tests in past 2 years'),
        ('Total # of positive gonorrhea tests in past 2 years', 'Total # of positive gonorrhea tests in past 2 years'),
        ('Total # of rx for bicillin ever', 'Total # of rx for bicillin ever'),
        ('Total # of rx for bicillin in past 2 years', 'Total # of rx for bicillin in past 2 years'),
        ('Total # of rx for bicillin in previous year', 'Total # of rx for bicillin in previous year'),
        ('Total # of rx for suboxone in past 2 years', 'Total # of rx for suboxone in past 2 years'),
        ]

    MPOX_NUM_CHOICES = [
        (0, 0),
        (1, 1),
        (2, 2),
        ]


    patient_id = forms.CharField(required=False, label="ESP Patient ID")
    patient_mrn = forms.CharField(required=False, label='MRN' )
    highest_risk_variable = forms.ChoiceField(choices=BLANK_CHOICE_DASH + RISK_FACTOR_CHOICES, required=False, label="Highest Risk Factor")
    second_highest_risk_variable = forms.ChoiceField(choices=BLANK_CHOICE_DASH + RISK_FACTOR_CHOICES, required=False,label="Second Highest Risk Factor")
    previous_prep_order = forms.NullBooleanField( widget=forms.Select( choices=[ ('', '---'), (True, 'Yes'), (False, 'No'), ]), required=False, label='Previous PrEP order?')
    disposition_recorded = forms.NullBooleanField( widget=forms.Select( choices=[ ('', '---'), (True, 'Yes'), (False, 'No'), ]), required=False, label='Disposition Recorded?')
    pcp_dept = forms.ChoiceField(choices=BLANK_CHOICE_DASH, required=False, label="PCP Department")
    new_indicator = forms.NullBooleanField( widget=forms.Select( choices=[ ('', '---'), (True, 'Yes'), (False, 'No'), ]), required=False, label='New to Line List?')
    mpox_vax_y_or_n = forms.NullBooleanField( widget=forms.Select( choices=[ ('', '---'), (True, 'Yes'), (False, 'No'), ]), required=False, label='Mpox Vaccine Administered?') 
    mpox_vax_num = forms.ChoiceField(choices=BLANK_CHOICE_DASH + MPOX_NUM_CHOICES, required=False, label="# Mpox Vaccines") 
    doxy_rx_y_or_n = forms.NullBooleanField( widget=forms.Select( choices=[ ('', '---'), (True, 'Yes'), (False, 'No'), ]), required=False, label='Doxycycline Rx Ever?') 
    hiv_test_y_or_n = forms.NullBooleanField( widget=forms.Select( choices=[ ('', '---'), (True, 'Yes'), (False, 'No'), ]), required=False, label='HIV Test Date Recorded?')



    # Migrations will fail unless you build the choices in sub
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.fields['pcp_dept'].choices = [(None, '')] + [(n, n) for n in Patient_Linelist.objects.values('patient__pcp__dept').order_by('patient__pcp__dept').distinct('patient__pcp__dept')]
        self.fields['pcp_dept'].choices = BLANK_CHOICE_DASH + [(pcp_dept.get('patient__pcp__dept'), pcp_dept.get('patient__pcp__dept')) for pcp_dept in \
                Patient_Linelist.objects.values('patient__pcp__dept').distinct('patient__pcp__dept')]

        #Change None to different value so filtering will work
        for i in range(len(self.fields['pcp_dept'].choices)):
            if self.fields['pcp_dept'].choices[i] == (None, None):
                self.fields['pcp_dept'].choices[i] = ('NO DEPT LISTED FOR PCP', 'NO DEPT LISTED FOR PCP')


class HRPTable(tables.Table):
    link = tables.LinkColumn('hiv_risk:hrp_detail', text='view', args=[A('patient_id')], verbose_name=' ', orderable=False)
    patient_id = tables.Column(verbose_name="ESP ID")
    patient__mrn = tables.Column(verbose_name="MRN")
    hiv_risk_score = tables.Column(verbose_name="HIV Risk Score")
    highest_risk_variable = tables.Column(verbose_name="Highest Risk Factor")
    second_highest_risk_variable = tables.Column(verbose_name="Second Highest Risk Factor")
    last_prep_order_date = tables.Column(verbose_name="Last PrEP Order Date")
    last_hiv_test_date = tables.Column(verbose_name="Last HIV Test Date")
    #patient__patient_dispositions__disposition__disposition_name = tables.Column(verbose_name="Disposition", orderable=False)
    latest_disposition = tables.Column(verbose_name="Disposition")
    patient__pcp__dept = tables.Column(verbose_name="PCP Dept.")
    first_ll_date = tables.Column(verbose_name="Date Added")
    new_indicator = tables.Column(verbose_name="New?")
    last_mpox_vax_date = tables.Column(verbose_name="Last Mpox Vax Date")
    num_mpox_vax = tables.Column(verbose_name="# of Mpox Vax")
    last_doxy_rx_date = tables.Column(verbose_name="Last Doxy Rx Date")

    class Meta:
        sequence = ('link', 'patient_id', 'patient__mrn', 'hiv_risk_score','highest_risk_variable','second_highest_risk_variable')

class HRPHistoryFilterForm(forms.Form):        
    patient_id = forms.CharField(required=False, label="ESP Patient ID")
    patient_mrn = forms.CharField(required=False, label='MRN')
    patient_natural_key = forms.CharField(required=False, label='Patient Natural Key') 
    patient_last_name = forms.CharField(required=False, label='Last Name')
    patient_first_name = forms.CharField(required=False, label='First Name')

class HRPHistoryTable(tables.Table):
    link = tables.LinkColumn('hiv_risk:hrp_history_detail', text='view', args=[A('patient_id')], verbose_name=' ', orderable=False)
    patient_id = tables.Column(verbose_name="ESP Patient ID")
    patient__mrn = tables.Column(verbose_name='MRN')
    patient__natural_key = tables.Column(verbose_name='Patient Natural Key')
    patient__last_name = tables.Column(verbose_name='Last Name')
    patient__first_name = tables.Column(verbose_name='First Name')

    class Meta:
        sequence = ('link', 'patient_id', 'patient__mrn' )
    

@login_required
def index(request):
    return HttpResponse("Hello, world. You're at the index.")

@login_required
def report(request):
    values = {}

    most_recent_linelist = Patient_Linelist_History.objects.latest('ll_date')
    most_recent_linelist_date=most_recent_linelist.ll_date


    #qs = Patient_Linelist.objects.values('patient_id', 'patient__mrn', 'hiv_risk_score','highest_risk_variable', 'second_highest_risk_variable', \
    #                                     'patient__patient_dispositions__disposition__disposition_name', 'last_prep_order_date', 'patient__areacode', 'patient__tel', \
    #                                     'patient__last_name', 'patient__first_name', 'patient__pcp__dept', 'patient__pcp__last_name', 'patient__pcp__first_name') \
    #                                     .order_by('patient_id', 'patient__mrn', 'hiv_risk_score', 'highest_risk_variable','second_highest_risk_variable', \
    #                                     'last_prep_order_date', 'patient__pcp__dept', '-patient__patient_dispositions__id', 'patient__patient_dispositions__disposition__disposition_name' ) \
    #                                     .distinct('patient_id', 'patient__mrn', 'hiv_risk_score','highest_risk_variable','second_highest_risk_variable', 'last_prep_order_date', 'patient__pcp__dept' )

    #qs = Patient_Linelist_Summary_View.objects.all()

    qs = Patient_Linelist_Summary_View.objects.values('patient_id', 'patient__mrn', 'hiv_risk_score','highest_risk_variable', 'second_highest_risk_variable', \
                                                       'latest_disposition', 'last_prep_order_date', 'patient__areacode', 'patient__tel', \
                                                       'patient__last_name', 'patient__first_name', 'patient__pcp__dept', 'patient__pcp__last_name', 'patient__pcp__first_name',\
                                                       'first_ll_date', 'new_indicator', 'last_mpox_vax_date', 'num_mpox_vax', 'last_doxy_rx_date', 'last_hiv_test_date') 

    #log.info(qs.values())


    search_form = HRPFilterForm(request.GET)
    if search_form.is_valid():
        #log.debug(search_form.cleaned_data)
        patient_id = search_form.cleaned_data['patient_id']
        if patient_id:
            qs = qs.filter(patient_id=patient_id)
        patient_mrn = search_form.cleaned_data['patient_mrn']
        if patient_mrn:
            qs = qs.filter(patient__mrn__istartswith=patient_mrn)
        highest_risk_variable = search_form.cleaned_data['highest_risk_variable']
        if highest_risk_variable:
            qs = qs.filter(highest_risk_variable=highest_risk_variable)
        second_highest_risk_variable = search_form.cleaned_data['second_highest_risk_variable']
        if second_highest_risk_variable:
            qs = qs.filter(second_highest_risk_variable=second_highest_risk_variable)
        disposition_recorded = search_form.cleaned_data['disposition_recorded']
        if disposition_recorded == True:
            qs = qs.filter(latest_disposition__isnull=False)
        elif disposition_recorded == False:
            qs = qs.filter(latest_disposition__isnull=True)
        previous_prep_order = search_form.cleaned_data['previous_prep_order']
        if previous_prep_order == True:
            qs = qs.filter(previous_prep_order=1)
        elif previous_prep_order == False:
            qs = qs.filter(previous_prep_order=0)
        pcp_dept = search_form.cleaned_data['pcp_dept']
        if pcp_dept == 'NO DEPT LISTED FOR PCP':
            qs = qs.filter(patient__pcp__dept__isnull=True)
        elif pcp_dept:
            qs = qs.filter(patient__pcp__dept=pcp_dept)
        new_indicator = search_form.cleaned_data['new_indicator']    
        if new_indicator == True:
            qs = qs.filter(new_indicator="NEW")
        elif new_indicator == False:
            qs = qs.filter(new_indicator__isnull=True)
        mpox_vax_y_or_n = search_form.cleaned_data['mpox_vax_y_or_n']
        if mpox_vax_y_or_n == True:
            qs = qs.filter(last_mpox_vax_date__isnull=False)
        if mpox_vax_y_or_n == False:
            qs = qs.filter(last_mpox_vax_date__isnull=True)
        mpox_vax_num = search_form.cleaned_data['mpox_vax_num']
        if mpox_vax_num:
            qs = qs.filter(num_mpox_vax=mpox_vax_num)
        doxy_rx_y_or_n = search_form.cleaned_data['doxy_rx_y_or_n']
        if doxy_rx_y_or_n == True:
            qs = qs.filter(last_doxy_rx_date__isnull=False)
        if doxy_rx_y_or_n == False:
            qs = qs.filter(last_doxy_rx_date__isnull=True)
        hiv_test_y_or_n = search_form.cleaned_data['hiv_test_y_or_n']
        if hiv_test_y_or_n == True:
            qs = qs.filter(last_hiv_test_date__isnull=False)
        if hiv_test_y_or_n == False:
            qs = qs.filter(last_hiv_test_date__isnull=True)


    #log.info('HIV High Risk Patient List', qs)

    if request.GET.get('export_csv', None) == 'high_risk_list':
        return export_high_risk_patient_list(request, qs)

    table = HRPTable(qs)
    #log_query('HIV High Risk Patient List', qs)
    
    RequestConfig(request).configure(table)
    # Remove '?sort=' bit from full URL path
    full_path = request.get_full_path()
    sort_index = full_path.find('&sort')
    if sort_index != -1:
        full_path = full_path[:sort_index]
    # If path does not contain a query string (beginning with '?'), add a '?'
    # so the template's "&sort=" html forms a valid query
    query_index = full_path.find('?')
    if query_index == -1:
        full_path += '?'
    clear_search_path = full_path[:query_index]
    values['full_path'] = full_path
    values['clear_search_path'] = clear_search_path
    values['table'] = table
    values['search_form'] = search_form
    values['most_recent_linelist_date'] = most_recent_linelist_date
    return render(request, PAGE_TEMPLATE_DIR + 'report.html', values)


@login_required
def export_high_risk_patient_list(request, qs):
    '''
    Exports case list from a queryset as a CSV file
    '''
    header = [
        'esp_patient_id',
        'mrn',
        'last_name',
        'first_name',
        'telephone',
        'pcp',
        'pcp_dept',
        'hiv_risk_score',
        'highest_risk_factor',
        'second_highest_risk_factor',
        'last_prep_order_date',
        'last_hiv_test_date',
        'disposition',
        'date_added',
        'new_on_list',
        'last_mpox_vax_date',
        'num_mpox_vax', 
        'last_doxy_rx_date',
        ]
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename=hiv_high_risk_patients.csv'
    writer = csv.writer(response)
    writer.writerow(header)
    for a_row in qs:
        try:
            telephone = a_row["patient__areacode"] + '-' + a_row["patient__tel"]
        except TypeError:
            telephone = None

        try:
            pcp_name = a_row["patient__pcp__last_name"] + ', ' + a_row["patient__pcp__first_name"]
        except TypeError:
            pcp_name = None

        row = [
            a_row["patient_id"],
            a_row["patient__mrn"],
            a_row["patient__last_name"],
            a_row["patient__first_name"],
            telephone,
            pcp_name,
            a_row["patient__pcp__dept"],
            a_row["hiv_risk_score"],
            a_row["highest_risk_variable"],
            a_row["second_highest_risk_variable"],
            a_row["last_prep_order_date"],
            a_row["last_hiv_test_date"],
            a_row["latest_disposition"],
            a_row["first_ll_date"],
            a_row["new_indicator"],
            a_row["last_mpox_vax_date"],
            a_row["num_mpox_vax"],
            a_row["last_doxy_rx_date"],
            ]
        writer.writerow(row)
    return response


@login_required
def hrp_detail(request, patient_id):
    '''
    Comment Goes Here 
    '''

    hrp = get_object_or_404(Patient_Linelist, patient_id=patient_id)
    patient = hrp.patient

    # patient.age is derived directly from patient.date_of_birth.  When the
    # latter is None, the former will also be None
    try:
        dob = patient.date_of_birth.strftime(PY_DATE_FORMAT)
        age = patient.age.years
    except AttributeError:
        age = None
        dob = None
    if age >= 90:
       age = '90+'

    try:
        telephone = patient.areacode + '-' + patient.tel
    except TypeError:
        telephone = None

    pcp = patient.pcp
    pcp_name = None
    if pcp and pcp.id and (pcp.id >= 1):
        pcp_name = combine_str(pcp.last_name, pcp.first_name)
        pcp_dept = pcp.dept

    dispo_hist = Patient_Dispositions.objects.filter(patient_id=patient_id).order_by('-disposition_date')
    #log.info(dispo_hist)


    form = HRPDispositionForm()

    if request.method == 'POST':
        form = HRPDispositionForm(request.POST)
        if form.is_valid():
            pat_dispo = Patient_Dispositions()
            pat_dispo.disposition_id = form.cleaned_data['disposition']
            pat_dispo.patient = patient
            pat_dispo.prep_date_entered = form.cleaned_data['prep_date_provided']
            pat_dispo.managing_provider_id = form.cleaned_data['provider']
            pat_dispo.disposition_date = form.cleaned_data['disposition_date']
            pat_dispo.comment = form.cleaned_data['comment']
            pat_dispo.created_by_user = request.user.username
            pat_dispo.save()
            msg = 'Disposition Has Been Recorded!'
            messages.add_message(request, messages.INFO, msg)
            return redirect(reverse('hiv_risk:hrp_detail', args=[patient_id]))

    values = {
        'title': 'Risk for Acquisition of HIV: Patient Details & Disposition',
        'patient_id': patient_id,
        'hrp': hrp,
        'telephone': telephone,
        'mrn': patient.mrn,
        'last_name': patient.last_name,
        'first_name': patient.first_name,
        'gender': patient.gender,
        'current_age': age,
        'ethnicity': patient.ethnicity,
        'race': patient.race,
        'home_language': patient.home_language,
        'pcp_name': pcp_name,
        'pcp_dept': pcp_dept,
        "request":request,
        'form': form,
        'dispo_hist': dispo_hist,
        }
    return render(request, PAGE_TEMPLATE_DIR + 'hrp_detail_view_update.html', values )


def combine_str(x,y):
    if x and y:
        return x + ', ' + y
    elif x:
        return x
    elif y:
        return y
    else:
        return None


def show_file(response):
    #HIV_RISK_PATH = os.path.join(settings.TOPDIR, 'hiv_risk')
    #pdf = open(os.path.join(HIV_RISK_PATH, 'MDPH_PrEP_line_list_dispositions.pdf'), 'rb')
    #MOVING FILE TO ESP_TOOLS FOLDER SO CHANGES DO NOT REQUIRE ESP CODE RELEASE CHANGE
    pdf = open(r'/srv/esp/esp_tools/sql_reports/HIV_and_PREP/MDPH Prep/MDPH_PrEP_line_list_dispositions.pdf', 'rb')
    response = FileResponse(pdf)
    return response


def ad_hoc_dispo(request):
    values = {}

    #qs = Patient_Linelist_History.objects.all().distinct("patient_id")
    qs = Patient_Linelist_History.objects.none()

    search_form = HRPHistoryFilterForm(request.GET)
    if search_form.is_valid():
        #log.debug(search_form.cleaned_data)

        patient_id = search_form.cleaned_data['patient_id']
        patient_mrn = search_form.cleaned_data['patient_mrn']
        patient_natural_key = search_form.cleaned_data['patient_natural_key']
        patient_last_name = search_form.cleaned_data['patient_last_name']
        patient_first_name = search_form.cleaned_data['patient_first_name']

        # Only want to display the results, not the full table
        # so only populate the queryset if a search value is entered. 
        if patient_id != '' or patient_mrn != '' or patient_natural_key != '' or patient_last_name != '' or patient_first_name != '':
            qs = Patient_Linelist_History.objects.distinct("patient", "patient__mrn", "patient__natural_key", "patient__last_name", "patient__first_name")

        if patient_id:
            qs = qs.filter(patient_id=patient_id)
        if patient_mrn:
            qs = qs.filter(patient__mrn__istartswith=patient_mrn)
            #log_query('HISTORY QUERY', qs)
        if patient_natural_key:
            qs = qs.filter(patient__natural_key__istartswith=patient_natural_key)
        if patient_last_name:
            qs = qs.filter(patient__last_name__istartswith=patient_last_name)
        if patient_first_name:
            qs = qs.filter(patient__first_name__istartswith=patient_first_name)

    table = HRPHistoryTable(qs)

    #RequestConfig(request, paginate={"per_page": 3}).configure(table)
    RequestConfig(request).configure(table)
    # Remove '?sort=' bit from full URL path
    full_path = request.get_full_path()
    sort_index = full_path.find('&sort')
    if sort_index != -1:
        full_path = full_path[:sort_index]
    # If path does not contain a query string (beginning with '?'), add a '?'
    # so the template's "&sort=" html forms a valid query
    query_index = full_path.find('?')
    if query_index == -1:
        full_path += '?'
    clear_search_path = full_path[:query_index]
    values['full_path'] = full_path
    values['clear_search_path'] = clear_search_path
    values['table'] = table
    values['search_form'] = search_form

    return render(request, PAGE_TEMPLATE_DIR + 'ad_hoc_disposition.html', values)


def hrp_history_detail(request, patient_id):
    '''
    Comment Goes Here
    '''

    qs = Patient_Linelist_History.objects.all().distinct("patient_id")

    #Send the distinct queryset or else we will get multiple objects error
    hrp_history = get_object_or_404(qs, patient_id=patient_id)
    patient = hrp_history.patient

    try:
        dob = patient.date_of_birth.strftime(PY_DATE_FORMAT)
        age = patient.age.years
    except AttributeError:
        age = None
        dob = None
        telephone = None

    if age >= 90:
       age = '90+'

    try:
        telephone = patient.areacode + '-' + patient.tel
    except TypeError:
        telephone = None


    pcp = patient.pcp
    pcp_name = None
    if pcp and pcp.id and (pcp.id > 1):
        pcp_name = combine_str(pcp.last_name, pcp.first_name)

    dispo_hist = Patient_Dispositions.objects.filter(patient_id=patient_id).order_by('-disposition_date')

    form = HRPDispositionForm()

    if request.method == 'POST':
        form = HRPDispositionForm(request.POST)
        if form.is_valid():
            pat_dispo = Patient_Dispositions()
            pat_dispo.disposition_id = form.cleaned_data['disposition']
            pat_dispo.patient = patient
            pat_dispo.prep_date_entered = form.cleaned_data['prep_date_provided']
            pat_dispo.managing_provider_id = form.cleaned_data['provider']
            pat_dispo.disposition_date = form.cleaned_data['disposition_date']
            pat_dispo.comment = form.cleaned_data['comment']
            pat_dispo.created_by_user = request.user.username
            pat_dispo.save()
            msg = 'Disposition Has Been Recorded!'
            messages.add_message(request, messages.INFO, msg)
            return redirect(reverse('hiv_risk:hrp_history_detail', args=[patient_id]))


    values = {
        'title': 'Risk for Acquisition of HIV: Record Disposition',
        'patient_id': patient_id,
        'hrp_history': hrp_history,
        'telephone': telephone,
        'mrn': patient.mrn,
        'last_name': patient.last_name,
        'first_name': patient.first_name,
        'gender': patient.gender,
        'current_age': age,
        'ethnicity': patient.ethnicity,
        'race': patient.race,
        'home_language': patient.home_language,
        'pcp_name': pcp_name,
        "request":request,
        'form': form,
        'dispo_hist': dispo_hist,
    }


    return render(request, PAGE_TEMPLATE_DIR + 'ad_hoc_disposition_view_update.html', values )


