# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0004_auto_20180619_1326'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hl7map',
            options={'verbose_name_plural': 'HL7 Maps'},
        ),
        migrations.AlterModelOptions(
            name='reportableextended_variables',
            options={'verbose_name': 'Reportable Extended Variables', 'verbose_name_plural': 'Reportable Extended Variables'},
        ),
    ]
