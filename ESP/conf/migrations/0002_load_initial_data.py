# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from ESP.utils.utils import LoadFixtureData
from ESP import settings
from django.db import models, migrations
from django.core.management import call_command
from django.conf import settings

db_engine = settings.DATABASES['default']['ENGINE']

if db_engine == 'django.db.backends.postgresql_psycopg2':
  class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'conf', 'fixtures', 'data') + ".json")),
        migrations.RunSQL("select setval('conf_reportabledx_code_id_seq',(select max(id) from conf_reportabledx_code)+1);"),
        migrations.RunSQL("select setval('conf_reportablemedication_id_seq',(select max(id) from conf_reportablemedication)+1);"),
        migrations.RunSQL("select setval('conf_reportablelab_id_seq',(select max(id) from conf_reportablelab)+1);"),
        migrations.RunSQL("select setval('conf_resultstring_id_seq',(select max(id) from conf_resultstring)+1);"),
        migrations.RunSQL("select setval('conf_hl7map_id_seq',(select max(id) from conf_hl7map)+1);"),
        migrations.RunSQL("select setval('conf_ignoredcode_id_seq',(select max(id) from conf_ignoredcode)+1);"),
        migrations.RunSQL("select setval('conf_immuexclusion_id_seq',(select max(id) from conf_immuexclusion)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_donotsend_results_id_seq',(select max(id) from conf_labtestmap_donotsend_results)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_excluded_indeterminate_strings_id_seq',(select max(id) from conf_labtestmap_excluded_indeterminate_strings)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_excluded_negative_strings_id_seq',(select max(id) from conf_labtestmap_excluded_negative_strings)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_excluded_positive_strings_id_seq',(select max(id) from conf_labtestmap_excluded_positive_strings)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_extra_indeterminate_strings_id_seq',(select max(id) from conf_labtestmap_extra_indeterminate_strings)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_extra_negative_strings_id_seq',(select max(id) from conf_labtestmap_extra_negative_strings)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_extra_positive_strings_id_seq',(select max(id) from conf_labtestmap_extra_positive_strings)+1);"),
        migrations.RunSQL("select setval('conf_labtestmap_id_seq',(select max(id) from conf_labtestmap)+1);"),
        migrations.RunSQL("select setval('conf_reportableextended_variables_id_seq',(select max(id) from conf_reportableextended_variables)+1);"),
        migrations.RunSQL("select setval('conf_sitehl7_id_seq',(select max(id) from conf_sitehl7)+1);"),
        migrations.RunSQL("select setval('conf_vaccinecodemap_id_seq',(select max(id) from conf_vaccinecodemap)+1);"),
        migrations.RunSQL("select setval('conf_vaccinemanufacturermap_id_seq',(select max(id) from conf_vaccinemanufacturermap)+1);"),
    ]

else:
  class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'conf', 'fixtures', 'data') + ".json")),
    ]
