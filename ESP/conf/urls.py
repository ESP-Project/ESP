'''
                              ESP Health Project
                             Configuration Module
                               URL Configuration

@authors: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''


from django.urls import include, re_path
from ESP.conf import views

urlpatterns = [
    re_path(r'^codes/ignore/(?P<native_code>.+)/$', views.ignore_code, name='ignore_code'),
    #change name worst case .. good point TODO 
    re_path(r'^codes/reportables/', views.heuristic_reportables, name='heuristic_reportables'),
    re_path(r'^codes/report', views.heuristic_mapping_report, name='heuristic_mapping_report'),
    #re_path(r'^json_code_grid', json_code_grid, name='json_code_grid'),
]
