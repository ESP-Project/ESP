# Constants and values that are used all around

import datetime

EPOCH = datetime.date(2002, 0o1, 0o1)
DEIDENTIFICATION_TIMEDELTA = 60
