# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0001_initial'),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Name for this event type', db_index=True)),
                ('source', models.TextField(max_length=250, verbose_name='What created this event?')),
                ('date', models.DateField(verbose_name='Date event occured', db_index=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Time event was created in db')),
                ('note', models.TextField(null=True, blank=True)),
                ('object_id', models.PositiveIntegerField(db_index=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
                ('patient', models.ForeignKey(to='emr.Patient', on_delete=models.CASCADE)),
                ('provider', models.ForeignKey(to='emr.Provider', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Timespan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField(max_length=128, verbose_name='Common name of this type of timespan')),
                ('source', models.TextField(max_length=250, verbose_name='What created this timespan?')),
                ('start_date', models.DateField(db_index=True)),
                ('end_date', models.DateField(db_index=True, null=True, blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Time this event was created in db')),
                ('pattern', models.TextField(null=True, blank=True)),
                ('events', models.ManyToManyField(to='hef.Event')),
                ('patient', models.ForeignKey(to='emr.Patient', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
