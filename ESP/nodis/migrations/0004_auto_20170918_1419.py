# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('emr', '0005_auto_20160811_0535'),
        ('nodis', '0003_auto_20160624_1615'),
    ]

    operations = [
        migrations.CreateModel(
            name='CaseReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('initial_lab_report', models.TextField(null=True, blank=True)),
                ('case', models.OneToOneField(to='nodis.Case', on_delete=models.CASCADE)),
                ('initial_lab', models.ForeignKey(to='emr.LabResult', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='CaseReportReported',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_sent', models.DateTimeField(auto_created=True)),
                ('case_report', models.ForeignKey(to='nodis.CaseReport', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['time_sent'],
            },
        ),
        migrations.CreateModel(
            name='Reported',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField(db_index=True)),
                ('case_reported', models.ForeignKey(to='nodis.CaseReportReported', on_delete=models.CASCADE)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='reported',
            unique_together=set([('case_reported', 'content_type', 'object_id')]),
        ),
    ]
