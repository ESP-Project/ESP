# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0008_auto_20180305_1123'),
    ]

    operations = [
        migrations.AddField(
            model_name='casereport',
            name='pregnancy_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
