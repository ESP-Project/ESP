# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0001_initial'),
        ('hef', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Case',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(max_length=100, verbose_name='Common English name for this medical condition', db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('criteria', models.CharField(db_index=True, max_length=2000, null=True, verbose_name='Criteria on which case was diagnosed', blank=True)),
                ('source', models.CharField(max_length=255, verbose_name='What algorithm created this case?')),
                ('status', models.CharField(default='AR', max_length=32, verbose_name='Case status', choices=[('AR', 'AR - Awaiting Review'), ('UR', 'UR - Under Review'), ('RM', 'RM - Review by MD'), ('FP', 'FP - False Positive - Do NOT Process'), ('Q', 'Q - Confirmed Case, Transmit to Health Department'), ('S', 'S - Transmitted to Health Department'), ('NO', 'NO - Do NOT send cases'), ('RQ', 'RQ - Re-queued for transmission. Updated after prior transmission'), ('RS', 'RS - Re-sent after update subsequent to prior transmission')])),
                ('notes', models.TextField(null=True, blank=True)),
                ('reportables', models.TextField(null=True, blank=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated_timestamp', models.DateTimeField(auto_now=True)),
                ('sent_timestamp', models.DateTimeField(null=True, blank=True)),
                ('followup_sent', models.BooleanField(default=False, verbose_name='Followup event sent?')),
                ('events', models.ManyToManyField(related_name='case', to='hef.Event')),
                ('followup_events', models.ManyToManyField(related_name='followup', to='hef.Event')),
                ('patient', models.ForeignKey(to='emr.Patient', on_delete=models.CASCADE)),
                ('provider', models.ForeignKey(to='emr.Provider', on_delete=models.CASCADE)),
                ('timespans', models.ManyToManyField(to='hef.Timespan')),
            ],
            options={
                'ordering': ['id'],
                'permissions': [('view_phi', 'Can view protected health information')],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CaseStatusHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True, db_index=True)),
                ('old_status', models.CharField(max_length=10, choices=[('AR', 'AR - Awaiting Review'), ('UR', 'UR - Under Review'), ('RM', 'RM - Review by MD'), ('FP', 'FP - False Positive - Do NOT Process'), ('Q', 'Q - Confirmed Case, Transmit to Health Department'), ('S', 'S - Transmitted to Health Department'), ('NO', 'NO - Do NOT send cases'), ('RQ', 'RQ - Re-queued for transmission. Updated after prior transmission'), ('RS', 'RS - Re-sent after update subsequent to prior transmission')])),
                ('new_status', models.CharField(max_length=10, choices=[('AR', 'AR - Awaiting Review'), ('UR', 'UR - Under Review'), ('RM', 'RM - Review by MD'), ('FP', 'FP - False Positive - Do NOT Process'), ('Q', 'Q - Confirmed Case, Transmit to Health Department'), ('S', 'S - Transmitted to Health Department'), ('NO', 'NO - Do NOT send cases'), ('RQ', 'RQ - Re-queued for transmission. Updated after prior transmission'), ('RS', 'RS - Re-sent after update subsequent to prior transmission')])),
                ('changed_by', models.CharField(max_length=30)),
                ('comment', models.TextField(null=True, blank=True)),
                ('case', models.ForeignKey(to='nodis.Case', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Case Status History',
                'verbose_name_plural': 'Case Status Histories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReferenceCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(max_length=100, db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('ignore', models.BooleanField(default=False, db_index=True)),
                ('notes', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Reference Case',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReferenceCaseList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(max_length=255)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('notes', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Reference Case List',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('filename', models.CharField(max_length=512)),
                ('sent', models.BooleanField(default=False, verbose_name='Case status was set to sent?')),
                ('message', models.TextField(verbose_name='Case report message')),
                ('cases', models.ManyToManyField(to='nodis.Case')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('hostname', models.CharField(max_length=255, verbose_name='Host on which data was loaded')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ValidatorResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(max_length=100, db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('disposition', models.CharField(max_length=30, choices=[('exact', 'Exact'), ('similar', 'Similar'), ('missing', 'Missing'), ('new', 'New')])),
                ('cases', models.ManyToManyField(to='nodis.Case', null=True, blank=True)),
                ('encounters', models.ManyToManyField(to='emr.Encounter', null=True, blank=True)),
                ('events', models.ManyToManyField(to='hef.Event', null=True, blank=True)),
                ('lab_results', models.ManyToManyField(to='emr.LabResult', null=True, blank=True)),
                ('prescriptions', models.ManyToManyField(to='emr.Prescription', null=True, blank=True)),
                ('ref_case', models.ForeignKey(blank=True, to='nodis.ReferenceCase', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Validator Result',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ValidatorRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('complete', models.BooleanField(default=False)),
                ('related_margin', models.IntegerField()),
                ('notes', models.TextField(null=True, blank=True)),
                ('list', models.ForeignKey(to='nodis.ReferenceCaseList', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'Validator Run',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='validatorresult',
            name='run',
            field=models.ForeignKey(to='nodis.ValidatorRun', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='report',
            name='run',
            field=models.ForeignKey(to='nodis.ReportRun', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='referencecase',
            name='list',
            field=models.ForeignKey(to='nodis.ReferenceCaseList', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='referencecase',
            name='patient',
            field=models.ForeignKey(blank=True, to='emr.Patient', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='case',
            unique_together=set([('patient', 'condition', 'date', 'source')]),
        ),
    ]
