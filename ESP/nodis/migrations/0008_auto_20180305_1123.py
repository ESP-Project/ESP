# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0007_auto_20171018_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caseactivehistory',
            name='change_reason',
            field=models.CharField(max_length=8, choices=[('Q', 'Q - Qualifying events'), ('D', 'D - Disqualifying events'), ('E', 'E - Elapsed time')]),
        ),
        migrations.AlterField(
            model_name='caseactivehistory',
            name='status',
            field=models.CharField(max_length=8, choices=[('I', 'I - Initial detection and activation'), ('R', 'R - Reactivated'), ('D', 'D - Deactivated')]),
        ),
    ]
