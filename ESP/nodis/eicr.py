'''
                                  ESP Health
                         Notifiable Diseases Framework
                    Electronic Initial Case Report (eICR)

@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics, https://commoninf.com
@copyright: (c) 2021 Commonwealth Informatics
'''
import os, csv, datetime, copy, re
from datetime import datetime, timezone
import xml.etree.ElementTree as ET

from ESP.settings import CASE_REPORT_EICR_TEMPLATE, CASE_REPORT_LANG_CODES
from ESP.settings import CASE_REPORT_ROUTE_CODES
from ESP.settings import NEVER_SEND_RESULT_STRINGS, SITE_CLIA, SITE_NAME
from ESP.emr.models import Patient, Patient_Guardian, Provider, Encounter
from ESP.static.models import SpecimenSourceSnomed
from ESP.conf.models import LabTestMap, ConditionConfig, ReportableMedication
from django.core.exceptions import ObjectDoesNotExist
from ESP.nodis.management.commands import case_report

class eICR:

    def create_eicr(self, case, encounters, labs, medications, preg_info, condition_code):
        # register namespaces
        cda_urn = "urn:hl7-org:v3"
        ET.register_namespace('', cda_urn)
        #ET.register_namespace('cda', cda_urn) # not registered to remove from output
        ET.register_namespace('sdtc',"urn:hl7-org:sdtc")
        ET.register_namespace('xsi',"http://www.w3.org/2001/XMLSchema-instance")
        ns = {'cda':cda_urn, 'sdtc':"urn:hl7-org:sdtc", 'xsi':"http://www.w3.org/2001/XMLSchema-instance"}

        # get eICR template
        template_file = os.path.join('case_report', CASE_REPORT_EICR_TEMPLATE)
        tree = ET.parse(template_file)
        root = tree.getroot()

        # Patient demographics
        self.patient(root, case, ns)
        # Author and custodian
        self.author_custodian(root, case, ns)
        # Encompassing Encounter
        self.encompassing_encounter(root, case, ns)
        # Encounters section
        self.encounters_section(root, case, encounters, ns)
        # Results section
        self.results_section(root, case, labs, ns)
        # Medications administered section
        self.meds_section(root, case, medications, ns)
        # Social history section
        self.socialhistory_section(root, case, ns)
        # Pregnancy section
        self.pregnancy_section(root, case, preg_info, ns)
        # Vital signs section
        self.vitals_section(root, case, ns)
        # Reportability Response section
        self.rr_section(root, condition_code, ns)

        # save updated eICR to a file
        tree.write('eicr_test.xml', encoding='utf-8', xml_declaration=True)
        header = '<?xml version="1.0"?><?xml-stylesheet  type="text/xsl" href="cda.xsl"?>\n'
        eicr_str = ET.tostring(root, encoding="unicode")
        return header + eicr_str

    def socialhistory_section(self, root, case, ns):
        title = "Social History"
        text = ""
        temp_id = "2.16.840.1.113883.10.20.22.2.17"
        social_sec = self.get_section(root, temp_id, title, text, ns)
        entry = social_sec.find("cda:entry", ns)
        social_sec.remove(entry)
    
    def rr_section(self, root, condition_code, ns):
            title = "Reportability Response"
            text = ""
            temp_id = "2.16.840.1.113883.10.20.15.2.2.5"
            rr_sec = self.get_section(root, temp_id, title, text, ns)
            obs = rr_sec.find("cda:entry/cda:organizer/cda:component/cda:observation", ns)
            obs_value = obs.find("cda:value", ns)
            if condition_code:
                obs_value.set("code", condition_code['condition_code'])
                obs_value.set("displayName", condition_code['condition_text'])

    def pregnancy_section(self, root, case, preg_info, ns):
        # get pregnancy info
        preg_code = preg_info['preg_code']
        preg_code_name = "Pregnant" if preg_code and preg_code == '77386006' else "Not pregnant"
        preg_edd = preg_info['preg_edd']
        preg_edd_str = preg_edd.strftime("%Y%m%d") if preg_edd else ""
        preg_ega = str(preg_info['preg_ega']) if preg_info['preg_ega'] else ""
        preg_enc_date = preg_info['encounter_date']
        preg_enc_date_str = preg_enc_date.strftime("%Y%m%d") if preg_enc_date else ""
        preg_enc_id = preg_info['encounter_id'] if preg_info['encounter_id'] else ""
        if not preg_code:
            # remove pregnancy section
            sbody = root.find("cda:component/cda:structuredBody", ns)
            temp_id = "2.16.840.1.113883.10.20.22.2.80"
            component = self.get_component(sbody, temp_id, ns)
            sbody.remove(component)
        else:
            # add pregnancy section
            title = "Pregnancy"
            text = ""
            temp_id = "2.16.840.1.113883.10.20.22.2.80"
            preg_sec = self.get_section(root, temp_id, title, text, ns)
            entry = preg_sec.find("cda:entry", ns)
            obs = entry.find("cda:observation", ns)
            obs_id = obs.find("cda:id", ns)
            obs_id.set("root", preg_enc_id)
            obs_etime_low = obs.find("cda:effectiveTime/cda:low", ns)
            obs_etime_low.set("value", preg_enc_date_str)
            obs_value = obs.find("cda:value", ns)
            obs_value.set("code", preg_code)
            obs_value.set("displayName", preg_code_name)
            if preg_code == '77386006':
                # estimated date of delivery (edd)
                temp_id = "2.16.840.1.113883.10.20.22.4.297"
                edd_obs = self.get_entrel_observation(obs, temp_id, ns)
                edd_obs_id = edd_obs.find("cda:id", ns)
                edd_obs_id.set("root", preg_enc_id)
                edd_obs_etime = edd_obs.find("cda:effectiveTime", ns)
                edd_obs_etime.set("value", preg_enc_date_str)
                edd_obs_value = edd_obs.find("cda:value", ns)
                edd_obs_value.set("value", preg_edd_str)
                # estimated gestational age (ega)
                temp_id = "2.16.840.1.113883.10.20.22.4.280"
                ega_obs = self.get_entrel_observation(obs, temp_id, ns)
                ega_obs_id = ega_obs.find("cda:id", ns)
                ega_obs_id.set("root", preg_enc_id)
                ega_obs_etime = ega_obs.find("cda:effectiveTime", ns)
                ega_obs_etime.set("value", preg_enc_date_str)
                ega_obs_value = ega_obs.find("cda:value", ns)
                ega_obs_value.set("value", preg_ega)
            else:
                for er in obs.findall("cda:entryRelationship", ns):
                    obs.remove(er)
    
    def vitals_section(self, root, case, ns):
        title = "Vital Signs (Last Filed)"
        text = ""
        temp_id = "2.16.840.1.113883.10.20.22.2.4.1"
        vitals_sec = self.get_section(root, temp_id, title, text, ns)
        entry = vitals_sec.find("cda:entry", ns)
        vitals_sec.remove(entry)

    def meds_section(self, root, case, medications, ns):
        title = "Medications Administered"
        text = ""
        temp_id = "2.16.840.1.113883.10.20.22.2.38"
        meds_sec = self.get_section(root, temp_id, title, text, ns)
        # one medication per entry
        entry = meds_sec.find("cda:entry", ns)
        meds_sec.remove(entry)
        med_list = []
        for rx in medications:
            # medication
            rx_id = rx.order_natural_key
            rx_date_low = rx.start_date if rx.start_date else rx.date
            rx_date_low_str = rx_date_low.strftime("%Y%m%d") if rx_date_low else ""
            rx_date_high_str = rx.end_date.strftime("%Y%m%d") if rx.end_date else ""
            rx_status = "completed" if rx.status else "active"
            rx_dose_num = re.findall(r'\d+', rx.dose) if rx.dose else ''
            rx_dose_unit = re.findall(r'\D+', rx.dose) if rx.dose else ''
            rx_dose = rx_dose_num[0] if len(rx_dose_num) > 0 else ''
            rx_dose_unit = rx_dose_unit[0] if len(rx_dose_unit) > 0 else ''
            rx_route_code = self.route_code(rx.route)
            rx_route_name = rx.route if rx.route else ""
            rx_code = rx.code if rx.code else ""
            rx_name = rx.name if rx.name else ""
            # replace null rx code with a code in ReportableMedication
            # for a matching case condition and rx name
            if not rx_code and rx_name:
                conf = ConditionConfig.objects.filter(name=case.condition).first()
                if conf:
                    try:
                        med = ReportableMedication.objects.get(condition=conf,
                                                           drug_name=rx_name,
                                                           drug_code__isnull=False)
                        rx_code= med.drug_code
                    except ObjectDoesNotExist:
                        pass
            rx_refills = rx.refills if rx.refills else ""
            rx_quantity = rx.quantity if rx.quantity else ""
            # exclude duplicates with same name and date
            med_name_date = rx_name + rx_date_low_str
            if med_name_date in med_list:
                continue
            med_list.append(med_name_date)
            # Populate template with medication
            # substanceAdministration
            entry_copy = copy.deepcopy(entry)
            sa = entry_copy.find("cda:substanceAdministration", ns)
            sa_id = sa.find("cda:id", ns)
            sa_id.set('root', rx_id)
            sa_etime = sa.find("cda:effectiveTime[@xsi:type='IVL_TS']", ns)
            sa_etime_low = sa_etime.find("cda:low", ns)
            sa_etime_low.set('value',rx_date_low_str)
            sa_etime_high = sa_etime.find("cda:high", ns)
            if rx_date_high_str:
                ET.SubElement(sa_etime,'high', {'value':rx_date_high_str})
            sa_status_code = sa.find("cda:statusCode", ns)
            sa_status_code.set('code', rx_status)
            sa_dose_qty = sa.find("cda:doseQuantity", ns)
            sa_dose_qty.set('value', rx_dose)
            if rx_dose_unit:
                sa_dose_qty.set('unit', rx_dose_unit)
            sa_route_code = sa.find("cda:routeCode", ns)
            if rx_route_code:
                sa_route_code.set('code', rx_route_code)
                sa_route_code.set('displayName', rx_route_name)
            else:
                sa.remove(sa_route_code)
            sa_mprod = sa.find("cda:consumable/cda:manufacturedProduct", ns)
            sa_mcode = sa_mprod.find("cda:manufacturedMaterial/cda:code", ns)
            sa_mcode.set("code", rx_code)
            sa_mcode.set("displayName", rx_name)
            sa_er = sa.find("cda:entryRelationship[@typeCode='REFR']", ns)
            if rx_quantity:
                sa_sup = sa_er.find("cda:supply", ns)
                sa_sup_id = sa_sup.find("cda:id", ns)
                sa_sup_id.set('root', rx_id)
                sa_sup_repnum = sa_sup.find("cda:repeatNumber", ns)
                sa_sup_repnum.set('value', rx_refills)
                sa_sup_qty = sa_sup.find("cda:quantity", ns)
                sa_sup_qty.set('value', rx_quantity)
            else:
                sa.remove(sa_er)
            meds_sec.append(entry_copy)


    def get_provider(self, provider_id):

        try:
            provider = Provider.objects.get(id=provider_id)
            p = {}
            p['first_name'] = provider.first_name
            p['last_name'] = provider.last_name
            p['dept'] = provider.dept
            address1 = provider.dept_address_1 if provider.dept_address_1 else ''
            address2 = provider.dept_address_2 if provider.dept_address_2 else ''
            p['address'] = address1 + address2
            p['city'] = provider.dept_city
            p['state'] = provider.dept_state
            p['zip'] = provider.dept_zip
            p['county'] = provider.dept_county_code
            p['country'] = provider.dept_country
            p['telephone'] = provider.telephone
            p['area_code'] = provider.area_code
            p['npi'] = provider.npi
            return p
        except:
            return None

    def get_clia( self, clia):
        if clia:
            c = {}
            c['clia_id'] = clia.CLIA_ID
            c['dept'] = clia.laboratory_name
            address1 = clia.address1 if clia.address1 else ''
            address2 = clia.address2 if clia.address2 else ''
            c['address'] = address1 + address2
            c['city'] = clia.city
            c['state'] = clia.state
            c['zip'] = clia.zip
            c['country'] = clia.country
            c['county'] = clia.county_code
            c['telephone'] = None
            c['area_code'] = None
            return c
        else:
            c = {}
            c['clia_id'] = SITE_CLIA
            c['dept'] = None
            c['address'] = None
            c['city'] = None
            c['state'] = None
            c['zip'] = None
            c['country'] = None
            c['county'] = None
            c['telephone'] = None
            c['area_code'] = None
            return c
            
    def results_section(self, root, case, labs, ns):
        title = "Results"
        text = ""
        temp_id = "2.16.840.1.113883.10.20.22.2.3"
        results_sec = self.get_section(root, temp_id, title, text, ns)
        # one lab per entry
        entry = results_sec.find("cda:entry", ns)
        results_sec.remove(entry)
        batch = case_report.hl7Batch()
        for lab in labs:
            # never send logic from case report
            if (not lab.result_float and not lab.result_string):
                continue
            snomed, snomed2, titer_dilution, finding = batch.getSNOMED(lab, case.condition, case)

            ignore_result = None
            if finding is None and NEVER_SEND_RESULT_STRINGS and lab.result_string:
                ignore_result = re.search(NEVER_SEND_RESULT_STRINGS, lab.result_string, re.I)
            if ignore_result is not None:
                continue

            test_map = LabTestMap.objects.filter(native_code__exact=lab.native_code)
            if finding:
                if test_map.filter(donotsend_results__indicates__iexact=finding).exists():
                    continue
            # lab results
            lab_id = lab.natural_key if lab.natural_key else ''
            lab_date = lab.collection_date if lab.collection_date else lab.date
            lab_date_str = lab_date.strftime("%Y%m%d") if lab_date else ""
            lab_code = lab.output_or_native_code if lab.output_or_native_code else ""
            lab_name = lab.native_name if lab.native_name else ""
            lab_res_str = lab.result_string if lab.result_string else ""
            lab_res_float = lab.result_float if lab.result_float else ""
            lab_ref_high_str = lab.ref_high_string if lab.ref_high_string else ""
            lab_ref_low_str = lab.ref_low_string if lab.ref_low_string else ""
            lab_ref_high_float = lab.ref_high_float if lab.ref_high_float else ""
            lab_ref_low_float = lab.ref_low_float if lab.ref_low_float else ""
            lab_ref_unit = lab.ref_unit if lab.ref_unit else ''
            lab_value = ''
            lab_unit = ''
            lab_snomed = ''
            lab_finding = ''
            lab_ref_high_value = ''
            lab_ref_low_value = ''
            lab_value_type = 'PQ'
            if snomed:
                lab_snomed = snomed
                lab_finding = finding
                lab_value_type = 'CD'
            else:
                if titer_dilution:
                    lab_value = lab_res_str
                elif lab.result_float:
                    lab_value = str(lab_res_float)
                    lab_unit = lab_ref_unit
                    lab_ref_high_value = str(lab_ref_high_float)
                    lab_ref_low_value = str(lab_ref_low_float)
                elif lab.result_string:
                    result_tokens = lab_res_str.strip().split(' ')
                    lab_value = "{}{}".format(" ".join(result_tokens[:8]), '...' if len(result_tokens) > 8 else '')
                    lab_ref_high_value = lab_ref_high_str
                    lab_ref_low_value = lab_ref_low_str
            # lab status
            status_f = ['FINAL', 'F', 'C', 'CORRECTED']
            lab_status_f = lab.status and lab.status.upper() in status_f
            lab_res_status = 'F' if lab_status_f else 'P'
            status_names = {'F':'Final', 'P':'Preliminary'}
            lab_res_status_name = status_names[lab_res_status]
            # lab specimen
            lab_spec_source = self.get_specimen_source(lab)
            lab_spec_code = lab_spec_source['code']
            lab_spec_name = lab_spec_source['name']
            # lab specimen num
            lab_spec_num = lab.specimen_num

            # Populate template with lab results
            # result organizer
            entry_copy = copy.deepcopy(entry)
            org = entry_copy.find("cda:organizer", ns)
            org_id = org.find("cda:id", ns)
            org_id.set('root', lab_id)
            org_etime_low = org.find("cda:effectiveTime/cda:low", ns)
            org_etime_low.set('value',lab_date_str)
            org_etime_high = org.find("cda:effectiveTime/cda:high", ns)
            org_etime_high.set('value',lab_date_str)
            org_code = org.find("cda:code", ns)
            org_code.set('code', lab_code)
            org_code.set('displayName', lab_name)
            # Set lab result performer to CLIA
            lab_performer = self.get_clia(lab.CLIA_ID)
            clia_id = lab_performer['clia_id'] if lab_performer else None
            if clia_id:
                perf_entity_el = org.find("cda:performer/cda:assignedEntity", ns)
                self.update_org(perf_entity_el, ns, clia_id, lab_performer)
            else:
                org.remove(org.find("cda:performer", ns))
            # Set lab result informant to lab facility
            lab_facility = self.get_provider(lab.facility_provider_id)
            if lab_facility and (lab_facility['dept'] or lab_facility['address']):
                informant_entity_el = org.find("cda:informant/cda:assignedEntity", ns)
                self.update_org(informant_entity_el, ns, lab_facility['npi'], lab_facility)
            else:
                org.remove(org.find("cda:informant", ns))
            # Set lab result participant to lab provider
            lab_provider = self.get_provider(lab.provider_id)
            if lab_provider and (lab_provider['dept'] or lab_provider['address']):
                participant_el = org.find("cda:participant/cda:participantRole", ns)
                self.update_participant(participant_el, ns, lab_provider)
            else:
                org.remove(org.find("cda:participant", ns))
            # lab result observation
            temp_id = "2.16.840.1.113883.10.20.22.4.2"
            obs = self.get_comp_observation(org,temp_id, ns)
            obs_id = obs.find("cda:id", ns)
            obs_id.set('root', lab_id)
            obs_etime = obs.find("cda:effectiveTime", ns)
            obs_etime.set('value', lab_date_str)
            if clia_id:
                perf_entity_el = obs.find("cda:performer/cda:assignedEntity", ns)
                self.update_org(perf_entity_el, ns, clia_id, lab_performer)
            else:
                obs.remove(obs.find("cda:performer", ns))
            # lab result code and value
            obs_code = obs.find("cda:code", ns)
            obs_code.set('code', lab_code)
            obs_code.set('displayName', lab_name)
            obs_value = obs.find("cda:value", ns)
            obs_value.clear()
            if lab_value_type == 'CD':
                obs_value.set('code', lab_snomed)
                obs_value.set('displayName', lab_finding)
                obs_value.set('codeSystem', "2.16.840.1.113883.6.96")
                obs_value.set('codeSystemName', "SNOMED CT")
                obs_value.set('xsi:type', 'CD')
                obs.remove(obs.find("cda:referenceRange", ns))
            else:
                obs_value.set('unit', lab_unit)
                obs_value.set('value',lab_value)
                obs_value.set('xsi:type', 'PQ')
                if lab_ref_low_value or lab_ref_high_value:
                    obs_ref = obs.find("cda:referenceRange/cda:observationRange", ns)
                    obs_ref_value_low = obs_ref.find("cda:value/cda:low", ns)
                    obs_ref_value_low.set('unit', lab_unit)
                    obs_ref_value_low.set('value', lab_ref_low_value)
                    obs_ref_value_high = obs_ref.find("cda:value/cda:high", ns)
                    obs_ref_value_high.set('unit', lab_unit)
                    obs_ref_value_high.set('value', lab_ref_high_value)
                else:
                    obs_ref = obs.find("cda:referenceRange", ns)
                    obs.remove(obs_ref)

            # lab result status observation
            temp_id = "2.16.840.1.113883.10.20.22.4.418"
            obs_status = self.get_comp_observation(org,temp_id, ns)
            obs_status_value = obs_status.find("cda:value", ns)
            obs_status_value.set('code', lab_res_status)
            obs_status_value.set('displayName', lab_res_status_name)
            # lab result specimen
            proc = org.find("cda:component/cda:procedure", ns)
            proc_etime = proc.find("cda:effectiveTime/cda:low", ns)
            proc_etime.set('value', lab_date_str)
            if lab_spec_code:
                proc_target = proc.find("cda:targetSiteCode", ns)
                proc_target.set('code', lab_spec_code)
                proc_target.set('displayName', lab_spec_name)
                if lab_spec_num:
                    proc_part_id = proc.find("cda:participant/cda:participantRole/cda:id",ns)
                    proc_part_id.set('extension', lab_spec_num)
                else:
                    proc.remove(proc.find("cda:participant", ns))
            else:
                if lab_spec_num:
                    proc_part_id = proc.find("cda:participant/cda:participantRole/cda:id",ns)
                    proc_part_id.set('extension', lab_spec_num)
                else:
                    org.remove(org.find("cda:component[cda:procedure]", ns))

            results_sec.append(entry_copy)

    def encounters_section(self, root, case, encounters, ns):
        title = "Encounters"
        text = ""
        temp_id = "2.16.840.1.113883.10.20.22.2.22"
        encounters_sec = self.get_section(root, temp_id, title, text, ns)
        code_sys = {"icd10":"2.16.840.1.113883.6.90",
                    "icd9":"2.16.840.1.113883.6.103"}
        # one encounter per entry
        entry = encounters_sec.find("cda:entry", ns)
        encounters_sec.remove(entry)
        dx_code_list = []
        for enc in encounters:
            entry_copy = copy.deepcopy(entry)
            entry_enc = entry_copy.find("cda:encounter", ns)
            # encounter activities
            entry_enc_id = entry_enc.find("cda:id", ns)
            entry_enc_id.set('root', enc["id"])
            entry_enc_etime = entry_enc.find("cda:effectiveTime", ns)
            enc_date = enc["date"].strftime("%Y%m%d") if enc["date"] else ""
            entry_enc_etime.set('value', enc_date)
            entry_enc_code = entry_enc.find("cda:code", ns)
            cpt_code = enc["cpt"] if enc["cpt"] else ""
            entry_enc_code.set('code', cpt_code)
            entry_enc_code.set('displayName', "")
            # encounter diagnosis
            entry_enc_rel = entry_enc.find("cda:entryRelationship", ns)
            entry_enc.remove(entry_enc_rel)
            # problem oberservation
            enc_dx_codes = enc["dx_codes"]
            for dx in enc_dx_codes:
                entry_rel_copy = copy.deepcopy(entry_enc_rel)
                obs = entry_rel_copy.find("cda:act/cda:entryRelationship/cda:observation", ns)
                obs_id = obs.find("cda:id", ns)
                obs_id.set("root", "")
                obs_etime_low = obs.find("cda:effectiveTime/cda:low", ns)
                obs_date = enc["date"].strftime("%Y%m%d") if enc["date"] else ""
                obs_etime_low.set("value", obs_date)
                obs_value = obs.find("cda:value", ns)
                dx_code = dx["code"] if dx["code"] else ""
                dx_name = dx["name"] if dx["name"] else ""
                dx_type = dx["type"] if dx["type"] else ""
                obs_value.set("code", dx_code)
                obs_value.set("displayName", dx_name)
                obs_value.set("codeSystemName", dx_type)
                obs_code_sys = code_sys[dx["type"]] if dx_type in ["icd9","icd10"] else ""
                obs_value.set("codeSystem", obs_code_sys)
                # exclude duplicate dx's with same name and date
                dx_name_date = dx_name + obs_date
                if dx_name_date in dx_code_list:
                    continue
                dx_code_list.append(dx_name_date)
                entry_enc.append(entry_rel_copy)
            encounters_sec.append(entry_copy)

    def get_specimen_source(self, lab):
        unknown = {'code':'261665006', 'name':'UNKNOWN'}
        if lab.specimen_source:
            try:
                sss = SpecimenSourceSnomed.objects.get(name=lab.specimen_source.lower())
                if sss.snomed:
                    sss_name = sss.name if sss.name else ''
                    return {'code':sss.snomed, 'name': sss_name}
                else:
                    return unknown
            except SpecimenSourceSnomed.DoesNotExist:
                return unknown
        return unknown

    def get_entrel_observation(self, tag, temp_id, ns):
        observations = tag.findall("cda:entryRelationship/cda:observation", ns)
        template = "cda:templateId[@root='%s']" % temp_id
        for obs in observations:
            obs_temp_id = obs.find(template, ns)
            if obs_temp_id is not None: return obs
        return None

    def get_comp_observation(self, root, temp_id, ns):
        observations = root.findall("cda:component/cda:observation", ns)
        template = "cda:templateId[@root='%s']" % temp_id
        for obs in observations:
            obs_temp_id = obs.find(template, ns)
            if obs_temp_id is not None: return obs
        return None

    def get_section(self, root, temp_id, title, text, ns):
        sections = root.findall("cda:component/cda:structuredBody/cda:component/cda:section", ns)
        template = "cda:templateId[@root='%s']" % temp_id
        for section in sections:
            sec_temp_id = section.find(template, ns)
            if sec_temp_id is not None:
                sec_title = section.find("cda:title", ns)
                sec_title.text = title
                sec_text = section.find("cda:text", ns)
                sec_text.text = text
                return section
        return None

    def get_component(self, sbody, temp_id, ns):
        components = sbody.findall("cda:component", ns)
        template = "cda:section/cda:templateId[@root='%s']" % temp_id
        for comp in components:
            sec_temp = comp.find(template, ns)
            if sec_temp is not None:
                return comp
        return None

    def encompassing_encounter(self, root, case, ns):
        # Get encounter id from the first available case encounter (dx or enc event) 
        # for the encompassing encounter.
        case_enc_event = ( case.events.filter(name__contains='enc') | 
                            case.events.filter(name__contains='dx') ).order_by('date').first()
        # ecompassing encounter elements
        encomp_enc = root.find("cda:componentOf/cda:encompassingEncounter", ns)
        enc_id = encomp_enc.find("cda:id", ns)
        enc_code = encomp_enc.find("cda:code", ns)
        enc_etime_low = encomp_enc.find("cda:effectiveTime/cda:low", ns)
        enc_resp_party = encomp_enc.find("cda:responsibleParty", ns)
        enc_location = encomp_enc.find("cda:location", ns)
        if case_enc_event:
            # Case with encompassing encounter
            # first case encounter id and date
            encounter = Encounter.objects.get(id=case_enc_event.object_id)
            encounter_id = encounter.natural_key
            encounter_date = encounter.date.strftime("%Y%m%d") if encounter.date else ""
            enc_id.set('extension',encounter_id)
            enc_etime_low.set('value', encounter_date)
            # assigned entity
            provider = Provider.objects.get(id=case.provider_id)
            ent = enc_resp_party.find("cda:assignedEntity", ns)
            ent_id = ent.find("cda:id", ns)
            ent_id.set("extension", provider.natural_key)
            ent_addr = ent.find("cda:addr", ns)
            prov_street = provider.dept_address_1 if provider.dept_address_1 else ''
            prov_city = provider.dept_city if provider.dept_city else ''
            prov_state = provider.dept_state if provider.dept_state else ''
            prov_zip = provider.dept_zip if provider.dept_zip else ''
            prov_country = provider.dept_country if provider.dept_country else ''
            prov_county = provider.dept_county_code if provider.dept_county_code else ''
            prov_fname = provider.first_name if provider.first_name else ''
            prov_lname = provider.last_name if provider.last_name else ''
            prov_mname = provider.middle_name if provider.middle_name else ''
            self.update_address(ent_addr, ns,
                            prov_street,
                            prov_city,
                            prov_state,
                            prov_zip,
                            prov_county,
                            prov_country)
            # assigned entity phone
            ent_prov_tel = ent.find("cda:telecom", ns)
            self.phone_email(ent_prov_tel, ns, provider.area_code, provider.telephone, None)
            # assigend entity assignedPerson
            ent_person_name = ent.find("cda:assignedPerson/cda:name", ns)
            self.update_name(ent_person_name, ns, 
                         prov_fname,
                         prov_lname,
                         prov_mname
                        )
            # assigned entity represented Org
            org = ent.find("cda:representedOrganization", ns)
            org_name = org.find("cda:name", ns)
            org_name.text = provider.dept
            org_addr = org.find("cda:addr", ns)
            self.update_address(org_addr, ns,
                            prov_street,
                            prov_city,
                            prov_state,
                            prov_zip,
                            prov_county,
                            prov_country)
            # location/healthCareFacility
            loc_fac = enc_location.find("cda:healthCareFacility", ns)
            fac_id = loc_fac.find("cda:id", ns)
            fac_id.set("extension", provider.natural_key)
            fac_addr = loc_fac.find("cda:location/cda:addr", ns)
            self.update_address(fac_addr, ns,
                            prov_street,
                            prov_city,
                            prov_state,
                            prov_zip,
                            prov_county,
                            prov_country)
            # serviceProviderOrganization
            sp_org = loc_fac.find("cda:serviceProviderOrganization", ns)
            sp_org_name = sp_org.find("cda:name", ns)
            sp_org_name.text = provider.dept
            sp_org_tel = sp_org.find("cda:telecom", ns)
            self.phone_email(sp_org_tel, ns, provider.area_code, provider.telephone, None)
            sp_org_addr = sp_org.find("cda:addr", ns)
            self.update_address(sp_org_addr, ns,
                            prov_street,
                            prov_city,
                            prov_state,
                            prov_zip,
                            prov_county,
                            prov_country)
        else:
            # Cases triggered outside of an encounter (i.e. only labs) 
            # Set nullFlavor to "NA" and no responsibleParty and location
            encomp_enc.remove(enc_id)
            ET.SubElement(encomp_enc,'id', {'nullFlavor':"NA"})
            enc_code.set('code', "PHC2237")
            enc_code.set('codeSystem',"2.16.840.1.114222.4.5.274")
            enc_code.set('codeSystemName',"PHIN VS (CDC Local Coding System)")
            enc_code.set('displayName', "External Historical Encounter")
            del enc_etime_low.attrib['value']
            enc_etime_low.set('nullFlavor',"NA")
            encomp_enc.remove(enc_resp_party)
            encomp_enc.remove(enc_location)

    def author_custodian(self, root, case, ns):
        provider = Provider.objects.get(id=case.provider_id)

        # author time
        now = datetime.now(timezone.utc).astimezone().strftime("%Y%m%d%H%M%S%z")
        auth = root.find("cda:author", ns)
        auth_time = auth.find("cda:time", ns)
        auth_time.set('value', now)
        # author address
        auth_street = provider.dept_address_1 if provider.dept_address_1 else ''
        auth_city = provider.dept_city if provider.dept_city else ''
        auth_state = provider.dept_state if provider.dept_state else ''
        auth_zip = provider.dept_zip if provider.dept_zip else ''
        auth_country = provider.dept_country if provider.dept_country else ''
        auth_county = provider.dept_county_code if provider.dept_county_code else ''
        auth_addr = auth.find("cda:assignedAuthor/cda:addr", ns)
        self.update_address(auth_addr, ns,
                            auth_street,
                            auth_city,
                            auth_state,
                            auth_zip,
                            auth_county,
                            auth_country)

        # author phone
        auth_tel = auth.find("cda:assignedAuthor/cda:telecom", ns)
        self.phone_email(auth_tel, ns, provider.area_code, provider.telephone, None)
        # author device
        auth_dev = auth.find("cda:assignedAuthor/cda:assignedAuthoringDevice" , ns)
        auth_dev_model = auth_dev.find("cda:manufacturerModelName" , ns)
        auth_dev_software = auth_dev.find("cda:softwareName" , ns)
        auth_dev_model.set('displayName', 'ESP-'+ SITE_NAME)
        auth_dev_software.set('displayName', 'ESP-' + SITE_NAME)

        # custodian org
        cust = root.find("cda:custodian/cda:assignedCustodian", ns)
        cust_org = cust.find("cda:representedCustodianOrganization", ns)
        # custodian id
        cust_id = cust_org.find("cda:id", ns)
        cust_id.set("extension", provider.natural_key)
        # custodian name
        cust_name = cust_org.find("cda:name", ns)
        cust_name.text = provider.dept
        # custodian phone
        cust_tel = cust_org.find("cda:telecom", ns)
        self.phone_email(cust_tel, ns, provider.area_code, provider.telephone, None)
        # custodian address
        cust_addr = cust_org.find("cda:addr", ns)
        self.update_address(cust_addr, ns,
                            auth_street,
                            auth_city,
                            auth_state,
                            auth_zip,
                            auth_county,
                            auth_country)

    def patient(self, root, case, ns):
        patient = Patient.objects.get(id=case.patient_id)

        # get root for patient
        rt = root.find("cda:recordTarget/cda:patientRole", ns)
        # id
        patient_mrn = str(patient.mrn) if patient.mrn else ''
        patient_ssn = str(patient.ssn) if patient.ssn else ''
        rt_mrn = rt.find("cda:id[@root='2.16.840.1.113883.19.5']", ns)
        rt_mrn.set('extension', patient_mrn)
        rt_ssn = rt.find("cda:id[@root='2.16.840.1.113883.4.1']", ns)
        rt_ssn.set('extension', patient_ssn)
        # address
        rt_addr = rt.find("cda:addr", ns)
        self.update_address(rt_addr, ns,
                            patient.address1, 
                            patient.city, 
                            patient.state, 
                            patient.zip, 
                            None, 
                            patient.country)
        # phone
        rt_tel = rt.find("cda:telecom[@use='HP']", ns)
        self.phone_email(rt_tel, ns, patient.areacode, patient.tel, None)
        # name
        rt_patient = rt.find("cda:patient", ns)
        rt_patient_name = rt_patient.find("cda:name", ns)
        self.update_name(rt_patient_name, ns, 
                         patient.first_name,
                         patient.last_name,
                         patient.middle_name
                        )
        # gender
        rt_patient_gender = rt_patient.find("cda:administrativeGenderCode", ns)
        patient_gender = patient.gender if patient.gender else ''
        rt_patient_gender.set('code', patient_gender)
        # date of birth
        rt_patient_dob = rt_patient.find("cda:birthTime", ns)
        patient_dob = patient.date_of_birth.strftime("%Y%m%d") if patient.date_of_birth else ''
        rt_patient_dob.set('value', patient_dob)
        # deceased
        rt_patient_deceased_ind = rt_patient.find("sdtc:deceasedInd", ns)
        deceased_ind_value = rt_patient_deceased_ind.get('value', ns)
        deceased = 'true' if patient.date_of_death else 'false'
        rt_patient_deceased_ind.set('value',deceased) 
        if deceased == 'true':
            dod = patient.date_of_death.strftime("%Y%m%d")
            ET.SubElement(rt_patient,'sdtc:deceasedTime', {'value':dod})
        # race
        rt_patient_race = rt_patient.find("cda:raceCode", ns)
        race_text = patient.race if patient.race else 'UNKNOWN'
        race = self.race(race_text)
        rt_patient_race.set('code', race['code'])
        rt_patient_race.set('displayName', race['name'])
        # ethnic group
        rt_patient_ethnicity = rt_patient.find("cda:ethnicGroupCode", ns)
        ethnicity_text = patient.ethnicity if patient.ethnicity else 'UNKNOWN'
        ethnicity = self.ethnicity(ethnicity_text)
        rt_patient_ethnicity.set('code', ethnicity['code'])
        rt_patient_ethnicity.set('displayName', ethnicity['name'])
        # guardian
        try:
            pg = Patient_Guardian.objects.get(patient_id=case.patient_id)
        except ObjectDoesNotExist:
            pg = None
            rt_pg = rt_patient.find("cda:guardian", ns)
            rt_patient.remove(rt_pg)
        if pg:
            pg_street = pg.address1 if pg.address1 else ''
            pg_city = pg.city if pg.city else ''
            pg_state = pg.state if pg.state else ''
            pg_zip = pg.zip if pg.zip else ''
            pg_country = pg.country if pg.country else ''

            pg_county = pg.county_code if pg.county_code else ''
            rt_pg_addr = rt_patient.find("cda:guardian/cda:addr", ns)
            self.update_address(rt_pg_addr, ns,
                            pg_street, 
                            pg_city, 
                            pg_state, 
                            pg_zip, 
                            pg_county, 
                            patient.country)
            rt_pg_tel = rt_patient.find("cda:guardian/cda:telecom", ns)
            self.phone_email(rt_pg_tel, ns, pg.areacode, pg.tel, None)
            rt_pg_name = rt_patient.find("cda:guardian/cda:guardianPerson/cda:name", ns)
            self.update_name(rt_pg_name, ns, 
                         pg.first_name, 
                         pg.last_name, 
                         pg.middle_name)
        # language
        language = patient.home_language if patient.home_language else ''
        lang_code = self.lang_code(language) if language else ''
        rt_lang_code = rt_patient.find("cda:languageCommunication/cda:languageCode", ns)
        rt_lang_code.set('code', lang_code)

    def update_name( self, name_el, ns, first, last, middle):
        fname = name_el.find("cda:given", ns)
        mname = name_el.find("cda:given[@qualifier='IN']", ns)
        lname = name_el.find("cda:family", ns)
        fname.text = first
        lname.text = last
        mname.text = middle

    def update_address(self, addr_el, ns, street, city, state, postcode, county, country):
        street_el = addr_el.find("cda:streetAddressLine", ns)
        street_el.text = street if street else ''
        city_el = addr_el.find("cda:city", ns)
        city_el.text = city if city else ''
        state_el = addr_el.find("cda:state", ns)
        state_el.text = state if state else ''
        postalCode_el = addr_el.find("cda:postalCode", ns)
        postalCode_el.text = postcode if postcode else ''
        county_el = addr_el.find("cda:county", ns)
        country_el = addr_el.find("cda:country", ns)
        if county:
            county_el.text = county
        else:
            addr_el.remove(county_el)
        if country:
            country_el.text = country
        else:
            addr_el.remove(country_el)

    def update_org(self, entity_el, ns, ext_code, org_info):
        if ext_code:
            entity_id = entity_el.find("cda:id", ns)
            entity_id.set("extension", ext_code)
        org_el = entity_el.find("cda:representedOrganization", ns)
        org_name_el = org_el.find("cda:name", ns)
        org_name_el.text = org_info['dept']
        org_tel_el = org_el.find("cda:telecom", ns)
        self.phone_email(org_tel_el, ns, 
                        org_info['area_code'], 
                        org_info['telephone'],  None)
        org_addr_el = org_el.find("cda:addr", ns)
        self.update_address(org_addr_el, ns,
                            org_info['address'],
                            org_info['city'],
                            org_info['state'],
                            org_info['zip'],
                            org_info['county'],
                            org_info['country'])

    def update_participant(self, participant_el, ns, participant_info):
        name_el = participant_el.find("cda:playingEntity/cda:name", ns)
        name_el.text = participant_info['dept']
        tel_el = participant_el.find("cda:telecom", ns)
        self.phone_email(tel_el, ns, 
                        participant_info['area_code'], 
                        participant_info['telephone'],  None)
        addr_el = participant_el.find("cda:addr", ns)
        self.update_address(addr_el, ns,
                            participant_info['address'],
                            participant_info['city'],
                            participant_info['state'],
                            participant_info['zip'],
                            participant_info['county'],
                            participant_info['country'])

    def route_code(self, route_name):
        if route_name:
            route = "\A" + route_name.lower()
            code_file = os.path.join('case_report', CASE_REPORT_ROUTE_CODES)
            with open(code_file) as f:
                reader = csv.DictReader(f, skipinitialspace=True)
                for r in reader:
                    desc = r['Description'].lower()
                    if re.search(route, desc):
                        return r['Code']
        return ''

    def lang_code(self, lang):
        lang_name = lang.lower()
        template_file = os.path.join('case_report', CASE_REPORT_LANG_CODES)
        with open(template_file) as f:
            reader = csv.DictReader(f, skipinitialspace=True)
            for r in reader:
                if lang_name in r['name'].lower() and r['iso_6391_code']:
                    return r['iso_6391_code']
        return ''

    def phone_email(self, tel_el, ns, areacode, tel, email):
        tel_email = ''
        if areacode and tel:
            tel_email = 'tel:+1-' + areacode + '-' + tel
        elif tel:
            tel_email = 'tel:'+ tel
        elif email:
            tel_email = 'mailto:' + email
        
        tel_el.set('value', tel_email)

    def race(self, race_text):
        race_name = race_text.lower()
        race = [{'code': '1002-5', 'name':'American Indain or Alask Native'},
                {'code': '2028-9', 'name': 'Asian'},
                {'code': '2054-5', 'name': 'Black or African American'},
                {'code': '2076-8', 'name': 'Native Hawaiian or Other Pacific Islander'},
                {'code': '2106-3', 'name': 'White'}]
        for r in race:
            if race_name in r['name'].lower():
                return r
        return {'code':'', 'name':''}

    def ethnicity(self, e_text):
        e_name = e_text.lower()
        ethnicity = [{'code': '2135-2', 'name':'Hispanic or Latino'},
                {'code': '2186-5', 'name': 'Not Hispanic or Latino'}]
        for e in ethnicity:
            if e_name in e['name'].lower():
                return e
        return {'code':'', 'name':''}
