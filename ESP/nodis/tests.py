from datetime import date, timedelta

from django.core.management import call_command, CommandError
from django.test import TestCase

from ESP.conf.models import ConditionConfig, ReportableDx_Code, ReportableLab, LabTestMap
from ESP.emr.models import Patient, Provider, LabResult, Encounter
from ESP.hef.models import Event
from ESP.nodis.management.commands.case_report import hl7Batch, Command
from ESP.nodis.models import Case, CaseReport, CaseReportReported, Reported
from ESP.static.models import Dx_code


class AbstractEncounterReport(object):
    @classmethod
    def setUpTestData(cls):
        cls.patient = Patient.objects.create(provenance_id=1)
        cls.provider = Provider.objects.create(provenance_id=1)
        cls.condition = ConditionConfig.objects.create(name=cls.condition_name)
        cls.dx_code = Dx_code.objects.create(combotypecode='TEST:TEST', code="TEST", type="TEST", name="test",
                                             longname="obx_test_dx")
        ReportableDx_Code.objects.create(condition=cls.condition, dx_code=cls.dx_code)
        ReportableLab.objects.create(condition=cls.condition, native_name=cls.condition_name)
        LabTestMap.objects.create(test_name=cls.condition_name, reportable=True, native_code=cls.condition_name)


class Na5DXCodeTestCase(AbstractEncounterReport, TestCase):
    condition_name = 'test_cond'

    @classmethod
    def setUpTestData(cls):
        super(Na5DXCodeTestCase, cls).setUpTestData()

    def setUp(self):
        today = date.today()
        self.case_date = today - timedelta(days=72)
        self.case = Case.objects.create(condition=self.condition_name, patient=self.patient,
                                        provider=self.provider,
                                        date=self.case_date)

        for pid in range(2):
            lab = LabResult.objects.create(date=self.case_date, patient=self.patient, provenance_id=pid,
                                           provider=self.provider, result_date=self.case_date,
                                           native_code=self.condition_name, native_name=self.condition_name,
                                           result_string="Positive")
            event = Event.create('test:lab:event', 'tests', lab.date, self.patient, self.provider, lab)[1][0]
            self.case.events.add(event)

        self.hl7 = hl7Batch()
        self.hl7.currentCase = self.case.id

        self.command = Command()

    def addEncounter(self, offset=0):
        self.encounter = Encounter.objects.create(patient=self.patient, provider=self.provider,
                                                  date=self.case_date - timedelta(days=offset), provenance_id=1)
        self.encounter.dx_codes.add(self.dx_code)

    def test_symptom_obx_previous_yes(self):
        result = self.hl7.symptom_obx(0, na_5=True, condition_config=self.case.condition_config)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

    def test_symptom_obx_no_lx_or_encounters(self):
        result = self.hl7.symptom_obx(0, na_5=False, condition_config=self.case.condition_config)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_symptom_obx_only_lx(self):
        result = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(),
                                      condition_config=self.case.condition_config)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_symptom_obx_only_encounter(self):
        self.addEncounter()
        result = self.hl7.symptom_obx(0, encounters=[self.encounter], condition_config=self.case.condition_config)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

    def test_symptom_obx_lx_encounter_within_window(self):
        self.addEncounter(12)
        result = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter],
                                      condition_config=self.case.condition_config)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

    def test_symptom_obx_lx_encounter_outside_window(self):
        self.addEncounter(29)
        result = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter],
                                      condition_config=self.case.condition_config)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_dx_code_obx(self):
        self.addEncounter()
        dxs = self.case.dx_from_encounters(self.case.reportable_encounters[0])
        result = self.hl7.dx_code_obx(0, dx_code=dxs, condition_config=self.case.condition_config)
        self.assertEqual(len(result), 1)

    def test_dx_inside_symptom_window(self):
        self.addEncounter()
        dxs = self.case.dx_from_encounters(self.case.reportable_encounters[0])
        self.encounter.date = self.case_date - timedelta(days=12)

        symptom = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter],
                                       condition_config=self.case.condition_config)
        self.assertEqual(symptom.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

        dx = self.hl7.dx_code_obx(0, dx_code=dxs)
        self.assertEqual(len(dx), 1)

    def test_dx_outside_symptom_window(self):
        self.addEncounter(29)

        symptom = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter],
                                       condition_config=self.case.condition_config)
        self.assertEqual(symptom.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

        dxs = self.case.dx_from_encounters(self.case.reportable_encounters[0])
        dx = self.hl7.dx_code_obx(0, dx_code=dxs, condition_config=self.case.condition_config)
        self.assertEqual(len(dx), 0)

    def test_dx_outside_reportable_window_by_mdph(self):
        self.addEncounter(30)
        report = ''.join(self.command.mdph(False, None, [self.case]).split())
        self.assertFalse("10187-3" in report)

    def test_dx_inside_reportable_window_by_mdph(self):
        self.addEncounter(28)
        report = ''.join(self.command.mdph(False, None, [self.case]).split())
        self.assertTrue("10187-3" in report)


class TemperatureTestCase(AbstractEncounterReport, TestCase):
    condition_name = 'test_cond'

    def setUp(self):
        today = date.today()
        self.case_date = today - timedelta(days=72)
        self.case = Case.objects.create(condition=self.condition_name, patient=self.patient,
                                        provider=self.provider,
                                        date=self.case_date)

        self.hl7 = hl7Batch()
        self.hl7.currentCase = self.case.id

        self.encounter = Encounter.objects.create(patient=self.patient, provider=self.provider,
                                                  date=self.case_date, provenance_id=1)

        self.low_temp = 98.6
        self.high_temp = 100.5

    def test_no_temperature(self):
        self.assertIsNone(self.hl7.temprature_obx(1, temperature=None))

    def test_low_temperature(self):
        self.assertIsNone(self.hl7.temprature_obx(1, temperature=self.low_temp))

    def test_high_temperature(self):
        self.assertIsInstance(self.hl7.temprature_obx(1, temperature=self.high_temp), dict)

    def test_snomed_with_no_temperature(self):
        self.assertEqual(self.hl7.symptom_obx(0, temperature=None).get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_snomed_with_low_temperature(self):
        self.assertEqual(self.hl7.symptom_obx(0, temperature=self.low_temp).get('obx5'),
                         [('CE.4', self.hl7.snomed['NO'])])

    def test_snomed_with_high_temperature(self):
        self.assertEqual(self.hl7.symptom_obx(0, temperature=self.high_temp).get('obx5'),
                         [('CE.4', self.hl7.snomed['YES'])])

    def test_no_temperature_from_encounter(self):
        self.assertEqual(len(self.hl7.addCaseOBX(encounters=[self.encounter], caseid=self.case.id)), 3)

    def test_low_temperature_from_encounter(self):
        self.encounter.temperature = self.low_temp
        self.assertEqual(len(self.hl7.addCaseOBX(encounters=[self.encounter], caseid=self.case.id)), 3)

    def test_high_temperature_from_encounter(self):
        self.encounter.temperature = self.high_temp
        self.assertEqual(len(self.hl7.addCaseOBX(encounters=[self.encounter], caseid=self.case.id)), 4)


class PregnancyWeeksHl7TestCase(TestCase):
    def setUp(self):
        today = date.today()
        self.edd = today + timedelta(weeks=24)
        self.hl7 = hl7Batch()

    def test_expected_hl7(self):
        expected = "<ORU_R01.OBXNTE_SUPPGRP><OBX><OBX.1>4</OBX.1><OBX.2>SN</OBX.2><OBX.3><CE.4>NA-12</CE.4></OBX.3><OBX.5><SN.2>16</SN.2></OBX.5></OBX></ORU_R01.OBXNTE_SUPPGRP>"
        pregweeks = self.hl7.makeOBX(**self.hl7._pregweeks_obx(4, self.edd)).toxml()
        self.assertEqual(expected, pregweeks)


class DeleteCaseReportTestCase(AbstractEncounterReport, TestCase):
    condition_name = 'test_cond'

    @classmethod
    def setUpTestData(cls):
        super(DeleteCaseReportTestCase, cls).setUpTestData()

    def setUp(self):
        today = date.today()
        self.case_date = today - timedelta(days=72)
        self.case = Case.objects.create(condition=self.condition_name, patient=self.patient,
                                        provider=self.provider,
                                        date=self.case_date)
        case_report = CaseReport.objects.create(case=self.case)
        case_report_reported = CaseReportReported.objects.create(case_report=case_report)
        lab_result = LabResult.objects.create(date=self.case_date, patient=self.patient, provenance_id=1,
                                              provider=self.provider, result_date=self.case_date,
                                              native_code=self.condition_name, native_name=self.condition_name,
                                              result_string="Positive")
        Reported.set_reported(case_report_reported, lab_result)
        self.case2 = Case.objects.create(condition=self.condition_name, patient=self.patient,
                                         provider=self.provider,
                                         date=self.case_date + timedelta(days=2))

    def test_wrong_number_case_ids(self):
        with self.assertRaises(CommandError):
            call_command('delete_case_report')
        with self.assertRaises(CommandError):
            call_command('delete_case_report', case=['12', '14'])

    def test_wrong_case_id(self):
        with self.assertRaises(CommandError):
            call_command('delete_case_report', case=['1234523'])

    def test_no_report(self):
        with self.assertRaises(CommandError):
            call_command('delete_case_report', case=[str(self.case2.pk)])

    def test_delete(self):
        self.assertEqual(1, CaseReport.objects.count())
        self.assertEqual(1, CaseReportReported.objects.count())
        self.assertEqual(1, Reported.objects.count())
        try:
            call_command('delete_case_report', case=[str(self.case.pk)])
        except CommandError:
            self.fail("Should not raise Error")
        self.assertEqual(0, CaseReport.objects.count())
        self.assertEqual(0, CaseReportReported.objects.count())
        self.assertEqual(0, Reported.objects.count())
