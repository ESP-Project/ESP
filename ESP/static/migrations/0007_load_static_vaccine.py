# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from ESP.utils.utils import LoadFixtureData
from ESP import settings
from django.db import models, migrations
from django.core.management import call_command
from django.conf import settings

db_engine = settings.DATABASES['default']['ENGINE']

if db_engine == 'django.db.backends.postgresql_psycopg2':
  class Migration(migrations.Migration):

    dependencies = [
        ('static', '0006_New_DugSynsAndDXCodes'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'static_vaccine') + ".json")),
        migrations.RunSQL("select setval('static_vaccine_id_seq',(select max(id) from static_vaccine)+1);"),
    ]


else:
  class Migration(migrations.Migration):

    dependencies = [
        ('static', '0006_New_DugSynsAndDXCodes'),
    ]

    operations = [
        migrations.RunPython(LoadFixtureData(os.path.join(settings.TOPDIR, 'static', 'fixtures', 'static_vaccine') + ".json")),
    ]
