# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('static', '0005_auto_20180829_1151'),
    ]

    operations = [
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Atovaquone', 'Atovaquone', 'self' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Atovaquone' and other_name = 'Atovaquone');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Atovaquone', 'Malrone', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Atovaquone' and other_name = 'Malrone');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Atovaquone', 'Mepron', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Atovaquone' and other_name = 'Mepron');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Azithromycin', 'Azithromycin', 'self' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Azithromycin' and other_name = 'Azithromycin');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Azithromycin', 'Zithromax', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Azithromycin' and other_name = 'Zithromax');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Azithromycin', 'Zmax', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Azithromycin' and other_name = 'Zmax');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Quinine', 'Quinine', 'self' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Quinine' and other_name = 'Quinine');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Quinine', 'Qualaquin', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Quinine' and other_name = 'Qualaquin');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Clindamycin', 'Clindamycin', 'self' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Clindamycin' and other_name = 'Clindamycin');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'Clindamycin', 'Cleocin', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'Clindamycin' and other_name = 'Cleocin');"),
       migrations.RunSQL("insert into static_drugsynonym (generic_name, other_name, comment) SELECT 'PENICILLIN G', 'BICILLIN', '' WHERE NOT EXISTS (SELECT * from static_drugsynonym WHERE generic_name = 'PENICILLIN G' and other_name = 'BICILLIN');"),
       migrations.RunSQL("INSERT INTO static_dx_code (combotypecode, code, type, name, longname) SELECT 'icd10:Z22.7', 'Z22.7', 'icd10', 'Latent tuberculosis', 'Latent tuberculosis' WHERE NOT EXISTS (SELECT * from static_dx_code WHERE code = 'Z22.7');"),
       migrations.RunSQL("INSERT INTO static_dx_code (combotypecode, code, type, name, longname) SELECT 'icd10:Z86.15', 'Z86.15', 'icd10', 'Personal history of latent tuberculosis infection', 'Personal history of latent tuberculosis infection' WHERE NOT EXISTS (SELECT * from static_dx_code WHERE code = 'Z86.15');"),
       migrations.RunSQL("INSERT INTO static_dx_code (combotypecode, code, type, name, longname) SELECT 'icd10:M79.18', 'M79.18', 'icd10', 'Myalgia, other site', 'Myalgia, other site' WHERE NOT EXISTS (SELECT * from static_dx_code WHERE code = 'M79.18');"),
       migrations.RunSQL("INSERT INTO static_dx_code (combotypecode, code, type, name, longname) SELECT 'icd10:M79.12', 'M79.12', 'icd10', 'Myalgia of auxiliary muscles, head and neck', 'Myalgia of auxiliary muscles, head and neck' WHERE NOT EXISTS (SELECT * from static_dx_code WHERE code = 'M79.12');"),
       migrations.RunSQL("INSERT INTO static_dx_code (combotypecode, code, type, name, longname) SELECT 'icd10:M79.11', 'M79.11', 'icd10', 'Myalgia of mastication muscle', 'Myalgia of mastication muscle' WHERE NOT EXISTS (SELECT * from static_dx_code WHERE code = 'M79.11');"),
       migrations.RunSQL("INSERT INTO static_dx_code (combotypecode, code, type, name, longname) SELECT 'icd10:M79.10', 'M79.10', 'icd10', 'Myalgia, unspecified site', 'Myalgia, unspecified site' WHERE NOT EXISTS (SELECT * from static_dx_code WHERE code = 'M79.10');"),
    ]

