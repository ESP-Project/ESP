import os
from datetime import datetime
from django.db import models
from ESP.emr.models import Patient, Provider

class Patlist(models.Model):
    patient = models.ForeignKey(Patient, blank=False, null=False, on_delete=models.CASCADE)
    pcp = models.ForeignKey(Provider, blank=True, null=True, on_delete=models.CASCADE)
    disease_condition = models.CharField(max_length=10, blank=False, null=False, db_index=True)
    year = models.CharField(max_length=4, blank=False, null=False, db_index=True)
    age_group = models.CharField(max_length=11, blank=False, null=False, db_index=True)
    sex = models.CharField(max_length=7, blank=False, null=False, db_index=True)
    race_ethnicity = models.CharField(max_length=9, blank=False, null=False, db_index=True)
    birth_cohort = models.CharField(max_length=20, blank=False, null=False, db_index=True)
    row_id = models.CharField(max_length=15, blank=False, null=False)
    phone = models.CharField(max_length=20, blank=False, null=False)


    def __str__(self):
        return "CoC patient listing for patient {}".format(self.patient.natural_key)

    class Meta:
        indexes = [ models.Index(fields=['row_id'], name='row_id_pattern_idx', 
                                 opclasses=['varchar_pattern_ops']), ]


class Rxlist(models.Model):
    patlist = models.ForeignKey(Patlist, blank=False, null=False, 
                                on_delete=models.CASCADE, db_index=True)
    provider = models.ForeignKey(Provider, blank=True, null=True, on_delete=models.CASCADE)
    rx_name = models.CharField(max_length=255, blank=False, null=False)
    rx_date = models.DateTimeField(blank=True, null=True)


    def __str__(self):
        return "CoC Rx listing for patient {}".format(self.patlist.patient.natural_key)

class Lablist(models.Model):
    patlist = models.ForeignKey(Patlist, blank=False, null=False, 
                                on_delete=models.CASCADE, db_index=True)
    provider = models.ForeignKey(Provider, blank=True, null=True, on_delete=models.CASCADE)
    lab_name = models.CharField(max_length=255, blank=False, null=False)
    lab_date = models.DateTimeField(blank=True, null=True)
    lab_result = models.CharField(max_length=255, blank=False, null=False)
    

    def __str__(self):
        return "CoC lab listing for patient {}".format(self.patlist.patient.natural_key)


