from django.urls import re_path
from ESP.coc import views

app_name = 'coc'

urlpatterns =[
    re_path(r'^linelist/$', views.linelist, name='linelist'),
]
