import json, os, itertools, csv

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from ESP.settings import DATA_DIR
from ESP.coc.models import Patlist, Rxlist, Lablist
from ESP.utils.utils import log

COC_CONDITIONS = {
    "hepc": "Hepatitis C",
    "hiv": "HIV",
    "hivrisk": "HIV Risk",
}
SEX = {
    "x": "All",
    "1": "Female",
    "2": "Male",
    "3": "Unknown",
}
AGE_GROUP = {
    "x": "All",
    "1": "0-19",
    "2": "20-39",
    "3": "40-59",
    "4": "Over 59",
    "5": "Unknown",
}
BIRTH_COHORT = {
    "x": "All",
    "1": "Born prior to 1945",
    "2": "Born 1945-1965",
    "3": "Born after 1965",
    "4": "Unknown",
}
RACE = {
    "x": "All",
    "1": "ASIAN",
    "2": "BLACK",
    "3": "CAUCASIAN",
    "4": "HISPANIC",
    "5": "OTHER",
    "6": "Unknown",
}
ROWID = {
        "p":"Acute: Persistently infected",
        "q":"Acute: Unknown infection status",
        "s":"Chronic: Never on treatment",
        "u":"Chronic: Persistently infected",
        "v":"Chronic: Unknown infection status",
        "x":"Chronic, treated: Viral load not measured"
        }


@login_required
def linelist(request):

    # get condition and row id
    if request.GET.get('condition', None):
        condition = request.GET.get('condition', None)
        request.session['condition'] = condition
    else:
        condition = request.session['condition']
    if request.GET.get('row_id', None):
        row_id = request.GET.get('row_id', None)
        request.session['row_id'] = row_id
    else:
        row_id = request.session['row_id']
    row_id_string=ROWID[row_id]

    # Get COC parameters
    if request.GET.get('year', None):
        year = request.GET.get('year', None)
        request.session['year'] = year
    else:
        year = request.session.get('year')
    if request.GET.get('sex', None):
        sex = request.GET.get('sex', None)
        request.session['sex'] = sex
    else:
        sex = request.session.get('sex')
    if request.GET.get('age_group', None):
        age_group = request.GET.get('age_group', None)
        request.session['age_group'] = age_group
    else:
        age_group = request.session.get('age_group')
    if request.GET.get('race_ethnicity', None):
        race_ethnicity = request.GET.get('race_ethnicity', None)
        request.session['race_ethnicity'] = race_ethnicity
    else:
        race_ethnicity = request.session.get('race_ethnicity')
    if request.GET.get('birth_cohort', None):
        birth_cohort = request.GET.get('birth_cohort', None)
        request.session['birth_cohort'] = birth_cohort
    else:
        birth_cohort = request.session['birth_cohort']

    if year=="YEAR": 
        year_string="All" 
    else: 
        year_string=year
    sex_codes = sex.split(",") if sex else None
    sex_string = ""
    if sex_codes:
        for s in sex_codes:
            sex_string += ", " + SEX[s] if sex_string else SEX[s]

    age_group_codes = age_group.split(",") if age_group else None
    age_group_string = ""
    if age_group_codes:
        for ag in age_group_codes:
            age_group_string += ", " + AGE_GROUP[ag] if age_group_string else AGE_GROUP[ag]

    race_ethnicity_codes = race_ethnicity.split(",") if race_ethnicity else None
    race_ethnicity_string = ""
    if race_ethnicity_codes:
        for r in race_ethnicity_codes:
            race_ethnicity_string += ", " + RACE[r] if race_ethnicity_string else RACE[r]

    birth_cohort_codes = birth_cohort.split(",") if birth_cohort else None
    birth_cohort_string = ""
    if birth_cohort_codes:
        for b in birth_cohort_codes:
            birth_cohort_string += ", " + BIRTH_COHORT[b] if birth_cohort_string else BIRTH_COHORT[b]
    

    patlisting = Patlist.objects.filter(disease_condition__exact=condition,
                                        row_id__contains=row_id)
    if age_group != "x":
        age_group_list=age_group_string.split(",")
        age_group_list= [x.strip() for x in age_group_list]
        patlisting = patlisting.filter(age_group__in=age_group_list)
    if sex != "x":
        sex_list=sex_string.split(",")
        sex_list= [x.strip() for x in sex_list]
        patlisting = patlisting.filter(sex__in=sex_list)
    if race_ethnicity != "x":
        race_ethnicity_list=race_ethnicity_string.split(",")
        race_ethnicity_list= [x.strip() for x in race_ethnicity_list]
        patlisting = patlisting.filter(race_ethnicity__in=race_ethnicity_list)
    if birth_cohort != "x":
        birth_cohort_list=birth_cohort_string.split(",")
        birth_cohort_list= [x.strip() for x in birth_cohort_list]
        patlisting = patlisting.filter(birth_cohort__in=birth_cohort_list) 
    if year != "YEAR":
        year_list=year_string.split(",")
        year_list= [x.strip() for x in year_list]
        patlisting = patlisting.filter(year__in=year_list)

    if request.GET.get('export_csv', None) == 'coc_list':
        log.warning('Here is the query for patlisting_dict: %s' % (str(patlisting.query)))
        return linelist_download(request, patlisting)
    breadcrumb_dict = {
           "condition":condition + " -- ",
           "patient set":row_id_string + " -- ",
           "year":year_string + " -- ",
           "age group":age_group_string + " -- ",
           "sex":sex_string + " -- ",
           "race ethnicity":race_ethnicity_string + " -- ",
           "birth cohort":birth_cohort_string
           }
    patlisting_dict = {}
    for patrec in patlisting:
       patrec_dict = {}
       rx_dict = {}
       lab_dict = {}
       patrec_dict['first_name'] = patrec.patient.first_name
       patrec_dict['last_name'] = patrec.patient.last_name
       patrec_dict['phone'] = patrec.phone
       patrec_dict['natural_key'] = patrec.patient.natural_key
       patrec_dict['pcp_first_name'] = patrec.pcp.first_name
       patrec_dict['pcp_last_name'] = patrec.pcp.last_name
       for labrec in patrec.lablist_set.all():
           labrec_dict = {}
           labrec_dict['lab_date'] = labrec.lab_date
           labrec_dict['lab_name'] = labrec.lab_name
           labrec_dict['lab_result'] = labrec.lab_result
           labrec_dict['provider_first_name'] = labrec.provider.first_name
           labrec_dict['provider_last_name'] = labrec.provider.last_name
           lab_dict[labrec.pk] = labrec_dict
       patrec_dict['lab'] = lab_dict
       for rxrec in patrec.rxlist_set.all():
           rxrec_dict = {}
           rxrec_dict['rx_date'] = rxrec.rx_date
           rxrec_dict['rx_name'] = rxrec.rx_name
           rxrec_dict['provider_first_name'] = rxrec.provider.first_name
           rxrec_dict['provider_last_name'] = rxrec.provider.last_name
           rx_dict[rxrec.pk] = rxrec_dict
       patrec_dict['rx'] = rx_dict
       patlisting_dict[patrec.pk] = patrec_dict
    return render (request, 'coc/linelist.html', 
                     {"patlisting_dict":patlisting_dict,
                      "breadcrumb_dict":breadcrumb_dict,
                      "patlisting":patlisting})

@login_required
def linelist_download(request,qs):
    '''
    Exports case list from a queryset as a CSV file
   '''
    header = [
        'First_name',
        'Last_name',
        'phone',
        'EHR_ID',
        'PCP_first_name',
        'PCP_last_name',
        'lab_date',
        'lab_name',
        'lab_result',
        'ordered_by_first_name',
        'ordererd_by_last_name',
        'rx_date',
        'rx_name',
        'ordered_by_first_name',
        'ordered_by_last_name',
        ]
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename=coc_patient_list.csv'
    writer = csv.writer(response)
    writer.writerow(header)
    for a_case in qs:
        row = [
            a_case.patient.first_name,
            a_case.patient.last_name,
            a_case.phone,
            a_case.patient.natural_key,
            a_case.patient.pcp.first_name,
            a_case.patient.pcp.last_name]
        for lab in a_case.lablist_set.all():
            row.append(lab.lab_date)
            row.append(lab.lab_name)
            row.append(lab.lab_result)
            row.append(lab.provider.first_name)
            row.append(lab.provider.last_name)
        for rx in a_case.rxlist_set.all():
            row.append(rx.rx_date)
            row.append(rx.rx_name)
            row.append(rx.provider.first_name)
            row.append(rx.provider.last_name)
        writer.writerow(row)
    return response


