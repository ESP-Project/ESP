#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 ESP Health
#                     System-wide Package Install Script
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# @author: Jason McVetta <jason.mcvetta@gmail.com>
# @organization: Channing Laboratory - http://www.channing.harvard.edu
# @contact: http://esphealth.org
# @copyright: (c) 2009-2011 Channing Laboratory
# @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Run this script to install system-wide dependencies - i.e. those which must
# be installed using the OS's package manager, rather than being installed to
# the local Python virtual environment by Pip.
#
# Currently this script only supports recent versions of Ubuntu.  Eventually it
# may support other OSes.
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo "installing required redhat-lsb pkg"
echo 
sudo yum install redhat-lsb-core -y
_pkg_manager=''
_pkg_list=''
_os=`lsb_release -sr`

echo
echo Detected OS: $_os
echo

#
# Check Operating System
#
case $_os in
    "8.6" | "8.8" | "8.9" )
        _pkg_manager='yum'
        _pkg_list="git gcc httpd wget python38 python38-setuptools python38-devel python3-virtualenv python38-mod_wsgi"

        ;;
    "7.9" )
        _pkg_manager='yum'
        _pkg_list="git gcc httpd rh-python38 rh-python38-mod_wsgi wget rh-python38-python-setuptools rh-python38-python-devel postgresql13 postgresql13-server swatch"
        ;;
esac

#
# Install Packages
#
if [ -n "$_pkg_manager" ]; then
    echo "Automatic system package installation is supported!"
    echo
    echo "Installing packages:"
    echo "    $_pkg_list"
    echo ""
else
    echo ERROR:
    echo ""
    echo "This script currently supports only RedHat Enterprise 7.9 and 8"
    echo ""
    exit 1
fi

case $_os in
    "8.6" | "8.8" | "8.9" )
        echo "adding the Postgres rpm for RHEL 8"
        echo ""
        dnf install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
        dnf -qy module disable postgresql        
        ;;
    "7.9" )
        echo "adding repositories for RHEL 7"
        echo ""
        sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm 
        sudo subscription-manager repos --enable rhel-server-rhscl-7-rpms
       ;;
esac

#case $_os in
#    "7.6" )
#        echo "adding the postgres rpm for 7.6"
#        echo ""
#        sudo yum localinstall https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm -y
#	     sudo yum localinstall https://epel.mirror.constant.com/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm -y
#        ;;
#    "7.8" )
#        echo "adding the postgres rpm for 7.8"
#        echo ""
#        sudo yum install https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm -y
#        sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
#        ;;
#esac

case $_pkg_manager in
    "yum" )
        # Update package list
        echo "Refreshing yum package list..."
        echo "" 
        sudo yum update -y
        echo 'Installing yum packages...'
        echo "" 
        # Install Redhat packages
        sudo yum install $_pkg_list -y
        echo "done"
        echo "" 
        ;;
    * )
        echo "Internal error - unknown package manager"
        exit 2
        ;;
esac
